from django.urls import path

from .views import PipelineListView

app_name = "api"

urlpatterns = [path("pipelines", PipelineListView.as_view(), name="list")]
