from rest_framework.generics import ListAPIView

from halfpipe.models import Pipeline
from halfpipe.serializers import PipelineListSerializer


class PipelineListView(ListAPIView):
    queryset = Pipeline.objects.all().order_by("-creation_date")
    serializer_class = PipelineListSerializer

    filterset_fields = ["status", "assembly"]
