"""
ASGI config for halfpipe project.

It exposes the ASGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/3.1/howto/deployment/asgi/
"""

import os

from channels.routing import ProtocolTypeRouter, URLRouter
from django.core.asgi import get_asgi_application

import halfpipe.routing

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "halfpipe.settings.base")

application = ProtocolTypeRouter(
    {
        "http": get_asgi_application(),
        "websocket": URLRouter(halfpipe.routing.websocket_urlpatterns),
    }
)
