from django.apps import AppConfig


class DifferentialConfig(AppConfig):
    name = "halfpipe.differential"

    def ready(self):
        import halfpipe.differential.signals  # noqa
