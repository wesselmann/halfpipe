from channels.generic.websocket import AsyncJsonWebsocketConsumer


class DifferentialListConsumer(AsyncJsonWebsocketConsumer):
    async def connect(self):
        await self.channel_layer.group_add("differential-list", self.channel_name)
        await self.accept()

    async def disconnect(self, close_code):
        await self.channel_layer.group_discard("differential-list", self.channel_name)

    async def differential_update(self, event):
        await self.send_json(content=event["differential"])


class DifferentialDetailConsumer(AsyncJsonWebsocketConsumer):
    async def connect(self):
        self.group_name = (
            f"differential-detail-{self.scope['url_route']['kwargs']['pk']}"
        )
        await self.channel_layer.group_add(self.group_name, self.channel_name)
        await self.accept()

    async def disconnect(self, close_code):
        await self.channel_layer.group_discard(self.group_name, self.channel_name)

    async def differential_update(self, event):
        await self.send_json(content=event["differential"])
