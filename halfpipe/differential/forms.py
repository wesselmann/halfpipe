from django import forms
from django.core.exceptions import ValidationError
from django.core.validators import MinValueValidator
from django.utils.translation import gettext as _

from halfpipe.fields import PipelineMultiSelectField
from halfpipe.models import Pipeline


class MotifFilterForm(forms.Form):
    name = forms.CharField(max_length=200, required=False)
    p_value = forms.FloatField(
        initial=0.05,
        min_value=0,
        max_value=1,
        required=False,
        label=_("p-value cutoff"),
    )
    include_no_plot = forms.BooleanField(required=False, initial=False)


class DiffForm(forms.Form):
    send_email = forms.BooleanField(
        initial=False,
        required=False,
        help_text=_("Sends a notification via email when the pipeline finished."),
    )

    note = forms.CharField(
        max_length=50,
        required=False,
        help_text=_(
            "Short description of the differential analysis "
            "for identification purposes."
        ),
    )

    pipelines = PipelineMultiSelectField(label=_("Pipelines"))
    cluster = forms.IntegerField(
        label=_("Cluster size"),
        help_text=_("Cluster size used for KEGG analysis."),
        validators=[MinValueValidator(1)],
        initial=5,
    )

    enable_bias_correction = forms.BooleanField(
        label=_("Enable bias correction"),
        help_text=_("Enables bias correction for footprint analysis."),
        initial=True,
        required=False,
    )

    def clean_pipelines(self):
        field = self.cleaned_data["pipelines"]
        data = Pipeline.objects.filter(pk__in=field["pipelines"])

        if data.count() < 2:
            self.add_error(
                "pipelines", ValidationError(_("At least two pipelines are required."))
            )

        return data
