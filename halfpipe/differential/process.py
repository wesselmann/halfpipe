from celery import group, shared_task

from halfpipe.differential.signals import update_detail_channel
from halfpipe.differential.tasks import (
    diff_analysis_chip,
    diff_analysis_hint,
    finalize,
    prepare,
)
from halfpipe.models import DiffAnalysis
from halfpipe.tasks import create_handle_error, create_handle_group_error

handle_failed_in_group = create_handle_group_error(
    DiffAnalysis, update_detail_channel, "halfpipe.differential"
)
handle_error = create_handle_error(DiffAnalysis, "halfpipe.differential")


@shared_task()
def run_differential_analysis(options, pk):
    diff = DiffAnalysis.objects.get(pk=pk)
    diff.status = "RUNNING"
    diff.save()

    extended = group(
        diff_analysis_hint.s(options, pk).on_error(
            handle_failed_in_group.s(pk=pk, field="diff_hint")
        ),
        diff_analysis_chip.s(options, pk).on_error(
            handle_failed_in_group.s(pk=pk, field="diff_chip")
        ),
    )

    differential = prepare.s(options, pk) | extended | finalize.s(options, pk)

    differential.apply_async((), link_error=handle_error.s(pk=pk))
