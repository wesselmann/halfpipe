from asgiref.sync import async_to_sync
from channels.layers import get_channel_layer
from django.db.models.signals import m2m_changed, post_save
from django.dispatch import receiver

from halfpipe.models import DiffAnalysis
from halfpipe.serializers import (
    DiffAnalysisDetailSerializer,
    DiffAnalysisListSerializer,
)


def update_detail_channel(instance, channel_layer=get_channel_layer()):
    """Sends an update to a differential analysis specific group."""

    # There could have been an update in the meantime, so we refresh from the db
    instance.refresh_from_db()
    async_to_sync(channel_layer.group_send)(
        f"differential-detail-{instance.pk}",
        {
            "type": "differential.update",
            "differential": DiffAnalysisDetailSerializer(instance).data,
        },
    )


@receiver(m2m_changed, sender=DiffAnalysis)
def file_update(sender, instance, action, **kwargs):
    update_detail_channel(instance)


@receiver(post_save, sender=DiffAnalysis)
def update_list_channel(sender, instance, created, **kwargs):
    """Updates the differential list and detail groups on model save."""
    channel_layer = get_channel_layer()

    update_detail_channel(instance, channel_layer)

    async_to_sync(channel_layer.group_send)(
        "differential-list",
        {
            "type": "differential.update",
            "differential": {
                "instance": DiffAnalysisListSerializer(instance).data,
                "created": created,
            },
        },
    )
