import logging

from halfpipe import celery_app as app
from halfpipe.differential.signals import update_detail_channel
from halfpipe.models import DiffAnalysis
from halfpipe.steps.footprinting_differential.footprinting import run_hint
from halfpipe.steps.peak_annotate_differential.annotate import run_chip
from halfpipe.tasks import cleanup, create_watchdog

logger = logging.getLogger(__name__)
watchdog = create_watchdog(DiffAnalysis, update_detail_channel, "diffanalysis")


@app.task(bind=True, name="halfpipe.differential.diff_analysis_hint")
@watchdog("diff_hint")
def diff_analysis_hint(self, previous, options, diff_id):
    return run_hint(previous, options, diff_id)


@app.task(bind=True, name="halfpipe.differential.diff_analysis_chip")
@watchdog("diff_chip")
def diff_analysis_chip(self, previous, options, diff_id):
    return run_chip(previous, options, diff_id)


@app.task(bind=True, name="halfpipe.differential.prepare")
def prepare(self, options, pk):
    return {}


@app.task(bind=True, name="halfpipe.differential.finalize")
def finalize(self, last, options, pk):
    logger.info(f"Finishing differential analysis {pk}")

    instance = DiffAnalysis.objects.get(pk=pk)
    cleanup(instance, "SUCCESS")
