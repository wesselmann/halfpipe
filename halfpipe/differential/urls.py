from django.urls import path

from .views import (
    ChipView,
    DifferentialCreateView,
    DifferentialDeleteView,
    DifferentialDetailView,
    DifferentialListView,
    HintView,
)

app_name = "differential"

urlpatterns = [
    path("list", DifferentialListView.as_view(), name="list"),
    path("create", DifferentialCreateView.as_view(), name="create"),
    path("detail/<int:pk>/delete", DifferentialDeleteView.as_view(), name="delete"),
    path("detail/<int:pk>", DifferentialDetailView.as_view(), name="detail"),
    path(
        "detail/<int:pk>/hint",
        HintView.as_view(),
        name="footprinting",
    ),
    path(
        "detail/<int:pk>/chip",
        ChipView.as_view(),
        name="annotation",
    ),
]
