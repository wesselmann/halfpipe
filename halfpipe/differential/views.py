from django.contrib.auth.mixins import LoginRequiredMixin
from django.db import transaction
from django.shortcuts import get_object_or_404
from django.views.generic import DetailView, ListView
from django.views.generic.edit import FormView

from halfpipe.differential.forms import DiffForm, MotifFilterForm
from halfpipe.differential.process import run_differential_analysis
from halfpipe.models import DiffAnalysis, DiffChip, DiffHint, Motif
from halfpipe.serializers import DiffAnalysisDetailSerializer
from halfpipe.views import ProcessDeleteView


class DifferentialListView(ListView):
    """Lists all created differential analysis."""

    model = DiffAnalysis
    paginate_by = 25
    template_name = "differential/list.html"
    ordering = ["-creation_date"]


class DifferentialCreateView(LoginRequiredMixin, FormView):
    """Creates a new differential analysis"""

    form_class = DiffForm
    template_name = "differential/create.html"
    success_url = "/diff_analysis/list"

    def form_valid(self, form):
        options = form.cleaned_data

        pipelines = options.pop("pipelines")
        note = options.pop("note", "")
        with transaction.atomic():
            analysis = DiffAnalysis.objects.create(
                options=options,
                diff_hint=DiffHint.objects.create(),
                diff_chip=DiffChip.objects.create(),
                user=self.request.user,
                note=note,
            )

            analysis.pipelines.set(pipelines)

            # Hack to trigger websocket update via signals after m2m change.
            # We should use a different signal for that.
            analysis.save()

            run_differential_analysis.apply_async(
                countdown=5, args=[options, analysis.pk]
            )

        return super().form_valid(form)


class DifferentialDetailView(DetailView):
    model = DiffAnalysis
    template_name = "differential/detail.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["differential_data"] = DiffAnalysisDetailSerializer(
            context["object"]
        ).data

        return context


class DifferentialDeleteView(ProcessDeleteView):
    model = DiffAnalysis
    success_url = "differential:list"


class HintView(ListView):
    template_name = "differential/hint.html"
    model = Motif
    paginate_by = 50
    context_object_name = "motifs"

    def get_queryset(self):
        self.differential = get_object_or_404(DiffAnalysis, pk=self.kwargs.get("pk"))

        qs = self.differential.diff_hint.motifs.order_by("p_values__0")

        self.form = MotifFilterForm(self.request.GET)
        if self.form.is_valid():
            cleaned_data = self.form.cleaned_data
            if not any(bool(v) for v in cleaned_data.values()):
                cleaned_data = {"name": None, "p_value": 0.05, "include_no_plot": False}
                self.form = MotifFilterForm(initial={})

            if name := cleaned_data.pop("name"):
                qs = qs.filter(name__icontains=name)

            if p_value := cleaned_data.pop("p_value"):
                qs = qs.filter(p_values__0__lte=p_value)

            if not cleaned_data.pop("include_no_plot"):
                qs = qs.filter(lineplot__isnull=False)

        return qs

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["differential_data"] = DiffAnalysisDetailSerializer(
            self.differential
        ).data

        context["object"] = self.differential
        context["form"] = self.form
        return context


class ChipView(DifferentialDetailView):
    template_name = "differential/chip.html"
