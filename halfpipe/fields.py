import json

from django.core.exceptions import ValidationError
from django.forms import CharField, Field
from django.utils.translation import gettext as _

from halfpipe.validators import validate_dna_sequence
from halfpipe.widgets import FileUploadWidget, PipelineMultiSelectWidget


class FileUploadField(Field):
    """File Upload field directly to an S3 compatible API."""

    widget = FileUploadWidget

    def to_python(self, value):
        if value:
            try:
                parsed = json.loads(value)
                # Ensure url and name are existent
                if set(["url", "name"]) != set(parsed.keys()):
                    raise ValidationError(_("Missing tags."))

                return parsed
            except json.JSONDecodeError:
                raise ValidationError(_("No valid json."))

        else:
            return value


class SequenceField(CharField):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.validators.append(validate_dna_sequence)

    def to_python(self, value):
        return super().to_python(value).upper()


class PipelineMultiSelectField(Field):
    widget = PipelineMultiSelectWidget

    def to_python(self, value):
        if value:
            try:
                return json.loads(value)
            except json.JSONDecodeError:
                raise ValidationError(_("No valid json."))
        # Should be implicitly work, only for clarification
        else:
            return value
