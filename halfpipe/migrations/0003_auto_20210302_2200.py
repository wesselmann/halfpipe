# Generated by Django 3.1.7 on 2021-03-02 22:00

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("halfpipe", "0002_auto_20210302_1925"),
    ]

    operations = [
        migrations.AddField(
            model_name="postalignmentqc",
            name="diff_mate_unmapp",
            field=models.IntegerField(null=True),
        ),
        migrations.AddField(
            model_name="postalignmentqc",
            name="diff_nonprim",
            field=models.IntegerField(null=True),
        ),
        migrations.AddField(
            model_name="postalignmentqc",
            name="diff_supp",
            field=models.IntegerField(null=True),
        ),
        migrations.AddField(
            model_name="postalignmentqc",
            name="diff_unmapp",
            field=models.IntegerField(null=True),
        ),
        migrations.AddField(
            model_name="postalignmentqc",
            name="diff_unpaired",
            field=models.IntegerField(null=True),
        ),
        migrations.AddField(
            model_name="postalignmentqc",
            name="diff_unprop",
            field=models.IntegerField(null=True),
        ),
    ]
