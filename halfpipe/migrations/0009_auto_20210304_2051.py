# Generated by Django 3.1.7 on 2021-03-04 20:51

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("halfpipe", "0008_auto_20210304_1415"),
    ]

    operations = [
        migrations.AddField(
            model_name="peakcalling",
            name="fragment_length",
            field=models.IntegerField(null=True),
        ),
        migrations.AddField(
            model_name="peakcalling",
            name="fragments_total",
            field=models.IntegerField(null=True),
        ),
        migrations.AddField(
            model_name="peakcalling",
            name="genome_size",
            field=models.IntegerField(null=True),
        ),
        migrations.AddField(
            model_name="peakcalling",
            name="p_value_cutoff",
            field=models.FloatField(null=True),
        ),
        migrations.AddField(
            model_name="peakcalling",
            name="q_value",
            field=models.FloatField(null=True),
        ),
        migrations.AddField(
            model_name="peakcalling",
            name="window_d",
            field=models.IntegerField(null=True),
        ),
    ]
