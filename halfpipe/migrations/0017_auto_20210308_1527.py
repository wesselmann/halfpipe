# Generated by Django 3.1.7 on 2021-03-08 15:27

from django.db import migrations


def forward_func(apps, schema_editor):
    Annotate = apps.get_model("halfpipe", "Annotate")
    Pipeline = apps.get_model("halfpipe", "Pipeline")

    db_alias = schema_editor.connection.alias
    annotate = Annotate.objects.using(db_alias).create(status="CANCELLED")
    Pipeline.objects.using(db_alias).all().update(annotate=annotate)


def reverse_func(apps, schema_editor):
    pass


class Migration(migrations.Migration):

    dependencies = [
        ("halfpipe", "0016_auto_20210308_1527"),
    ]

    operations = [migrations.RunPython(forward_func, reverse_func)]
