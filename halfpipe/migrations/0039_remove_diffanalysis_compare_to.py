# Generated by Django 3.1.7 on 2021-03-17 22:47

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ("halfpipe", "0038_diffanalysis"),
    ]

    operations = [
        migrations.RemoveField(
            model_name="diffanalysis",
            name="compare_to",
        ),
    ]
