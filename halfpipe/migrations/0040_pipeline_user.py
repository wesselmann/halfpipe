# Generated by Django 3.1.7 on 2021-03-19 13:03

import django.db.models.deletion
from django.conf import settings
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ("halfpipe", "0039_auto_20210318_1437"),
    ]

    operations = [
        migrations.AddField(
            model_name="pipeline",
            name="user",
            field=models.ForeignKey(
                null=True,
                on_delete=django.db.models.deletion.CASCADE,
                related_name="pipelines",
                to=settings.AUTH_USER_MODEL,
            ),
        ),
    ]
