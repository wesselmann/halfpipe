# Generated by Django 3.1.7 on 2021-03-19 21:48

from django.db import migrations


def forward_func(apps, schema_editor):
    File = apps.get_model("halfpipe", "File")
    Pipeline = apps.get_model("halfpipe", "Pipeline")

    # Move exsisting files to m2m relation
    db_alias = schema_editor.connection.alias
    for pipeline in Pipeline.objects.using(db_alias).all():
        pipeline.files.add(*File.objects.using(db_alias).filter(pipeline=pipeline))


def reverse_func(apps, schema_editor):
    pass


class Migration(migrations.Migration):

    dependencies = [
        ("halfpipe", "0045_auto_20210319_2148"),
    ]

    operations = [migrations.RunPython(forward_func, reverse_func)]
