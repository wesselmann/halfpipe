# Generated by Django 3.1.7 on 2021-03-19 21:51

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ("halfpipe", "0046_auto_20210319_2148"),
    ]

    operations = [
        migrations.RemoveField(
            model_name="file",
            name="pipeline",
        ),
    ]
