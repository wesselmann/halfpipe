# Generated by Django 3.1.7 on 2021-03-22 10:25

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("halfpipe", "0049_auto_20210320_1059"),
    ]

    operations = [
        migrations.AddField(
            model_name="diffhint",
            name="lineplots",
            field=models.ManyToManyField(to="halfpipe.File"),
        ),
    ]
