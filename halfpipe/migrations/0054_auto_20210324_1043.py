# Generated by Django 3.1.7 on 2021-03-24 10:43

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("halfpipe", "0053_auto_20210323_1528"),
    ]

    operations = [
        migrations.RemoveField(
            model_name="diffhint",
            name="scatter_plot",
        ),
        migrations.AddField(
            model_name="diffhint",
            name="scatter_plot",
            field=models.ManyToManyField(related_name="hint", to="halfpipe.File"),
        ),
    ]
