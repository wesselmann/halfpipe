# Generated by Django 3.1.7 on 2021-03-24 21:02

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("halfpipe", "0056_auto_20210324_2059"),
    ]

    operations = [
        migrations.AddField(
            model_name="diffhint",
            name="lineplots",
            field=models.ManyToManyField(to="halfpipe.Motif"),
        ),
    ]
