import logging
from datetime import timedelta
from pathlib import Path

import urllib.parse
from django.conf import settings
from django.contrib.auth.models import User
from django.contrib.postgres.fields import ArrayField
from django.db import models
from django.template import Template
from django.template.loader import get_template
from django.templatetags.static import static
from django.utils import timezone
from django.utils.translation import gettext_lazy as _

logger = logging.getLogger()

PENDING = "PENDING"
RUNNING = "RUNNING"
SUCCESS = "SUCCESS"
FAILURE = "FAILURE"
CANCELLED = "CANCELLED"

STATUS_CHOICES = [
    (PENDING, "Pending"),
    (RUNNING, "Received"),
    (SUCCESS, "Succeeded"),
    (FAILURE, "Failed"),
    (CANCELLED, "Cancelled"),
]

PIPELINE_STEP_FIELDS = [
    "pre_alignment_qc",
    "alignment",
    "post_alignment_qc",
    "peak_calling",
    "annotate",
    "footprinting",
]

DEFAULT = "DEFAULT"
PLOT = "PLOT"
USER = "USER"
FILE_TYPES = [(DEFAULT, "default"), (PLOT, "plot"), (USER, "user")]


class Process(models.Model):
    email_template = None
    email_subject = None

    # Small description of the data for identification purposes
    note = models.CharField(max_length=150, blank=True)

    # Creation date of the pipeline
    creation_date = models.DateTimeField(default=timezone.now)

    # finish data of the pipeline
    end_date = models.DateTimeField(null=True)

    # Status of the pipeline
    status = models.CharField(max_length=9, choices=STATUS_CHOICES, default=PENDING)

    # Input options given via `halfpipe.pipeline.forms.PipelineForm`
    options = models.JSONField()

    user = models.ForeignKey(User, on_delete=models.CASCADE)

    class Meta:
        abstract = True

    def get_email_template(self):
        return self.email_template

    def get_email_subject(self, *args, **kwargs):
        return self.email_subject

    def has_successful(self):
        """Returns true if at least one step finished successfully."""
        return any((step.status == SUCCESS) for step in self.get_steps())

    def get_by_status(self, status):
        """Returns all diff analyses that have the given status."""
        return (f for f in self.get_steps() if f.status == status)

    def get_duration(self):
        """Returns the runtime of the diff analysis.

        If the analysis didn't finish, this is 0.
        """
        if self.end_date:
            return self.end_date - self.creation_date
        else:
            return timedelta()

    def save(self, *args, **kwargs):
        # Set status of all unfinished steps to cancelled
        if self.status == FAILURE or self.status == CANCELLED:
            # Set Pending fields to cancelled
            for field in self.get_by_status(PENDING):
                field.status = CANCELLED
                field.save()
            # Set (the one) running field to failure
            for field in self.get_by_status(RUNNING):
                field.status = FAILURE
                field.save()

        super().save(*args, **kwargs)

    def send_email(self):
        """Sends an email to the user on process end."""
        if self.user.email and self.options.get("send_email", False):
            logger.info(f"Sending mail to {self.user.email}")
            template: Template = get_template(self.get_email_template())
            self.user.email_user(
                self.get_email_subject(),
                template.render(
                    {
                        "username": self.user.username,
                        "status": self.status,
                        "duration": self.get_duration(),
                        "pk": self.pk,
                        "site_location": settings.SITE_LOCATION,
                    }
                ),
                fail_silently=False,
            )


class Pipeline(Process):
    email_template = "pipeline/email/pipeline_end.html"
    email_subject = _("Your pipeline has finished.")

    # Assembly used for this pipeline
    assembly = models.ForeignKey(
        "Assembly", on_delete=models.CASCADE, related_name="pipeline"
    )

    # Steps of the default pipeline
    pre_alignment_qc = models.ForeignKey(
        "PreAlignmentQC", on_delete=models.CASCADE, related_name="pipeline"
    )
    alignment = models.ForeignKey(
        "Alignment", on_delete=models.CASCADE, related_name="pipeline"
    )
    post_alignment_qc = models.ForeignKey(
        "PostAlignmentQC", on_delete=models.CASCADE, related_name="pipeline"
    )
    peak_calling = models.ForeignKey(
        "PeakCalling", on_delete=models.CASCADE, related_name="pipeline"
    )
    annotate = models.ForeignKey(
        "Annotate", on_delete=models.CASCADE, related_name="pipeline"
    )
    footprinting = models.ForeignKey(
        "Footprinting", on_delete=models.CASCADE, related_name="pipeline"
    )

    files = models.ManyToManyField("File", related_name="pipelines")

    # TODO: Deprecate this
    def get_pipeline_path(self):
        return self.get_path()

    def get_path(self):
        return Path(settings.MEDIA_ROOT) / f"pipeline_{ self.pk }"

    def get_steps(self):
        """Returns all step instances from this pipeline."""
        return [
            self.pre_alignment_qc,
            self.alignment,
            self.post_alignment_qc,
            self.peak_calling,
            self.annotate,
            self.footprinting,
        ]

    def get_by_status(self, status):
        """Returns all pipeline steps that have the given status."""
        return (f for f in self.get_steps() if f.status == status)

    def get_duration(self):
        """Returns the runtime of the pipeline.

        If the pipeline didn't finish, this is 0.
        """
        if self.end_date:
            return self.end_date - self.creation_date
        else:
            return timedelta()

    def get_score(self):
        """Returns the score of the pipeline as status path."""

        score = 0
        if self.status == SUCCESS:
            SCORES = ((0.50, "red"), (0.75, "yellow"), (1, "green"))
            score = self.alignment.get_score() * self.pre_alignment_qc.get_score()

            for v, k in SCORES:
                if score <= v:
                    name = k
                    break
        else:
            name = "empty"

        return score, static(f"/svg/{name}.svg")

    def save(self, *args, **kwargs):
        # Set status of all unfinished steps to cancelled
        if self.status == FAILURE or self.status == CANCELLED:
            # Set Pending fields to cancelled
            for field in self.get_by_status(PENDING):
                field.status = CANCELLED
                field.save()
            # Set (the one) running field to failure
            for field in self.get_by_status(RUNNING):
                field.status = FAILURE
                field.save()

        super().save(*args, **kwargs)

    def get_bucket_name(self):
        return settings.MINIO_PIPELINE_BUCKET_NAME.format(self.pk)


class Organism(models.Model):
    name = models.CharField(max_length=200, unique=True)
    short = models.CharField(max_length=200)
    genome_size = models.DecimalField(max_digits=60, decimal_places=0)
    taxonomy_id = models.CharField(max_length=200)

    class Meta:
        unique_together = ("name", "short")


class Assembly(models.Model):
    organism = models.ForeignKey(
        "Organism", related_name="indices", on_delete=models.CASCADE
    )

    information = models.CharField(max_length=200)

    r_package_name = models.CharField(max_length=300)
    name = models.CharField(max_length=300)
    mitochondrial_tag = models.CharField(max_length=300)

    def get_index_path(self):
        return Path(settings.MEDIA_ROOT) / "indices" / self.name

    def __str__(self):
        return f"{self.organism.name} - {self.information}"


class File(models.Model):
    url = models.URLField(unique=True)
    name = models.CharField(max_length=200)
    file_type = models.CharField(max_length=10, choices=FILE_TYPES, default=DEFAULT)


class Message(models.Model):
    text = models.TextField()


class Step(models.Model):
    status = models.CharField(max_length=9, choices=STATUS_CHOICES, default=PENDING)
    messages = models.ManyToManyField(Message)

    def add_messages(self, *args):
        for message in args:
            self.add_message(message)

    def add_message(self, text):
        if text:
            self.messages.add(Message.objects.create(text=text))

    class Meta:
        abstract = True


class PreAlignmentQC(Step):
    fastp_report = models.JSONField(null=True)

    def get_score(self):
        """Returns score consisting of read count and filtering percentage."""

        # Effective coverage
        READ_COUNT_SCORING = (
            (5, 1),
            (4, 0.9),
            (3, 0.8),
            (2, 0.7),
            (0.5, 0.5),
            (0, 0),
        )

        if self.fastp_report:
            # Filter rate
            before = self.fastp_report["summary"]["before_filtering"]["total_bases"]
            after = self.fastp_report["summary"]["after_filtering"]["total_bases"]
            filter_rate = after / before

            # Readcount scoring
            bases = self.fastp_report["summary"]["before_filtering"]["total_bases"]
            estimated_coverage = (
                bases / self.pipeline.get().assembly.organism.genome_size
            )
            for v, r in READ_COUNT_SCORING:
                if estimated_coverage >= v:
                    count_score = r
                    break

            # Factor in readcount and filter rate
            return filter_rate * count_score
        else:
            return 0


class Alignment(Step):
    analysis_plot = models.ForeignKey("File", null=True, on_delete=models.CASCADE)
    all_reads = models.IntegerField(null=True, default=0)
    # valid reads are all paired bzw. unpaired reads depending on the experiment
    valid_reads = models.IntegerField(null=True, default=0)
    zero_aligned_reads = models.IntegerField(null=True, default=0)
    exactly_one_align = models.IntegerField(null=True, default=0)
    more_than_one = models.IntegerField(null=True, default=0)
    overall_alignment_rate = models.FloatField(null=True)
    # only for paired experiments
    discord_aligned_reads = models.IntegerField(null=True, default=0)
    never_aligned_reads = models.IntegerField(null=True, default=0)
    only_one_mate_aligned = models.IntegerField(null=True, default=0)
    no_mate_aligned = models.IntegerField(null=True, default=0)

    def get_score(self):
        """Returns the alignment rate for scoring purposes."""
        if self.overall_alignment_rate:
            return self.overall_alignment_rate / 100
        else:
            return 0


class PostAlignmentQC(Step):
    diff_nonprim = models.IntegerField(null=True)
    diff_supp = models.IntegerField(null=True)
    diff_unmapp = models.IntegerField(null=True)
    diff_unpaired = models.IntegerField(null=True)
    diff_unprop = models.IntegerField(null=True)
    diff_unprop_wo = models.IntegerField(null=True)
    diff_mate_unmapp = models.IntegerField(null=True)
    diff_quality = models.IntegerField(null=True)
    diff_dup = models.IntegerField(null=True)
    diff_mito = models.IntegerField(null=True)
    diff_truly_unpaired = models.IntegerField(null=True)
    final_alignment = models.ForeignKey(
        "File", null=True, on_delete=models.CASCADE, related_name="bam"
    )
    plots = models.ManyToManyField(File)


class PeakCalling(Step):
    genome_size = models.BigIntegerField(null=True)
    p_value_cutoff = models.FloatField(null=True)
    q_value = models.FloatField(null=True)
    fragment_length = models.IntegerField(null=True)
    fragments_total = models.IntegerField(null=True)
    window_d = models.IntegerField(null=True)
    peak_count = models.IntegerField(null=True)
    peaks = models.ForeignKey(
        "File", null=True, on_delete=models.CASCADE, related_name="peaks"
    )


class Annotate(Step):
    covplot = models.ForeignKey(
        "File", null=True, on_delete=models.CASCADE, related_name="+"
    )
    heatmap = models.ForeignKey(
        "File", null=True, on_delete=models.CASCADE, related_name="+"
    )
    avgplot = models.ForeignKey(
        "File", null=True, on_delete=models.CASCADE, related_name="+"
    )
    upset = models.ForeignKey(
        "File", null=True, on_delete=models.CASCADE, related_name="+"
    )
    upset_enriched = models.ForeignKey(
        "File", null=True, on_delete=models.CASCADE, related_name="+"
    )
    gene_set_enrichment = models.ForeignKey(
        "File", null=True, on_delete=models.CASCADE, related_name="+"
    )
    over_representation = models.ForeignKey(
        "File", null=True, on_delete=models.CASCADE, related_name="+"
    )
    cnet = models.ForeignKey(
        "File", null=True, on_delete=models.CASCADE, related_name="+"
    )
    tagmatrix = models.ForeignKey(
        "File", null=True, on_delete=models.CASCADE, related_name="+"
    )
    enriched = models.BooleanField(null=True)


class Footprinting(Step):
    num_reads = models.IntegerField(null=True)
    num_peaks = models.IntegerField(null=True)
    num_tc_within_peaks = models.FloatField(null=True)
    num_footprints = models.IntegerField(null=True)
    plots = models.ManyToManyField(File)

    mpbs_file = models.ForeignKey(
        "File", null=True, on_delete=models.CASCADE, related_name="bed"
    )

    footprint_file = models.ForeignKey(
        "File", null=True, on_delete=models.CASCADE, related_name="footprint"
    )


class DiffAnalysis(Process):
    email_template = "differential/email/differential_end.html"
    email_subject = _("Your differential analysis has finished.")

    pipelines = models.ManyToManyField("Pipeline")

    # Steps of the default differential analysis
    diff_hint = models.ForeignKey(
        "DiffHint", on_delete=models.CASCADE, related_name="diffanalysis"
    )

    diff_chip = models.ForeignKey(
        "DiffChip", on_delete=models.CASCADE, related_name="diffanalysis"
    )

    files = models.ManyToManyField("File", related_name="differential")

    def get_assembly(self):
        if pipeline := self.pipelines.first():
            return pipeline.assembly

    # Deprecate this
    def get_diff_path(self):
        return self.get_path()

    def get_path(self):
        return Path(settings.MEDIA_ROOT) / f"diff_analysis_{self.pk}"

    def get_steps(self):
        """Returns all step instances from this diff analysis."""
        return [self.diff_hint, self.diff_chip]

    def get_bucket_name(self):
        return settings.MINIO_DIFFERENTIAL_BUCKET_NAME.format(self.pk)


class Motif(models.Model):
    name = models.CharField(max_length=200)
    p_values = ArrayField(models.FloatField())
    lineplot = models.ForeignKey(
        "File", on_delete=models.CASCADE, related_name="motif", null=True
    )

    def get_name(self):
        return self.name.rsplit(".", 1)[1]

    def get_url_jaspar(self):
        # No https in 2021. :(
        return f"http://jaspar.genereg.net/matrix/{self.name.rsplit('.', 1)[0]}"

    def get_url_uniprot(self):
        taxonomy_id = (
            self.diffhint_set.get()
            .diffanalysis.get()
            .get_assembly()
            .organism.taxonomy_id
        )
        params = {"query": f"gene:{self.get_name()} organism:{taxonomy_id}"}
        return "https://www.uniprot.org/uniprot/?" + urllib.parse.urlencode(params)

    class Meta:
        unique_together = ("name", "lineplot")


class DiffHint(Step):
    scatter_plots = models.ManyToManyField(File, related_name="hint")
    motifs = models.ManyToManyField(Motif)


class DiffChip(Step):
    avgplot = models.ForeignKey(
        "File", null=True, on_delete=models.CASCADE, related_name="avg"
    )
    heatmap = models.ForeignKey(
        "File", null=True, on_delete=models.CASCADE, related_name="heat"
    )
    annobar = models.ForeignKey(
        "File", null=True, on_delete=models.CASCADE, related_name="anno"
    )
    dist_to_tss = models.ForeignKey(
        "File", null=True, on_delete=models.CASCADE, related_name="dist"
    )
    dotplot = models.ForeignKey(
        "File", null=True, on_delete=models.CASCADE, related_name="dot"
    )
    vennplot = models.ForeignKey(
        "File", null=True, on_delete=models.CASCADE, related_name="venn"
    )
