from django.apps import AppConfig


class PipelineConfig(AppConfig):
    name = "halfpipe.pipeline"

    def ready(self):
        import halfpipe.pipeline.signals  # noqa
