from channels.generic.websocket import AsyncJsonWebsocketConsumer


class PipelineListConsumer(AsyncJsonWebsocketConsumer):
    async def connect(self):
        await self.channel_layer.group_add("pipeline-list", self.channel_name)
        await self.accept()

    async def disconnect(self, close_code):
        await self.channel_layer.group_discard("pipeline-list", self.channel_name)

    async def pipeline_update(self, event):
        await self.send_json(content=event["pipeline"])


class PipelineDetailConsumer(AsyncJsonWebsocketConsumer):
    async def connect(self):
        self.group_name = f"pipeline-detail-{self.scope['url_route']['kwargs']['pk']}"
        await self.channel_layer.group_add(self.group_name, self.channel_name)
        await self.accept()

    async def disconnect(self, close_code):
        await self.channel_layer.group_discard(self.group_name, self.channel_name)

    async def pipeline_update(self, event):
        await self.send_json(content=event["pipeline"])
