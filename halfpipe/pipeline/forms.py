from django import forms
from django.core.exceptions import ValidationError
from django.core.validators import MinValueValidator
from django.utils.translation import gettext as _

from halfpipe.fields import FileUploadField, SequenceField
from halfpipe.models import Assembly
from halfpipe.validators import validate_even
from halfpipe.widgets import FileUploadWidget

FASTQ_INPUT_ATTRS = {"accept": ".fastq,.fastq.gz,text/plain,chemical/seq-na-fastq"}
BAM_INPUT_ATTRS = {"accept": ".bam"}


class PresetForm(forms.Form):
    PRESET_NAMES = (
        ("normal", _("Normal")),
        ("strict", _("Strict")),
        ("lax", _("Lax")),
    )

    preset = forms.ChoiceField(
        choices=PRESET_NAMES,
        required=False,
        help_text=_("Select a preset for specific filtering needs."),
    )


class PipelineForm(forms.Form):
    """Provides the main form for pipeline creation."""

    note = forms.CharField(
        max_length=50,
        required=False,
        help_text=_("Short description of the pipeline for identification purposes."),
    )

    send_email = forms.BooleanField(
        initial=False,
        required=False,
        help_text=_("Sends a notification via email when the pipeline finished."),
    )

    # General * non biological * pipeline settings
    retain_user_uploaded = forms.BooleanField(
        initial=False,
        required=False,
        help_text=_(
            "Saves the files uploaded to this form. If you mix multiple Pipelines"
            " this helps to keep track of the input data."
        ),
    )

    # General pipeline settings
    assembly = forms.ModelChoiceField(
        queryset=Assembly.objects.all(),
        label=_("Assembly"),
        help_text=_(
            "Choose the reference assembly to be used to process the given reads."
        ),
    )

    enable_paired_end = forms.BooleanField(
        initial=True,
        required=False,
        help_text=_("Enable for paired end data."),
    )

    forward_readfile_url = FileUploadField(
        label=_("Forward read file"),
        required=False,
        help_text=_("Forward readfile."),
        widget=FileUploadWidget(FASTQ_INPUT_ATTRS),
    )

    reverse_readfile_url = FileUploadField(
        label=_("Reverse read file"),
        required=False,
        help_text=_("Reverse readfile."),
        widget=FileUploadWidget(FASTQ_INPUT_ATTRS),
    )

    readfile_url = FileUploadField(
        label=_("Read file"),
        required=False,
        help_text=_("Readfile."),
        widget=FileUploadWidget(FASTQ_INPUT_ATTRS),
    )

    # Pre alignment processing
    enable_adapter_trimming = forms.BooleanField(
        initial=True,
        required=False,
        help_text=_("Enables trimming of adapters that are left from sequencing."),
    )

    detect_adapters = forms.BooleanField(
        initial=True,
        required=False,
        help_text=_("Autodetect the used adapters."),
    )
    forward_adapter_sequence = SequenceField(
        max_length=200,
        required=False,
        help_text=_("Forward adapter sequence."),
    )
    reverse_adapter_sequence = SequenceField(
        max_length=200,
        required=False,
        help_text=_("Reverse adapter sequence."),
    )
    adapter_sequence = SequenceField(
        max_length=200,
        required=False,
        help_text=_("Adapter sequence in this field."),
    )

    # Post alignment processing
    enable_unpaired_filter = forms.BooleanField(initial=True, required=False)
    enable_unproperly_paired_filter = forms.BooleanField(
        required=False,
        help_text=_(
            "To be properly paired, a mapped read has to be paired with a mate mapping "
            "on the complementary strand of the same chromosome in a reasonable "
            "distance. Improperly paired reads do not satisfy all criteria."
        ),
    )
    enable_mate_unmapped_filter = forms.BooleanField(required=False)
    enable_unmapped_filter = forms.BooleanField(initial=True, required=False)
    enable_nonprimary_filter = forms.BooleanField(
        initial=True,
        required=False,
        help_text=_(
            "A non-primary (secondary) alignment occurs when a read maps "
            "decently well to multiple locations on the genome."
        ),
    )
    enable_supplementary_filter = forms.BooleanField(
        initial=True,
        required=False,
        help_text=_(
            "A supplementary is read that does not map linearly along the "
            "chromosome."
        ),
    )

    enable_quality_filter = forms.BooleanField(
        initial=True,
        required=False,
        help_text=_(
            "If quality filter is enabled, all reads with a MAPQ score smaller "
            "than the given threshold get discarded."
        ),
    )
    quality_threshold = forms.IntegerField(
        required=False,
        validators=[MinValueValidator(0)],
        initial=10,
        help_text=_(
            "Typical thresholds for Bowtie2 alignment are 10 (less conservative), 20, "
            "or 30 (more conservative). The highest possible MAPQ score in a Bowtie2 "
            "alignment is 42."
        ),
    )

    enable_mitochondrial_filter = forms.BooleanField(
        initial=True,
        required=False,
        help_text=_(
            "If mitochondrial filter is enabled, all reads mapped to the "
            "mitochondrial genome get discarded. Filtering unmapped reads prior "
            "is mandatory."
        ),
    )

    enable_duplicated_filter = forms.BooleanField(
        initial=True,
        required=False,
        help_text=_(
            "Duplicates can occur during library construction (PCR) or "
            "during sequencing (optical)."
        ),
    )

    enable_shift_filter = forms.BooleanField(
        initial=False,
        required=False,
        help_text=_(
            "Only recommended if the exact position of motifs is needed while "
            "processing ATAC-seq data, since the sequence and quality scores "
            "are trimmed during the shifting."
        ),
    )

    # Peak Calling:
    control_bam_file = forms.BooleanField(
        initial=False,
        required=False,
        help_text=_(
            "The control is reducing the number of false peaks (exist as a background)"
            " when used with the treated sample."
        ),
    )

    control_file_url = FileUploadField(
        label=_("Control file (Background)"),
        required=False,
        widget=FileUploadWidget(BAM_INPUT_ATTRS),
    )

    use_fdr_cutoff = forms.BooleanField(
        initial=True,
        required=False,
        label=_("Use FDR cutoff"),
        help_text=_("Use FDR-value as cutoff instead of a p-value."),
    )

    # pvalue Pvalue cutoff for peak detection.
    p_value_cutoff = forms.FloatField(
        initial=0.05,
        validators=[MinValueValidator(0)],
        required=False,
        help_text=_(
            "P value cutoff for peak detection. If pvalue cutoff is set, qvalue will "
            "not be calculated and reported as -1 in the final result"
        ),
    )

    # oder qvalue Minimum FDR (q-value) cutoff for peak detection.
    minimum_fdr_cutoff = forms.FloatField(
        initial=0.05,
        validators=[MinValueValidator(0)],
        required=False,
        help_text=_(
            "Minimum false discovery rate (q-value) cutoff for peak detection."
        ),
    )

    # Effective genome size default hs
    exact_genome_size = forms.BooleanField(
        initial=False,
        required=False,
        help_text=_("Specific genome size for, otherwise the given organism is used."),
    )

    genome_size = forms.IntegerField(
        label=_("Genome size in Mbp"),
        initial=270,
        validators=[MinValueValidator(1)],
        required=False,
        help_text=_("Insert the exact genome size in million base pairs."),
    )

    # Footprinting:
    calculate_footprint_scores = forms.BooleanField(
        initial=True,
        required=False,
        help_text=_("Select to calculate footprint scores with HINT."),
    )
    build_genome_tracks = forms.BooleanField(
        initial=False,
        required=False,
        help_text=_("Select to build a genomic profile for visualization."),
    )
    find_matching_motifs = forms.BooleanField(
        initial=True,
        required=False,
        help_text=_(
            "Select to perform motif matching for footprints (source: JASPAR)."
        ),
    )
    build_seq_profiles = forms.BooleanField(
        initial=True,
        required=False,
        help_text=_(
            "Select to generate profiles to inspect Chromatin accessability"
            "around binding sites for TFs."
        ),
    )

    max_number_of_plots = forms.IntegerField(
        initial=4,
        required=False,
        help_text=_(
            "Choose EVEN maximum number of profile-around-motif plots. Will only "
            "show the sequencing profiles around the n most appearing motifs."
        ),
        validators=[MinValueValidator(0), validate_even],
    )

    NUM = "num"
    BITSCORE = "bit"
    MOTIF_RANKING_CHOICES = [
        (NUM, "Number of occurences of motif"),
        (BITSCORE, "BitScore of single motif appearances"),
    ]
    motif_ranking = forms.ChoiceField(choices=MOTIF_RANKING_CHOICES, required=False)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def remove(self, cleaned_data, to_remove):
        """Removes given values in `to_remove` from `cleaned_data`."""

        for remove in to_remove:
            # Sanity checking
            if cleaned_data.get(remove):
                del cleaned_data[remove]

    def handle_field_dependencies(self, cleaned_data, needed_fields, to_remove=[]):
        """Checks if all given fields exist, and throws an error if not.

        Unneeded fields are removed.
        """
        for field in needed_fields:
            if not cleaned_data.get(field[0]):
                self.add_error(field[0], ValidationError(field[1]))

        self.remove(cleaned_data, to_remove)

    def clean(self):
        cleaned_data = super().clean()

        if not cleaned_data.get("assembly"):
            assembly = Assembly.objects.last()
        else:
            assembly = cleaned_data["assembly"]
            del cleaned_data["assembly"]

        # This gets removed in the `PipelineCreateView`
        cleaned_data["assembly_instance"] = assembly

        # Bowtie index folder
        cleaned_data["index_folder"] = str(assembly.get_index_path())
        cleaned_data["r_package_name"] = assembly.r_package_name
        cleaned_data["organism"] = assembly.organism.name
        cleaned_data["assembly"] = assembly.name

        if not cleaned_data.get("exact_genome_size"):
            # Field is in bp, cleaned_data["genome_siz"] initially is not (is Mbp).
            cleaned_data["genome_size"] = str(assembly.organism.genome_size)
        else:
            if not cleaned_data.get("genome_size"):
                self.add_error(
                    "genome_size",
                    ValidationError(_("The genome size needs to be specified.")),
                )
            else:
                cleaned_data["genome_size"] = (
                    str(cleaned_data["genome_size"]) + "000000"
                )

        # Check paired and single ended
        if cleaned_data.get("enable_paired_end"):
            self.handle_field_dependencies(
                cleaned_data,
                (
                    (
                        "forward_readfile_url",
                        _(
                            "The forward read file needs to be specified on "
                            "paired end experiments."
                        ),
                    ),
                    (
                        "reverse_readfile_url",
                        _(
                            "The reverse read file needs to be specified on "
                            "paired end experiments."
                        ),
                    ),
                ),
            )
        else:
            self.handle_field_dependencies(
                cleaned_data,
                (("readfile_url", _("The readfile is required.")),),
            )

        # Check adapter trimming
        if cleaned_data.get("enable_adapter_trimming"):
            # Auto detection of adapters
            if cleaned_data.get("detect_adapters"):
                self.remove(
                    cleaned_data,
                    [
                        "forward_adapter_sequence",
                        "reverse_adapter_sequence",
                        "adapter_sequence",
                    ],
                )
            else:
                # Manually specified adapters
                if cleaned_data.get("enable_paired_end"):
                    self.handle_field_dependencies(
                        cleaned_data,
                        (
                            (
                                ("forward_adapter_sequence", _("Adapter is missing.")),
                                ("reverse_adapter_sequence", _("Adapter is missing.")),
                            )
                        ),
                        ("adapter_sequence",),
                    )
                else:
                    self.handle_field_dependencies(
                        cleaned_data,
                        (("adapter_sequence", _("This field is required.")),),
                        ("forward_adapter_sequence", "reverse_adapter_sequence"),
                    )
        else:
            self.remove(
                cleaned_data,
                [
                    "forward_adapter_sequence",
                    "reverse_adapter_sequence",
                    "adapter_sequence",
                    "detect_adapters",
                ],
            )

        # Check quality filtering
        if cleaned_data.get("enable_quality_filter"):
            self.handle_field_dependencies(
                cleaned_data,
                (
                    (
                        "quality_threshold",
                        _(
                            "The quality threshold needs to be specified as a "
                            "non-negative Integer."
                        ),
                    ),
                ),
            )

        if not cleaned_data.get("enable_paired_end"):
            cleaned_data["enable_unpaired_filter"] = False
            cleaned_data["enable_unproperly_paired_filter"] = False
            cleaned_data["enable_mate_unmapped_filter"] = False
            cleaned_data["enable_shift_filter"] = False

        if not cleaned_data.get("enable_quality_filter"):
            self.remove(cleaned_data, ["quality_threshold"])

        if not cleaned_data.get("calculate_footprint_scores"):
            cleaned_data["find_matching_motifs"] = False
            cleaned_data["build_seq_profiles"] = False

        if cleaned_data.get("use_fdr_cutoff"):
            self.handle_field_dependencies(
                cleaned_data,
                (("minimum_fdr_cutoff", _("The minimum FDR needs to be specified.")),),
                ("p_value_cutoff",),
            ),
        else:
            self.handle_field_dependencies(
                cleaned_data,
                (("p_value_cutoff", _("The p value needs to be specified.")),),
                ("minimum_fdr_cutoff",),
            ),

        return cleaned_data
