from celery import group, shared_task

from halfpipe.models import Pipeline
from halfpipe.pipeline.signals import update_detail_channel
from halfpipe.pipeline.tasks import (
    align,
    finalize,
    footprint_analysis,
    peak_annotate,
    peak_calling,
    post_alignment_qc,
    pre_alignment_qc,
    prepare,
)
from halfpipe.tasks import create_handle_error, create_handle_group_error

handle_failed_in_group = create_handle_group_error(
    Pipeline, update_detail_channel, "halfpipe.pipeline"
)
handle_error = create_handle_error(Pipeline, "halfpipe.pipeline")


@shared_task()
def run_pipeline(options, pipeline_id):
    """Main logic behind the pipeline execution.

    This defines the order of the pipeline steps.
    """

    pipeline = Pipeline.objects.get(pk=pipeline_id)
    pipeline.status = "RUNNING"
    pipeline.save()

    extended = group(
        peak_annotate.s(options, pipeline_id).on_error(
            handle_failed_in_group.s(pk=pipeline_id, field="annotate")
        ),
        footprint_analysis.s(options, pipeline_id).on_error(
            handle_failed_in_group.s(pk=pipeline_id, field="footprinting")
        ),
    )

    # Pipeline always starts with pre aligment qc step
    pipeline = (
        prepare.s(options, pipeline_id)
        | pre_alignment_qc.s(options, pipeline_id)
        | align.s(options, pipeline_id)
        | post_alignment_qc.s(options, pipeline_id)
        | peak_calling.s(options, pipeline_id)
        | extended
        | finalize.s(options, pipeline_id)
    )

    pipeline.apply_async((), link_error=handle_error.s(pk=pipeline_id))
