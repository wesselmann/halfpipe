import logging

from halfpipe import celery_app as app
from halfpipe.models import Pipeline
from halfpipe.pipeline.signals import update_detail_channel
from halfpipe.steps.alignment.build_index_files import run_bowtie
from halfpipe.steps.footprint_analysis.footprinting import run as run_fp
from halfpipe.steps.peak_annotate_single.annotate import run_annotate
from halfpipe.steps.peak_calling.macs3 import run as run_macs3
from halfpipe.steps.post_alignment_qc.post_align_qc import run as run_paqc
from halfpipe.steps.pre_alignment_qc.main import run_pre_alignment_qc
from halfpipe.storage import download_pipeline_form_data
from halfpipe.tasks import cleanup, create_watchdog

logger = logging.getLogger(__name__)
watchdog = create_watchdog(Pipeline, update_detail_channel, "pipeline")


@app.task(bind=True, name="halfpipe.pipeline.pre_alignment_qc")
@watchdog("pre_alignment_qc")
def pre_alignment_qc(self, previous, options, pipeline_id):
    return run_pre_alignment_qc(previous, options, pipeline_id)


@app.task(bind=True, name="halfpipe.pipeline.alignment")
@watchdog("alignment")
def align(self, last, options, pipeline):
    return run_bowtie(last, options, pipeline)


@app.task(bind=True, name="halfpipe.pipeline.post_alignment_qc")
@watchdog("post_alignment_qc")
def post_alignment_qc(self, last, options, pipeline):
    return run_paqc(last, options, pipeline)


@app.task(bind=True, name="halfpipe.pipeline.peak_calling")
@watchdog("peak_calling")
def peak_calling(self, last, options, pipeline):
    return run_macs3(last, options, pipeline)


@app.task(bind=True, name="halfpipe.pipeline.annotate")
@watchdog("annotate")
def peak_annotate(self, last, options, pipeline):
    return run_annotate(last, options, pipeline)


@app.task(bind=True, name="halfpipe.pipeline.footprinting")
@watchdog("footprinting")
def footprint_analysis(self, last, options, pipeline):
    return run_fp(last, options, pipeline)


@app.task(bind=True)
def prepare(self, options, pipeline_id):
    logger.info(f"Preparing pipeline {pipeline_id}")

    result = {}

    pipeline = Pipeline.objects.get(pk=pipeline_id)
    # Download files from storage backend
    if options["enable_paired_end"]:
        result["forward_readfile"] = download_pipeline_form_data(
            pipeline, options["forward_readfile_url"], options
        )
        result["reverse_readfile"] = download_pipeline_form_data(
            pipeline, options["reverse_readfile_url"], options
        )
    else:
        result["readfile"] = download_pipeline_form_data(
            pipeline, options["readfile_url"], options
        )

    return result


@app.task(bind=True)
def finalize(self, last, options, pipeline_id):
    logger.info(f"Finishing pipeline {pipeline_id}")

    pipeline = Pipeline.objects.get(pk=pipeline_id)
    cleanup(pipeline, "SUCCESS")
