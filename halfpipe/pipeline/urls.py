from django.urls import path

from .views import (
    AlignmentView,
    AnnotateView,
    FootprintingView,
    PeakCallingView,
    PipelineCreateView,
    PipelineDeleteView,
    PipelineDetailView,
    PipelineListView,
    PostAlignmentQCView,
    PreAlignmentQCView,
)

app_name = "pipeline"

urlpatterns = [
    path("create", PipelineCreateView.as_view(), name="create"),
    path("list", PipelineListView.as_view(), name="list"),
    path("detail/<int:pk>/delete", PipelineDeleteView.as_view(), name="delete"),
    path("detail/<int:pk>", PipelineDetailView.as_view(), name="detail"),
    path(
        "detail/<int:pk>/pre_alignment_qc",
        PreAlignmentQCView.as_view(),
        name="pre_alignment_qc",
    ),
    path(
        "detail/<int:pk>/alignment",
        AlignmentView.as_view(),
        name="alignment",
    ),
    path(
        "detail/<int:pk>/post_alignment_qc",
        PostAlignmentQCView.as_view(),
        name="post_alignment_qc",
    ),
    path(
        "detail/<int:pk>/peak_calling",
        PeakCallingView.as_view(),
        name="peak_calling",
    ),
    path(
        "detail/<int:pk>/annotate/",
        AnnotateView.as_view(),
        name="annotate",
    ),
    path(
        "detail/<int:pk>/footprinting/",
        FootprintingView.as_view(),
        name="footprinting",
    ),
]
