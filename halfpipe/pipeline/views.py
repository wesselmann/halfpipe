from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic import DetailView, ListView
from django.views.generic.edit import FormView

from halfpipe.models import (
    Alignment,
    Annotate,
    Footprinting,
    PeakCalling,
    Pipeline,
    PostAlignmentQC,
    PreAlignmentQC,
)
from halfpipe.pipeline.forms import PipelineForm, PresetForm
from halfpipe.pipeline.process import run_pipeline
from halfpipe.serializers import PipelineDetailSerializer
from halfpipe.views import ProcessDeleteView

PRESETS = {
    "normal": {},  # Normal uses initial value from fields in the form.
    "strict": {
        "enable_unproperly_paired_filter": True,
        "enable_mate_unmapped_filter": True,
        "quality_threshold": 30,
        # "enable_shift_filter": True, # TODO: Reenable
        "minimum_fdr_cutoff": 0.025,
    },
    "lax": {
        "quality_threshold": 10,
        # "enable_shift_filter": True, # TODO: Reenable
        "minimum_fdr_cutoff": 0.075,
    },
}


class PipelineCreateView(LoginRequiredMixin, FormView):
    """Creates a new pipeline and queues it for processing."""

    form_class = PipelineForm
    template_name = "pipeline/create.html"
    success_url = "/pipeline/list"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        # Append preset form to context
        context["preset_form"] = self.preset_form

        return context

    def get_initial(self):
        # Use preset from url if present
        preset = self.request.GET.get("preset", "normal")
        self.preset_form = PresetForm({"preset": preset})

        if self.preset_form.is_valid():
            return PRESETS[self.preset_form.cleaned_data["preset"]].copy()
        else:
            return {}

    def form_valid(self, form):
        options = form.cleaned_data

        # The instance can't be serialized, and we don't want to break existing api
        assembly = options.pop("assembly_instance")
        note = options.pop("note", "")

        pipeline = Pipeline.objects.create(
            options=options,
            pre_alignment_qc=PreAlignmentQC.objects.create(),
            alignment=Alignment.objects.create(),
            post_alignment_qc=PostAlignmentQC.objects.create(),
            peak_calling=PeakCalling.objects.create(),
            annotate=Annotate.objects.create(),
            footprinting=Footprinting.objects.create(),
            assembly=assembly,
            note=note,
            user=self.request.user,
        )

        # Start the pipeline job
        run_pipeline.apply_async(countdown=5, args=[options, pipeline.pk])

        return super().form_valid(form)


class PipelineListView(ListView):
    """Lists all created pipelines."""

    model = Pipeline
    paginate_by = 25
    template_name = "pipeline/list.html"
    ordering = ["-creation_date"]


class PipelineDeleteView(ProcessDeleteView):
    model = Pipeline
    success_url = "pipeline:list"


class PipelineDetailView(DetailView):
    model = Pipeline
    template_name = "pipeline/detail.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        # Inject serialized pipeline for use in js
        # The same serializer is used for the detailed websocket updates
        context["pipeline_data"] = PipelineDetailSerializer(context["object"]).data

        return context


class PreAlignmentQCView(PipelineDetailView):
    template_name = "pipeline/pre_alignment_qc.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        # Saving some typing.
        # Would be available as `object.pre_alignment_qc.fastp_report`
        context["pre_alignment_qc"] = context["object"].pre_alignment_qc.fastp_report
        return context


class AlignmentView(PipelineDetailView):
    template_name = "pipeline/alignment.html"


class PostAlignmentQCView(PipelineDetailView):
    template_name = "pipeline/post_alignment_qc.html"


class PeakCallingView(PipelineDetailView):
    template_name = "pipeline/peak_calling.html"


class AnnotateView(PipelineDetailView):
    template_name = "pipeline/annotate.html"


class FootprintingView(PipelineDetailView):
    template_name = "pipeline/footprinting.html"
