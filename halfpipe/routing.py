from django.urls import path

from halfpipe.differential.consumers import (
    DifferentialDetailConsumer,
    DifferentialListConsumer,
)
from halfpipe.pipeline.consumers import PipelineDetailConsumer, PipelineListConsumer

websocket_urlpatterns = [
    path("ws/pipeline-list", PipelineListConsumer.as_asgi()),
    path("ws/pipeline-detail/<int:pk>", PipelineDetailConsumer.as_asgi()),
    path("ws/differential-list", DifferentialListConsumer.as_asgi()),
    path("ws/differential-detail/<int:pk>", DifferentialDetailConsumer.as_asgi()),
]
