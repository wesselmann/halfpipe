from rest_framework import serializers

from halfpipe.models import (
    Alignment,
    Annotate,
    Assembly,
    DiffAnalysis,
    DiffChip,
    DiffHint,
    File,
    Footprinting,
    PeakCalling,
    Pipeline,
    PostAlignmentQC,
    PreAlignmentQC,
)


class FileSerializer(serializers.ModelSerializer):
    class Meta:
        model = File
        fields = ["id", "name", "url", "file_type"]


class AssemblySerializer(serializers.ModelSerializer):
    text = serializers.CharField(source="__str__", read_only=True)

    class Meta:
        model = Assembly
        fields = ["id", "information", "text"]


class PipelineListSerializer(serializers.ModelSerializer):
    assembly = AssemblySerializer()

    class Meta:
        model = Pipeline
        fields = ["id", "creation_date", "status", "assembly", "note"]


class PreAlignmentQCSerializer(serializers.ModelSerializer):
    class Meta:
        model = PreAlignmentQC
        fields = ["status"]


class AlignmentSerializer(serializers.ModelSerializer):
    aligned_reads = serializers.SerializerMethodField()

    class Meta:
        model = Alignment
        exclude = ["analysis_plot"]

    def get_aligned_reads(self, object):
        return (
            object.exactly_one_align
            + object.more_than_one
            + object.discord_aligned_reads
            + (object.only_one_mate_aligned / 2)
        )


class PostAlignmentQCSerializer(serializers.ModelSerializer):
    class Meta:
        model = PostAlignmentQC
        exclude = ["plots"]


class PeakCallingSerializer(serializers.ModelSerializer):
    class Meta:
        model = PeakCalling
        fields = ["status"]


class AnnotateSerializer(serializers.ModelSerializer):
    class Meta:
        model = Annotate
        fields = "__all__"


class FootprintingSerializer(serializers.ModelSerializer):
    class Meta:
        model = Footprinting
        fields = ["status"]


class PipelineDetailSerializer(serializers.ModelSerializer):
    pre_alignment_qc = PreAlignmentQCSerializer()
    alignment = AlignmentSerializer()
    post_alignment_qc = PostAlignmentQCSerializer()
    peak_calling = PeakCallingSerializer()
    annotate = AnnotateSerializer()
    footprinting = FootprintingSerializer()

    duration = serializers.DurationField(source="get_duration", read_only=True)
    traffic_url = serializers.SerializerMethodField()

    pairness = serializers.SerializerMethodField()

    files = FileSerializer(many=True)

    class Meta:
        model = Pipeline
        fields = [
            "id",
            "creation_date",
            "status",
            "pre_alignment_qc",
            "alignment",
            "post_alignment_qc",
            "peak_calling",
            "annotate",
            "footprinting",
            "files",
            "duration",
            "traffic_url",
            "pairness",
            "note",
        ]

    def get_traffic_url(self, obj):
        return obj.get_score()[1]

    def get_pairness(self, obj):
        return obj.options.get("enable_paired_end", "unspecified")


class DiffHintSerializer(serializers.ModelSerializer):
    class Meta:
        model = DiffHint
        fields = ["status"]


class DiffChipSerializer(serializers.ModelSerializer):
    class Meta:
        model = DiffChip
        fields = ["status"]


class DiffAnalysisBaseSerializer(serializers.ModelSerializer):
    assembly = AssemblySerializer(source="get_assembly", read_only=True)
    count = serializers.SerializerMethodField()

    class Meta:
        model = DiffAnalysis
        fields = ["id", "creation_date", "status", "assembly", "count", "note"]

    def get_count(self, obj):
        return obj.pipelines.count()


class DiffAnalysisDetailSerializer(DiffAnalysisBaseSerializer):
    diff_chip = DiffChipSerializer()
    diff_hint = DiffHintSerializer()

    duration = serializers.DurationField(source="get_duration", read_only=True)
    files = FileSerializer(many=True)

    class Meta:
        model = DiffAnalysis
        fields = [
            "id",
            "creation_date",
            "status",
            "assembly",
            "count",
            "diff_chip",
            "diff_hint",
            "duration",
            "files",
            "note",
        ]


class DiffAnalysisListSerializer(DiffAnalysisBaseSerializer):
    pass
