// Needs script update_detail.js for pipeline object!
// const pipeline = JSON.parse(document.getElementById("pipeline_data").textContent)

// Filtered reads plot
const filteredReadsCtx = document.getElementById("bowtie2_data")
const v = pipeline.alignment
const chart = new Chart(filteredReadsCtx, {
    type: "bar",
    data: {
        labels: ["total reads", "aligned reads", "aligned concord. exactly 1 time",
        "aligned concord. >1 time", "aligned concordantly 0 times", "algined disconcordantly",
        "did not align", "only one mate aligned", "no mate aligned",],
        datasets: [{
            backgroundColor: "rgba(20, 50, 180, 0.5)",
            label: "Bowtie2 Mapping Information",
            data: [
                v.all_reads, v.aligned_reads, v.exactly_one_align, v.more_than_one,
                v.zero_aligned_reads, v.discord_aligned_reads, v.never_aligned_reads,
                v.only_one_mate_aligned, v.no_mate_aligned,
            ]
        }]
    },
    options: {
        responsive: true,
        maintainAspectRatio: false,
        aspectRatio: 1,
        indexAxis: "y",
    },
})