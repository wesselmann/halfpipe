class PipelineCreateForm {
  constructor () {
    this.presetSelect = document.getElementById("id_preset")

    this.hasChanged = false
    document.getElementById("pipelineForm").addEventListener("change", (event) => this.hasChanged = true, { once: true })

    this.presetSelect.addEventListener("change", (event) => this.presetChange(event.target.value))

    // Matrix of fields other fields need to be enabled
    this.matrix = {
      forward_readfile_url: {
        enable_paired_end: true,
      },
      reverse_readfile_url: {
        enable_paired_end: true
      },
      readfile_url: {
        enable_paired_end: false
      },
      detect_adapters: {
        enable_adapter_trimming: true,
      },
      adapter_sequence: {
        enable_adapter_trimming: true,
        detect_adapters: false,
        enable_paired_end: false
      },
      forward_adapter_sequence: {
        enable_adapter_trimming: true,
        detect_adapters: false,
        enable_paired_end: true
      },
      reverse_adapter_sequence: {
        enable_adapter_trimming: true,
        detect_adapters: false,
        enable_paired_end: true
      },
      enable_unpaired_filter: {
        enable_paired_end: true
      },
      enable_unproperly_paired_filter: {
        enable_unpaired_filter: true,
        enable_paired_end: true
      },
      enable_mate_unmapped_filter: {
        enable_unpaired_filter: true,
        enable_paired_end: true
      },
      quality_threshold: {
        enable_quality_filter: true
      },
      enable_shift_filter: {
        enable_paired_end: true
      },
      control_file_url: {
        control_bam_file: true
      },
      genome_size: {
        exact_genome_size: true
      },
      minimum_fdr_cutoff: {
        use_fdr_cutoff: true
      },
      p_value_cutoff: {
        use_fdr_cutoff: false
      },
      find_matching_motifs: {
        calculate_footprint_scores: true
      },
      build_seq_profiles: {
        calculate_footprint_scores: true,
        find_matching_motifs: true
      },
      motif_ranking: {
        calculate_footprint_scores: true,
        find_matching_motifs: true,
        build_seq_profiles: true
      },
      max_number_of_plots: {
        calculate_footprint_scores: true,
        find_matching_motifs: true,
        build_seq_profiles: true
      }
    }

    // List of all fields that effect other fields
    const reactiveFields = ["enable_paired_end", "detect_adapters", "enable_adapter_trimming",
                            "enable_unpaired_filter", "enable_quality_filter", "enable_mitochondrial_filter",
                            "enable_unmapped_filter", "control_bam_file", "exact_genome_size",
                            "calculate_footprint_scores", "find_matching_motifs", "build_seq_profiles",
                            "use_fdr_cutoff"]

    this.fields = {}
    for (const field of reactiveFields) {
      this.fields[field] = document.getElementById("id_" + field)
      this.fields[field].addEventListener("change", (event) => this.handleChange())
    }

    this.handleChange()
  }

  presetChange (value) {
    // Show warning modal if the form changed prior to preset change
    if (this.hasChanged) {
      document.getElementById("confirmedSelectChange").value = value
      const modal = new bootstrap.Modal(document.getElementById("confirmedSelectModal"))
      modal.toggle()
    } else {
      const path = `/pipeline/create?preset=${value}`
      window.location.replace(path)
    }
  }

  // Returns the fields values of all fields in this.fields as booleans
  getCurrent () {
    const result = {}
    for (const [name, input] of Object.entries(this.fields)) {
      result[name] = input.checked
    }

    return result
  }

  handleChange () {
    const current = this.getCurrent()

    let flag
    for (const [name, field] of Object.entries(this.matrix)) {
      flag = true

      // Check if there is an corresponding entry in current for each entry in the matrix
      for (const [key, value] of Object.entries(field)) {
        if (current[key] != value) {
          flag = false
          break
        }
      }

      this.setField(name, flag)
    }
  }

  // Sets the visibility of a field
  setField(name, status) {
    const field = document.getElementById(name)
    field.style.display = status ? "" : "none"
  }
}

const form = new PipelineCreateForm()
