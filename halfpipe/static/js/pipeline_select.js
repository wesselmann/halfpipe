class Select {
  constructor () {
    this.fromPipelines = []
    this.toPipelines = []
    this.assembly = null

    this.elements = {
      fromTable: document.getElementById("fromPipelines"),
      toTable: document.getElementById("toPipelines"),
      input: document.getElementById("pipelineInput"),
      toCard: document.getElementById("toCard")
    }

    this.initialize()
  }

  async initialize() {
    const initial = JSON.parse(document.getElementById("select").textContent)
    // We know there was an error
    if (initial) {
      // Color card
      this.elements.toCard.className = "card h-100 border-danger"

      // Remember assembly, for fetching
      this.assembly = initial.assembly
      await this.fetchFromPipelines(true)

      // Add all added pipelines again
      for (const id of initial.pipelines) {
        // Bad runtime? GTA-Style
        const pipeline = this.fromPipelines.filter((value) => value.id === id)[0]
        this.addTo(pipeline, null)
      }

      // Update from table
      this.updateFrom()
    } else {
      this.fetchFromPipelines()
    }
  }

  async fetchFromPipelines(noUpdate) {
    const params = {
      status: "SUCCESS",
    }

    if (this.assembly) {
      params.assembly = this.assembly
    }

    const url = new URL(window.location.origin + "/api/pipelines")
    url.search = new URLSearchParams(params).toString()

    const result = await fetch(url)
    this.fromPipelines = (await result.json()).results

    // Remove the pipeline that was freshly added to `toPipelines` from possibilities
    const exclude = this.toPipelines[0]
    if (exclude) {
      this.fromPipelines = this.fromPipelines.filter((value) => value.id != exclude.id)
    }

    if (!noUpdate) {
      this.updateFrom()
    }
  }

  // Adds a new row to a table and removes from another, if given
  addRow(pipeline, newTable, old, oldTable) {
    if (old && oldTable) {
      oldTable.deleteRow(old.rowIndex)
    }

    const rows = newTable.rows
    const row = newTable.insertRow(rows.length)
    row.insertCell(0).innerHTML = pipeline.id
    row.insertCell(1).innerHTML = pipeline.assembly.text
    row.insertCell(2).innerHTML = pipeline.note || ""

    return row
  }

  // Adds a button with a callback to a given cell
  addButton(row, callback, text) {
    const buttonCell = row.insertCell(3)
    const button = document.createElement("button")
    button.type = "button"
    button.className = "btn btn-primary btn-sm"
    button.innerHTML = text
    buttonCell.append(button)
    button.addEventListener("click", callback, { once: true })
  }

  // Remove a pipeline from a given array
  remove(array, pipeline) {
    const index  = array.indexOf(pipeline)
    if (index != -1) array.splice(index, 1)
  }

  // Add a pipeline to the selected list
  addTo(pipeline, row) {
    this.toPipelines.push(pipeline)
    this.remove(this.fromPipelines, pipeline)

    // If this is the first element, restrict input choices to assembly
    if (this.toPipelines.length === 1 && row) {
      this.assembly = pipeline.assembly.id
      this.fetchFromPipelines()
      row = null
    } else if (this.assembly != pipeline.assembly.id && row) {
      // We said no to multi assembly... sanity check
      console.warn("Assembly does not correspond to to current one. sadface.")
      return
    }

    const created = this.addRow(
      pipeline, this.elements.toTable,
      row, this.elements.fromTable,
    )

    this.addButton(created, () => this.removeTo(pipeline, created), "-")
    this.updateOutField()
  }

  // Unselect pipeline
  removeTo(pipeline, row) {
    this.fromPipelines.push(pipeline)
    this.remove(this.toPipelines, pipeline)

    // Lift assembly restriction
    if (this.toPipelines.length === 0) {
      this.assembly = null
      this.fetchFromPipelines()
    }

    const created = this.addRow(
      pipeline, this.elements.fromTable,
      row, this.elements.toTable,
    )

    this.addButton(created, () => this.addTo(pipeline, created), "+")
    this.updateOutField()
  }

  updateFrom() {
    const length = this.elements.fromTable.rows.length
    for (let i = 1; i < length; i++) {
      this.elements.fromTable.deleteRow(1)
    }

    for (const pipeline of this.fromPipelines) {
      const row = this.addRow(pipeline, this.elements.fromTable, null, null)
      this.addButton(row, () => this.addTo(pipeline, row), "+")
    }
  }

  updateOutField() {
    this.elements.input.value = JSON.stringify({
      pipelines: this.toPipelines.map((p) => p.id),
      assembly: this.assembly
    })
  }
}

const select = new Select()
