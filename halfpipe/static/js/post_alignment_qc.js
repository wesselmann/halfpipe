// Needs script update_detail.js for pipeline object!
// const pipeline = JSON.parse(document.getElementById("pipeline_data").textContent)

// Filtered reads plot
const filteredReadsCtx = document.getElementById("filtered_reads")
const v = pipeline.post_alignment_qc
var total;
if( pipeline.pairness ) {
    total = pipeline.alignment.all_reads * 2;
} else {
    total = pipeline.alignment.all_reads;
}
const chart = new Chart(filteredReadsCtx, {
    type: "bar",
    data: {
        labels: ["Secondary", "Supplementary", "Unmapped", "Unpaired", "Truly unpaired",
            "Improperly paired", "Mate unmapped", "Quality failed", "Duplicated", "Mitochondrial",
            "(Ref: Total number of all reads)"],
        datasets: [{
            backgroundColor: ["rgba(20, 50, 180, 0.5)", "rgba(20, 50, 180, 0.5)", "rgba(20, 50, 180, 0.5)"
            , "rgba(20, 50, 180, 0.5)", "rgba(20, 50, 180, 0.5)", "rgba(20, 50, 180, 0.5)", "rgba(20, 50, 180, 0.5)"
            , "rgba(20, 50, 180, 0.5)", "rgba(20, 50, 180, 0.5)", "rgba(20, 50, 180, 0.5)", "rgba(254, 94, 80, 0.75)"],
            label: ("Column 1-10: Number of filtered reads out of " + total + " reads"),
            data: [v.diff_nonprim, v.diff_supp, v.diff_unmapp, v.diff_unpaired, v.diff_truly_unpaired,
                v.diff_unprop, v.diff_mate_unmapp, v.diff_quality, v.diff_dup, v.diff_mito, total]
        }]
    },
    options: {

        scales: {
            y: {
                type: "logarithmic"
            }
        },
        responsive: true,
        maintainAspectRatio: false,
        aspectRatio: 1,
    },
})