// Color setup
const BAR_COLOR = "rgba(19, 54, 171, 0.5)"
const BASE_A_COLOR = "#ff2b08"
const BASE_T_COLOR = "#00bc0d"
const BASE_C_COLOR = "#009aff"
const BASE_G_COLOR = "#ffb507"

// Returns last index of value that is not 0
const trimRightIndex = (data) => {
  for (let i = data.length - 1; i >= 0; i--) {
    if (data[i] !== 0) {
      return i
    }
  }
}

// Returns first index of value that is not 0
const trimLeftIndex = (data) => {
  for (let i = 0; i < data.length; i++) {
    if (data[i] !== 0) {
      return i
    }
  }
}

// Calculates the difference between two numerical arrays
const calculateDiff = (a, b) => {
  result = []
  for (let i = 0; i < a.length; i++) {
    result.push(a[i] - b[i])
  }

  return result
}

// Plots the all bases per position
const qualityCurvePlot = (data, ctx, title) => {
  const datasets = [
    {
      borderColor: BASE_A_COLOR,
      label: "A",
      data: data.A,
    },
    {
      borderColor: BASE_T_COLOR,
      label: "T",
      data: data.T,
    },
    {
      borderColor: BASE_C_COLOR,
      label: "C",
      data: data.C,
    },
    {
      borderColor: BASE_G_COLOR,
      label: "G",
      data: data.G,
    },
  ]

  if (data.N) {
    datasets.push({
      label: "N",
      data: data.N
    })
  }

  if (data.GC) {
    datasets.push({
      label: "GC",
      data: data.GC
    })
  }

  if (data.mean) {
    datasets.push({
      label: "mean",
      data: data.mean
    })
  }

  return new Chart(ctx, {
    type: 'line',
    data: {
      labels: Array.from(Array(data.A.length).keys()),
      datasets: datasets,
    },
    options: {
      responsive: true,
      maintainAspectRatio: false,
      aspectRatio: 1,
      scales: {
        x: {
          scaleLabel: { display: true, labelString: "nth base from read start"}
        }
      },
      plugins: {
        title: {
          display: true,
          text: title
        }
      }
    }
  })
}

// Fetch data from script tag
const data = JSON.parse(document.getElementById("pre-alignment-qc-data").textContent)

// Duplication Chart
const duplicationCtx = document.getElementById("duplication_histogram")
const duplicationData = data.duplication.histogram.slice(0, trimRightIndex(data.duplication.histogram) + 15)
const duplicationChart = new Chart(duplicationCtx, {
  type: 'bar',
  data: {
    labels: Array.from(Array(duplicationData.length).keys()),
    datasets: [{
      backgroundColor: BAR_COLOR,
      label: "Duplicated reads",
      data: duplicationData,
    }]
  },
  options: {
    scales: {
      y: {
        type: 'logarithmic'
      }
    }
  }
})

// Mean gc Chart
const meanGCCtx = document.getElementById("mean_gc")
const meanGCData = data.duplication.mean_gc.slice(0, trimRightIndex(data.duplication.mean_gc) + 15)
const meanGCChart = new Chart(meanGCCtx, {
  type: 'line',
  data: {
    labels: Array.from(Array(meanGCData.length).keys()),
    datasets: [{
      backgroundColor: BAR_COLOR,
      label: "Mean gc",
      data: meanGCData.map(x => x === 0 ? NaN : x),
      spanGaps: true,
      cubicInterpolationMode: "default",
      lineTension: 0.3,
    }]
  },
})

// Insert Size chart
if (data.insert_size != undefined) {
  const insertSize = data.insert_size.histogram
  const trimmedInsertSize = insertSize.slice(
    Math.max(0, trimLeftIndex(insertSize) - 10),
    Math.min(insertSize.length, trimRightIndex(insertSize) + 10)
  )

  const insertSizeCtx = document.getElementById("insert_size")
  const insertSizeChart = new Chart(insertSizeCtx, {
    type: 'bar',
    data: {
      responsive: true,
      maintainAspectRatio: false,
      aspectRatio: 1,
      labels: Array.from(Array(trimmedInsertSize.length).keys()),
      datasets: [{
        backgroundColor: BAR_COLOR,
        label: "Insert size",
        data: trimmedInsertSize
      }]
    }
  })
}

const readQualityBlock = (before, after, diffCtx, beforeCtx, afterCtx) => {
  const diffQuality = (b, a) => {
    return {
      A: calculateDiff(a.A, b.A),
      T: calculateDiff(a.T, b.T),
      C: calculateDiff(a.C, b.C),
      G: calculateDiff(a.G, b.G),
      mean: calculateDiff(a.mean, b.mean),
    }
  }

  const diff = diffQuality(before, after)
  qualityCurvePlot(diff, diffCtx, "Quality difference")
  qualityCurvePlot(before, beforeCtx, "Quality before filtering")
  qualityCurvePlot(after, afterCtx, "Quality after filtering")
}

const readQualityDiffCtx = document.getElementById("quality_curve_one_diff")
const readOneQualityCtx = document.getElementById("quality_curve_one_prefilter")
const readOneQualityFilteredCtx = document.getElementById("quality_curve_one_filtered")
readQualityBlock(data.read1_before_filtering.quality_curves, data.read1_after_filtering.quality_curves, readQualityDiffCtx, readOneQualityCtx, readOneQualityFilteredCtx)


if (data.read2_before_filtering) {
  const readQualityDiffCtx = document.getElementById("quality_curve_two_diff")
  const readOneQualityCtx = document.getElementById("quality_curve_two_prefilter")
  const readOneQualityFilteredCtx = document.getElementById("quality_curve_two_filtered")
  readQualityBlock(data.read2_before_filtering.quality_curves, data.read2_after_filtering.quality_curves, readQualityDiffCtx, readOneQualityCtx, readOneQualityFilteredCtx)
}

const readOneContentCtx = document.getElementById("content_curve_after")
qualityCurvePlot(data.read1_after_filtering.content_curves, readOneContentCtx, "Content curve")
