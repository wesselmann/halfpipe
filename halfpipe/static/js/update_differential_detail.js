// Fetch pipeline from document
const differential = JSON.parse(document.getElementById("differential_data").textContent)

// States in which the `Details` button on the steps are enabled
const STEPS = [
  "diff_hint",
  "diff_chip"
]

new DetailUpdate(differential, `ws/differential-detail/${differential.id}`, STEPS)
