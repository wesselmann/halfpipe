class DifferentialList extends ListUpdate {
  constructor () {
    super()

    this.table = document.getElementById("diffTable")

    this.initialColoring()

    const path = getWebsocketBasePath('ws/differential-list')
    this.ws = new WebSocket(path)
    this.ws.onmessage = (event) => { this.update(JSON.parse(event.data)) }
  }

  update(diff) {
    if (diff.created) {
      const row = this.table.insertRow(1)
      row.id = `diff_${diff.instance.id}`

      this.pkCell(row, diff.instance, 0)
      this.statusCell(row, diff.instance, 1)

      const countCell = row.insertCell(2)
      countCell.innerHTML = diff.instance.count

      // We can't set assembly just yet, the pipelines are m2m related
      const assemblyCell = row.insertCell(3)
      assemblyCell.innerHTML = ""

      this.dateCell(row, diff.instance, 4)
      this.noteCell(row, diff.instance, 5)

      const actionCell = row.insertCell(6)
      actionCell.innerHTML = `<a href="/diff_analysis/detail/${diff.instance.id}">Detail</a>`

      // Next page
      if (this.table.rows.length > 26) {
        this.table.deleteRow(26)
      }
    } else {
      const row = this.table.rows.namedItem(`diff_${diff.instance.id}`)

      const status_cell = row.cells[1]
      status_cell.firstChild.innerHTML = diff.instance.status

      const countCell = row.cells[2]
      countCell.innerHTML = diff.instance.count

      const assemblyCell = row.cells[3]
      assemblyCell.innerHTML = diff.instance.assembly.text

      this.updateCellColor(status_cell)
    }
  }
}

new DifferentialList()
