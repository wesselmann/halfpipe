// Fetch pipeline from document
const pipeline = JSON.parse(document.getElementById("pipeline_data").textContent)

// States in which the `Details` button on the steps are enabled
const STEPS = [
  "pre_alignment_qc",
  "alignment",
  "post_alignment_qc",
  "peak_calling",
  "annotate",
  "footprinting"
]

class PipelineDetailUpdate extends DetailUpdate {
  constructor (data, url, steps) {
      super(data, url, steps)
  }

  update() {
    super.update()

    const traffic = document.getElementById('traffic')
    traffic.src = this.data.traffic_url
  }
}


new PipelineDetailUpdate(pipeline, `ws/pipeline-detail/${pipeline.id}`, STEPS)
