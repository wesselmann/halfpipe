class PipelineList extends ListUpdate {
  constructor () {
    super()

    this.table = document.getElementById("pipeline_table")

    this.initialColoring()

    const path = getWebsocketBasePath('ws/pipeline-list')
    this.ws = new WebSocket(path)
    this.ws.onmessage = (event) => { this.update(JSON.parse(event.data)) }
  }

  update(pipeline) {
    if (pipeline.created) {
      const row = this.table.insertRow(1)
      row.id = `pipeline_${pipeline.instance.id}`

      this.pkCell(row, pipeline.instance, 0)
      this.statusCell(row, pipeline.instance, 1)
      this.assemblyCell(row, pipeline.instance, 2)
      this.dateCell(row, pipeline.instance, 3)
      this.noteCell(row, pipeline.instance, 4)

      const actionCell = row.insertCell(5)
      actionCell.innerHTML = `<a href="/pipeline/detail/${pipeline.instance.id}">Detail</a>`

      // Next page
      if (this.table.rows.length > 26) {
        this.table.deleteRow(26)
      }
    } else {
      const row = this.table.rows.namedItem(`pipeline_${pipeline.instance.id}`)

      const status_cell = row.cells[1]
      status_cell.firstChild.innerHTML = pipeline.instance.status
      this.updateCellColor(status_cell)
    }
  }
}

new PipelineList()
