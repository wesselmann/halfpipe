const SUCCESS = 0
const PENDING = 1
const FAILURE = 2

class FileUploadWidget {
  /**
   * Creates a new file upload widget.
   *
   * The given file is uploaded directly to S3 and the url is included in the form.
  */
  constructor(idPrefix) {
    this.idPrefix = idPrefix

    this.hiddenFormInput = document.getElementById("id_" + this.idPrefix)
    this.fileInput = document.getElementById(this.idPrefix + "_input")

    this.progressIndicator = document.getElementById(this.idPrefix + "_progress")
    this.statusIndicator = document.getElementById(this.idPrefix + "_status")

    this.retryButton = document.getElementById(this.idPrefix + "_retry")

    // Upload if the input changes
    this.fileInput.addEventListener('change', (event) => {
      this.upload()
    })

    // Upload if the retry button is pressed
    this.retryButton.addEventListener('click', (event) => {
      this.upload()
    })

    // Url is requested from the backend before starting the upload
    this.url = null

    // If the form was submitted but there were errors with other field we need to
    // display the already uploaded filename somewhere.
    this.funkyChange = document.getElementById(this.idPrefix + "_funkychange")
  }

  // Returns the first file of the input
  getFile () {
    return this.fileInput.files[0]
  }

  // Unlocks the whole form, using a semaphore like property on the submit button
  // This is done to prevent multiple concurrent uploads to unlock the buttons before
  // the other uploads succeeded
  unlockFormSubmit () {
    if (--this.submitButton.metadata == 0) {
      this.submitButton.disabled = false
    }
  }

  // Locks the submit button. See `unlockFormSubmit`
  lockFormSubmit () {
    this.submitButton.disabled = true
    if (this.submitButton.metadata === undefined) {
      this.submitButton.metadata = 1
    } else {
      // sanity
      this.submitButton.disabled = true
      this.submitButton.metadata++
    }
  }


  // Reflects all state changes in the UI
  setStatus ( status ) {
    let text
    let css

    switch (status) {
      case SUCCESS:
        const result = {
          url: this.url.split("?")[0],
          name: this.getFile().name
        }

        // Add the url and the filename to the form input
        this.hiddenFormInput.value = JSON.stringify(result)

        this.retryButton.disabled = true
        this.unlockFormSubmit()

        text = "SUCCESS"
        css = "text-success"
        break
      case PENDING:
        this.retryButton.disabled = true
        this.lockFormSubmit()

        this.hiddenFormInput.value = null

        text = "LOADING"
        css = "text-secondary"
        break
      case FAILURE:
        this.retryButton.disabled = false
        this.unlockFormSubmit()

        text = "FAILURE"
        css = "text-danger"
        break
    }

    this.statusIndicator.innerHTML = text
    this.statusIndicator.className = "input-group-text fw-bold " + css
  }

  // Gets a presigned url for the file to upload from the backend
  async getURL () {
    try {
      const response = await fetch(
        "/api/get_presigned_url"
      )

      if (response.ok) {
        return await response.text()
      } else {
        this.setStatus(FAILURE)
      }
    } catch (error) {
      this.setStatus(FAILURE)
    }
  }

  // Submits the file to the S3 storage api
  async upload () {
    // Submit button is not initialized when the constructor is called, therefore it's deferred until the upload input get's clicked.
    this.submitButton = document.getElementById("formSubmit")

    // Clear current funky value, after form errors
    if (this.funkyChange) {
      this.funkyChange.remove()
    }

    // Setup progress bar
    this.url = null
    this.setStatus(PENDING)
    this.progressIndicator.className = "input-group-text"
    this.progressIndicator.innerHTML = "0%"

    const xhr = new XMLHttpRequest()

    xhr.onerror = (event) => {
      this.setStatus(FAILURE)
    }

    xhr.onload = (event) => {
      this.setStatus(SUCCESS)
    }

    xhr.upload.onprogress = (event) => {
      if (event.lengthComputable) {
        const value = Math.floor((event.loaded / event.total) * 100)
        this.progressIndicator.innerHTML = `${value}%`
      }
    }

    // Fetch the presigned url from the backend
    this.url = await this.getURL()
    if (this.url) {
      xhr.open("PUT", this.url)
      xhr.send(this.getFile())
    }
  }
}
