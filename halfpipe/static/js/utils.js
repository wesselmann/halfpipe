// Returns the given path joined on the protocol and host for websocket connections
const getWebsocketBasePath = (path) => {
  const tls = window.location.protocol === "https:"
  const protocol = tls ? "wss://" : "ws://"
  const host = window.location.host

  return `${protocol}/${host}/${path}`
}

// Status for each pipeline step
const STATUS_MAPPING = {
  "SUCCESS": "text-white bg-success",
  "PENDING": "text-white bg-secondary",
  "FAILURE": "text-white bg-danger",
  "RUNNING": "text-white bg-primary",
  "CANCELLED": "text-dark bg-warning"
}

// End states for enbaled cards
const END_STATES = ["SUCCESS", "FAILURE"]

// Id mapping for filetabs
const FILE_TAB_MAPPING = {
  "DEFAULT": "default-files",
  "PLOT": "plot-files",
  "USER": "user-files",
}


// Updates a list
class ListUpdate {
  // Updates the badge color based on the status
  updateCellColor(cell) {
    cell.firstChild.className = "badge " + STATUS_MAPPING[cell.firstChild.innerHTML]
  }

  // Sets the initial coloring of each pipeline
  initialColoring() {
    for (let i = 1; i < this.table.rows.length; i++) {
      this.updateCellColor(this.table.rows[i].cells[1])
    }
  }

  pkCell(row, instance, index) {
    row.insertCell(index).innerHTML = instance.id
  }

  noteCell(row, instance, index) {
    row.insertCell(index).innerHTML = instance.note
  }

  statusCell(row, instance, index) {
    const statusCell = row.insertCell(index)
    statusCell.innerHTML = "<span>" + instance.status + "</span>"
    this.updateCellColor(statusCell)
  }

  assemblyCell(row, instance, index) {
    const assemblyCell = row.insertCell(index)
    assemblyCell.innerHTML = instance.assembly.text
  }

  dateCell(row, instance, index) {
    const dateCell = row.insertCell(index)
    dateCell.innerHTML = instance.creation_date.replace("Z", "+00:00")
  }
}


// Updates multiple file tables in their respective tabs
class FileTable {
  constructor () {
    this.currentFiles = []
  }

  // Appends new files to the given file table
  appendFileTable (tab, file) {
    const row = tab[0].insertRow(1)

    const nameCell = row.insertCell(0)
    const urlCell = row.insertCell(1)

    nameCell.innerHTML = file.name
    urlCell.innerHTML = `<a href="${file.url}">${file.url}</a>`

    // Remove "no data sign"
    if (tab[1]) {
      tab[0].className = "table"
      tab[1].remove()
    }
  }

  // Updates the file list
  updateFileList (files) {
    if (!files)
      return

    const elements = {}

    // Find previously unfilled tables
    for (const [key, value] of Object.entries(FILE_TAB_MAPPING)) {
      elements[key] = [document.getElementById(value), document.getElementById(value + "-nodata")]
    }

    for (let file of files) {
      if (!this.currentFiles.includes(file.url)) {
        this.appendFileTable(elements[file.file_type.toUpperCase()], file)
        this.currentFiles.push(file.url)
      }
    }
  }
}

class DetailUpdate {
  constructor (data, url, steps) {
    this.data = data
    this.steps = steps

    this.elements = {
      duration: document.getElementById('duration'),
      status: document.getElementById('status')
    }

    // TODO: Websocket wrapper, for reconnect
    // TODO: The websocket is only needed if the run is not finished.
    const path = getWebsocketBasePath(url)
    this.ws = new WebSocket(path)
    this.ws.onmessage = (event) => {
      this.data = JSON.parse(event.data)
      this.update()
    }

    // We don't create a timer if the run already terminated
    if (!END_STATES.includes(this.data.status)) {
      this.updateTime()
      this.durationTimer = setInterval(this.updateTime.bind(this), 1000)
    }

    // initialize file tables
    this.fileTable = new FileTable()

    this.update()
  }

  // Calculates the time since the run started
  recalculateTime () {
    let ms = Date.now() - new Date(this.data.creation_date)

    // We always want double digits
    const h = ("0" + (Math.floor(ms / 3600000))).slice(-2)
    const m = ("0" + (Math.floor(ms / 60000) % 60)).slice(-2)
    const s = ("0" + (Math.floor(ms / 1000) % 60)).slice(-2)

    return `${h}:${m}:${s}`
  }

  updateTime () {
    this.elements.duration.innerHTML = this.recalculateTime()
  }

  update () {
    for (const step of this.steps) {
      this.updateCard(step, this.data[step].status)
    }

    this.fileTable.updateFileList(this.data.files)
    this.updateRunStatus()

    // Clear interval on pipeline termination
    if (END_STATES.includes(this.data.status)) {
      clearInterval(this.durationTimer)
      this.elements.duration.innerHTML = this.data.duration.split(".")[0]
    }
  }

  // Updates the text of the cards
  updateCard (name, status) {
    const card = document.getElementById(`step_${name}`)
    const cardText = document.getElementById(`status_text_${name}`)

    card.className = `card ${STATUS_MAPPING[status]}`
    cardText.innerHTML = status

    // If the step is one of `END_STATES`, enable the detail page
    const button = document.getElementById(`step_button_${name}`)
    let className = "btn btn-primary float-end"
    if (!END_STATES.includes(status)) {
      className += " disabled"
    }

    button.className = className
  }

  updateRunStatus() {
    this.elements.status.innerHTML = this.data.status
    this.elements.status.className = 'badge ' + STATUS_MAPPING[this.data.status]
  }
}
