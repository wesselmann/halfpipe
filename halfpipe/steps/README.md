# Step documentation

## Preaalignment QC
Preprocessing of the reads is done by [fastp](https://github.com/OpenGene/fastp). Steps include

- Adapter trimming, which can be detected or entered manually.
- Global trimming
- Quality filtering
- Length filtering
- Complexity filtering
