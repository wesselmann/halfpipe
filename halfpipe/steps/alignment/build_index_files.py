import os
import subprocess
from pathlib import Path

from django.conf import settings

from halfpipe.models import Pipeline
from halfpipe.steps.alignment import plot_mapping_info
from halfpipe.storage import cleanup, upload_to_storage


class AlignmentStep:
    def __init__(self, previous, options, pipeline_id):
        self.data = previous
        self.options = options
        self.pipeline_id = pipeline_id

        self.pipeline = Pipeline.objects.get(pk=pipeline_id)

    def build_bam(self):
        index_path = self.options["index_folder"]
        index_name = Path(index_path).name

        arguments = [settings.BIO_BOWTIE_BINARY]
        if self.options["enable_paired_end"]:
            fasta_fw = self.data["processed_forward_readfile"]
            fasta_rv = self.data["processed_reverse_readfile"]
            arguments += ["-x", index_name, "-1", fasta_fw, "-2", fasta_rv]
        else:
            fasta = self.data["processed_readfile"]
            arguments += ["-x", index_name, "-U", fasta]

        # Assemble output paths
        bam_file = self.pipeline.get_pipeline_path() / "alignment.bam"
        sam_file = str(self.pipeline.get_pipeline_path() / "out.sam")

        # Append bamfile to result for next step
        self.data["bamfile"] = "alignment.bam"

        # Add sam output file to arguments
        arguments += ["-S", sam_file]

        # Add thread number
        arguments += ["-p", str(settings.BIO_THREADS)]

        # Add bowtie index variable to env
        env = os.environ.copy()
        env["BOWTIE2_INDEXES"] = index_path

        # call bowtie and save stderr
        result = subprocess.run(
            arguments,
            stderr=subprocess.PIPE,
            stdout=subprocess.PIPE,
            universal_newlines=True,
            env=env,
        )

        self.pipeline.alignment.add_messages(
            result.stderr, result.stdout, "".join(arguments)
        )

        result.check_returncode()

        # convert samfile to bamfile
        conversion_result = subprocess.run(
            [
                settings.BIO_SAMTOOLS_BINARY,
                "view",
                "-@",
                str(settings.BIO_THREADS),
                "-S",
                "-b",
                "-o",
                str(bam_file),
                sam_file,
            ],
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE,
            universal_newlines=True,
        )

        self.pipeline.alignment.add_messages(
            conversion_result.stderr, conversion_result.stdout
        )

        # Todo store output as message
        conversion_result.check_returncode()

        # Upload bam file
        upload_to_storage(self.pipeline, path=bam_file, file_type="DEFAULT")
        # Remove samfile
        cleanup(sam_file)

        # produce plot
        instance = self.pipeline.alignment
        plot_name = "mapping_statistics.png"
        plot_path = self.pipeline.get_pipeline_path() / plot_name
        if self.options["enable_paired_end"]:
            plot_mapping_info.plot_paired(result.stderr, plot_path, instance)
        else:
            plot_mapping_info.plot_unpaired(result.stderr, plot_path, instance)

        # Store plot
        instance.analysis_plot = upload_to_storage(
            self.pipeline,
            path=plot_path,
            file_type="PLOT",
        )

        instance.save()


def run_bowtie(previous, options, pipeline_id):
    step = AlignmentStep(previous, options, pipeline_id)
    step.build_bam()

    return step.data
