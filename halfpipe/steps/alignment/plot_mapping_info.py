import matplotlib.pyplot as plt


def process_unpaired_stderr(output, instance):
    # get lines
    lines = output.split("\n")
    # store numbers in list:
    # [all_reads, unpaired_reads, zero_aligned_reads, exactly_one_align,
    # more_than_one, overall_aligned_reads]

    read_statistic = [int(line.strip().split(" ")[0]) for line in lines[:5]]

    # add overall_aligned_reads
    read_statistic.append(read_statistic[3] + read_statistic[4])
    # store data
    store_unpaired_data(instance, read_statistic)
    overall_alignment_rate = round(read_statistic[5] * 100 / read_statistic[0], 2)
    instance.overall_alignment_rate = overall_alignment_rate

    return read_statistic


def process_paired_stderr(output, instance):
    # get lines
    lines = output.split("\n")
    # store concordantly aligned data in list:
    # [all_reads(0), paired_reads(1), exactly_one_concord_align(2),
    # more_than_one_concord_align(3),zero_concord_aligned(4), discord_align(5),
    # never_aligned(6), only one mate aligned(7), no mate aligned(8),
    # overall_aligned_mates(9)]

    read_statistics = []
    for i in range(2):
        read_statistics.append(int(lines[i].strip().split(" ")[0]))

    for i in range(3, 5):
        read_statistics.append(int(lines[i].strip().split(" ")[0]))
    # add zero_concord_aligned
    read_statistics.append(int(lines[2].strip().split(" ")[0]))
    # add number of alignments that only worked discordantly
    read_statistics.append(int(lines[7].strip().split(" ")[0]))
    # add number of never aligned pairs (neither concord. nor disconcord.)
    read_statistics.append(int(lines[9].strip().split(" ")[0]))
    # only one mate aligned
    read_statistics.append(
        int(lines[12].strip().split(" ")[0]) + int(lines[13].strip().split(" ")[0]) // 2
    )
    # no mate aligned
    read_statistics.append(read_statistics[6] - read_statistics[7])
    # add overall_alignment_rate
    overall_aligned_mates = (
        read_statistics[2]
        + read_statistics[3]
        + read_statistics[5]
        + int(read_statistics[7] / 2)
    )
    read_statistics.append(overall_aligned_mates)

    # store data
    store_paired_data(instance, read_statistics)
    # store overall_alignment_rate
    overall_alignment_rate = round(overall_aligned_mates * 100 / read_statistics[0], 2)
    instance.overall_alignment_rate = overall_alignment_rate

    return read_statistics


def store_paired_data(instance, read_statistics_list):
    # list structure
    # [all_reads(0), paired_reads(1), exactly_one_concord_align(2),
    # more_than_one_concord_align(3),zero_concord_aligned(4), discord_align(5),
    # never_aligned(6), only one mate aligned(7),
    # no mate aligned(8), overall_aligned_mates(9)]

    instance.all_reads = read_statistics_list[0]
    instance.valid_reads = read_statistics_list[1]
    instance.exactly_one_align = read_statistics_list[2]
    instance.more_than_one = read_statistics_list[3]
    instance.zero_aligned_reads = read_statistics_list[4]
    instance.discord_aligned_reads = read_statistics_list[5]
    instance.never_aligned_reads = read_statistics_list[6]
    instance.only_one_mate_aligned = read_statistics_list[7]
    instance.no_mate_aligned = read_statistics_list[8]


def store_unpaired_data(instance, read_statistics_list):
    # list structure
    # [all_reads, unpaired_reads, zero_aligned_reads, exactly_one_align,
    # more_than_one, overall_aligned reads]
    instance.all_reads = read_statistics_list[0]
    instance.valid_reads = read_statistics_list[1]
    instance.zero_aligned_reads = read_statistics_list[2]
    instance.exactly_one_align = read_statistics_list[3]
    instance.more_than_one = read_statistics_list[4]


def plot_paired(output, file_path, instance):
    read_statistics = process_paired_stderr(output, instance)[::-1]
    value_names = [
        "total reads",
        "total paired reads",
        "aligned concord. exactly 1 time",
        "aligned concord. >1 time",
        "aligned concordantly 0 times",
        "aligned disconcordantly",
        "did not align",
        "only one mate aligned",
        "no mate aligned",
        "overall alignment rate",
    ][::-1]
    fig, ax = plt.subplots()
    ax.spines["right"].set_visible(False)
    ax.spines["top"].set_visible(False)
    width = 0.5
    plt.barh(value_names, read_statistics, width)
    plt.title("Bowtie2 Mapping Information")
    for counter, value in enumerate(read_statistics):
        side_text = (
            "  "
            + str(value)
            + " ("
            + str(round(value * 100 / read_statistics[-1], 2))
            + "%)"
        )
        ax.text(
            value, counter, side_text, color="black", va="center", fontweight="bold"
        )
    plt.xlabel("number of reads")
    axes = plt.gca()
    axes.xaxis.grid(b=True, which="major", linestyle="--")
    plt.savefig(str(file_path), bbox_inches="tight")


def plot_unpaired(output, file_path, instance):
    read_statistics = process_unpaired_stderr(output, instance)[::-1]
    value_names = [
        "total reads",
        "total unpaired reads",
        "aligned 0 times",
        "aligned exactly 1 time",
        "aligned >1 time",
        "overall alignment rate",
    ][::-1]
    fig, ax = plt.subplots()
    ax.spines["right"].set_visible(False)
    ax.spines["top"].set_visible(False)
    width = 0.5
    plt.barh(value_names, read_statistics, width)
    plt.title("Bowtie2 Mapping Information")
    for counter, value in enumerate(read_statistics):
        side_text = (
            "  "
            + str(value)
            + "\n ("
            + str(round(value * 100 / read_statistics[-1], 2))
            + "%)"
        )
        ax.text(
            value, counter, side_text, color="black", va="center", fontweight="bold"
        )
    plt.xlabel("number of reads")
    axes = plt.gca()
    axes.xaxis.grid(b=True, which="major", linestyle="--")
    plt.savefig(str(file_path), bbox_inches="tight")
