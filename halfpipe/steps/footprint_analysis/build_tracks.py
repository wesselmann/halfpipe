import subprocess

from halfpipe.steps.utils import get_rgtdata_env


def generate_tracks(bamfile_path, peakfile_path, pipeline_path, organism, pe, instance):
    """Uses HINT to call footprints for a given narrowPeak/bed file and organism."""
    # TODO(isi): possible choice for user to get bias-corrected,
    # normalized or raw signal
    result = subprocess.run(
        f"rgt-hint tracks --bc --bigWig --organism={organism} "
        f"--output-location {str(pipeline_path)} {str(bamfile_path)} "
        f"{str(peakfile_path)}",
        shell=True,
        stderr=subprocess.PIPE,
        stdout=subprocess.PIPE,
        universal_newlines=True,
        env=get_rgtdata_env(),
    )

    instance.add_messages(result.stderr, result.stdout)
    result.check_returncode()

    return pipeline_path / "tracks.bw"
