import subprocess

from halfpipe.steps.utils import get_rgtdata_env


def parse_info_file(pipeline_path, instance):
    """Parses the 4-line footprints.info file generated during this step."""
    with open(pipeline_path / "footprints.info", "r") as f:
        values = (int(line.split(" ")[-1].split(".")[0]) for line in f.readlines()[:4])

        instance.num_reads = next(values)
        instance.num_peaks = next(values)
        instance.num_tc_within_peaks = next(values)
        instance.num_footprints = next(values)
        instance.save()

    instance.add_messages(
        f"Reads: {instance.num_reads}",
        f"Peaks: {instance.num_peaks}",
        f"Tag counts within peaks: {instance.num_tc_within_peaks}",
        "Peaks: {instance.num_footprints}",
    )


def call_fps(bamfile_path, peakfile_path, pipeline_path, organism, pe, instance):
    """Uses HINT to call footprints for a given narrowPeak/bed file and organism."""

    paired_tag = "--paired-end" if pe else ""
    command = (
        f"rgt-hint footprinting --atac-seq {paired_tag} --organism={organism} "
        f"--output-location={str(pipeline_path)} {str(bamfile_path)} "
        f"{str(peakfile_path)}"
    )

    # Can't do multiprocessing for whatever reason :(
    result = subprocess.run(
        command,
        shell=True,
        stderr=subprocess.PIPE,
        stdout=subprocess.PIPE,
        universal_newlines=True,
        env=get_rgtdata_env(),
    )

    instance.add_messages(command, result.stderr, result.stdout)
    result.check_returncode()

    parse_info_file(pipeline_path, instance)
    return pipeline_path / "footprints.bed"
