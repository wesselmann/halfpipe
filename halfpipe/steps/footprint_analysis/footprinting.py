import logging

from halfpipe.models import Pipeline
from halfpipe.steps.footprint_analysis import (
    build_tracks,
    call_footprints,
    fp_prep,
    make_profiles,
    motif_analysis,
)
from halfpipe.steps.utils import convert_to_png
from halfpipe.storage import upload_to_storage

logger = logging.getLogger()


def complete_footprinting(
    steps,
    org_bam_path,
    peak_path,
    pipeline_path,
    instance,
    organism,
    paired,
    motif_ranking_crit,
    n,
    pipeline,
    options,
):
    """tbd"""
    prep_bam_path = org_bam_path
    motif_names = set()
    # (1) Build index and sort Bam file
    if steps[0]:
        logger.debug("Preparing for footprinting...")
        prep_bam_path = fp_prep.sort_and_index(org_bam_path, pipeline_path, instance)

    # (2) Call footprints
    if steps[1]:
        logger.debug("Calling footprints...")
        footprints_path = call_footprints.call_fps(
            prep_bam_path, peak_path, pipeline_path, organism, paired, instance
        )
        # Upload footprint path to database
        instance.footprint_file = upload_to_storage(
            pipeline,
            path=footprints_path,
        )
        # TODO(isi): use info file

    # (3) Generate tracks
    if steps[2]:
        logger.debug("Generating tracks...")
        build_tracks.generate_tracks(
            prep_bam_path, peak_path, pipeline_path, organism, paired, instance
        )

    # (4) Find overlapping motifs
    if steps[3]:
        logger.debug("Finding motifs...")
        mpbs_path = motif_analysis.match_motifs(
            footprints_path, pipeline_path, organism, instance
        )

        # Upload mpbs files to database
        instance.mpbs_file = upload_to_storage(
            pipeline,
            path=mpbs_path,
        )

    # (5) Generate profiles
    if steps[4]:
        logger.debug("Generating profiles...")
        motif_names = make_profiles.generate_profiles(
            mpbs_path,
            prep_bam_path,
            pipeline_path,
            organism,
            instance,
            motif_ranking_crit,
            n,
        )

    instance.save()

    return motif_names


class Footprinting:
    def __init__(self, previous, options, pipeline_id):
        self.result = {}
        self.options = options
        self.pipeline_id = pipeline_id
        self.previous = previous

        self.pipeline = Pipeline.objects.get(pk=pipeline_id)

    def process(self):
        # rdb.set_trace()   # debugging
        pipeline_path = self.pipeline.get_pipeline_path()
        instance = self.pipeline.footprinting
        bamfile = self.previous["postalqc_result_bam"]
        peakfile = self.previous["narrow_peak"]

        organism = self.pipeline.assembly.name
        paired = self.options["enable_paired_end"]
        find_fps = self.options["calculate_footprint_scores"]
        build_peak_tracks = self.options["build_genome_tracks"]
        find_mpbs = self.options["find_matching_motifs"]
        build_profiles = self.options["build_seq_profiles"]
        n = self.options["max_number_of_plots"]
        motif_ranking_crit = self.options["motif_ranking"]

        at_least_one = any(
            (find_fps, build_peak_tracks, find_fps, find_mpbs, build_profiles)
        )

        # Determine needed steps.
        todos = (at_least_one, find_fps, build_peak_tracks, find_mpbs, build_profiles)
        logger.debug(todos)

        # Perform advanced analysis.
        motif_names = complete_footprinting(
            todos,
            bamfile,
            peakfile,
            pipeline_path,
            instance,
            organism,
            paired,
            motif_ranking_crit,
            n,
            self.pipeline,
            self.options,
        )

        logger.debug("Saving files...")
        # Upload plots.
        if todos[4] and len(motif_names) > 0:
            self.get_plots_and_upload(motif_names)

    def get_plots_and_upload(self, motif_names):
        """Uploads plots generated during the process of making profiles."""
        pipeline_path = self.pipeline.get_pipeline_path()
        instance = self.pipeline.footprinting

        for name in motif_names:
            file_path = pipeline_path / ("Lineplots/" + name + ".pdf")
            # convert file to png
            png_path = pipeline_path / "Lineplots" / f"{name}.png"
            convert_to_png(file_path, png_path, instance)

            plot_object = upload_to_storage(self.pipeline, png_path, "PLOT")
            instance.plots.add(plot_object)


def run(previous, options, pipeline_id):
    footprinting = Footprinting(previous, options, pipeline_id)
    footprinting.process()

    return footprinting.result
