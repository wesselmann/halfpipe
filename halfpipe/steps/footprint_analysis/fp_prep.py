import subprocess

from halfpipe.models import Message
from halfpipe.steps.post_alignment_qc.utils import samtools


def sort_and_index(bamfile_path, pipeline_path, instance):
    """Coordinate-sorts and indexes a given bam file for footprinting."""
    dest_path = pipeline_path / "cs_final.bam"

    # COMMAND: samtools sort foo.bam -o sorted_foo.bam
    result = subprocess.run(
        samtools(f"sort {str(bamfile_path)} -o {str(dest_path)}"),
        shell=True,
        stderr=subprocess.PIPE,
        universal_newlines=True,
    )
    if result.stderr:
        # save error message to table
        instance.messages.add(Message.objects.create(text=result.stderr))
    result.check_returncode()

    # COMMAND: samtools index foo.bam
    result = subprocess.run(
        samtools(f"index {str(dest_path)}"),
        shell=True,
        stderr=subprocess.PIPE,
        universal_newlines=True,
    )
    if result.stderr:
        # save error message to table
        instance.messages.add(Message.objects.create(text=result.stderr))
    result.check_returncode()
    return dest_path
