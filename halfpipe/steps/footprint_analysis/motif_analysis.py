import subprocess

from halfpipe.steps.utils import get_rgtdata_env


def match_motifs(fp_path, path, organism, instance):
    """Uses RGT's motif analysis to find motif predicted binding sites (MPBS) by
    matching motifs to footprints
    (find motifs overlapping with predicted footprints).
    """

    command = (
        f"rgt-motifanalysis matching --organism={organism} "
        f"--output-location {path} "
        f"--input-files {str(fp_path)}"
    )

    result = subprocess.run(
        command,
        shell=True,
        stderr=subprocess.PIPE,
        stdout=subprocess.PIPE,
        universal_newlines=True,
        env=get_rgtdata_env(),
    )

    instance.add_messages(command, result.stderr, result.stdout)
    result.check_returncode()

    outpath = fp_path.with_name(f"{str(fp_path.stem)}_mpbs.bed")
    return outpath
