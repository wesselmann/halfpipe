import logging
import subprocess
from bisect import insort
from shutil import move

from django.conf import settings
from django.db import transaction

from halfpipe.models import Motif
from halfpipe.steps.footprint_analysis import motif_analysis
from halfpipe.steps.utils import convert_to_png, filter_mpbs_file, get_rgtdata_env
from halfpipe.storage import download_from_storage, upload_to_storage

logger = logging.getLogger(__name__)


def store_lineplots(diffanalysis, instance, motifs, output_location):
    """Converts all given motifs to pngs.

    Names are guessed based on the motif names.
    """

    outputpath = output_location / "Lineplots"

    for motif in motifs:
        name = motif.name.replace("(", "_").replace(")", "")

        file_path = outputpath / (name + ".pdf")
        # convert file to png
        png_path = outputpath / f"{name}.png"
        convert_to_png(file_path, png_path, instance)

        motif.lineplot = upload_to_storage(diffanalysis, png_path, "PLOT")
        motif.save()


def get_files(diffanalysis, pipelines, motifs):
    """Generates or fetches all required files for differential footprinting."""

    organism = diffanalysis.get_assembly().name

    mpbs_files = []
    read_files = []
    conditions = []
    for pipeline in pipelines:
        # Append pipeline to conditions
        conditions.append(f"{pipeline.pk}")

        # Fetch readfile from storage
        read_file = diffanalysis.get_path() / f"alignment_{pipeline.pk}.bed"
        read_files.append(str(read_file))
        if not read_file.exists():
            download_from_storage(
                pipeline.post_alignment_qc.final_alignment, read_file, folder=False
            )

        mpbs_file = diffanalysis.get_path() / f"footprints_mpbs_{pipeline.pk}.bed"
        # The file might exist on subsequent hint calls of the same step
        if not mpbs_file.exists():
            if mpbs_file_instance := pipeline.footprinting.mpbs_file:
                download_from_storage(mpbs_file_instance, mpbs_file, folder=False)
            else:
                logger.debug(
                    "No mpbs file on footprinting found, generating a new one."
                )

                footprint_file = download_from_storage(
                    pipeline.footprinting.footprint_file,
                    diffanalysis.get_path(),
                )

                generated_mpbs = motif_analysis.match_motifs(
                    footprint_file,
                    diffanalysis.get_path(),
                    organism,
                    diffanalysis.diff_hint,
                )

                # Retrofit pipeline.footprinting with this file
                pipeline.footprinting.mpbs_file = upload_to_storage(
                    pipeline, path=generated_mpbs
                )
                pipeline.footprinting.save()

                move(generated_mpbs, mpbs_file)

        # Filter by motifs if given.
        if motifs:
            filtered_mpbs_file = (
                diffanalysis.get_path() / f"footprints_mpbs_filtered_{pipeline.pk}.bed"
            )

            filter_mpbs_file(
                mpbs_file,
                filtered_mpbs_file,
                motifs,
            )

            mpbs_files.append(str(filtered_mpbs_file))
        else:
            mpbs_files.append(str(mpbs_file))

    mpbs_str = ",".join(mpbs_files)
    read_str = ",".join(read_files)
    conditions_str = ",".join(conditions)

    return (mpbs_str, read_str, conditions_str)


def call_diff_hint(
    diffanalysis,
    pipelines,
    initial=False,
    lineplots=False,
    p_value_cutoff=0.05,
    filter_motifs=None,
    enable_bias_correction=True,
):
    # get organism
    organism = diffanalysis.get_assembly().name
    instance = diffanalysis.diff_hint

    # output location
    output_location = diffanalysis.get_path()
    output_location.mkdir(parents=True, exist_ok=True)

    mpbs_str, read_str, conditions_str = get_files(
        diffanalysis,
        pipelines,
        motifs=filter_motifs.values_list("name", flat=True) if filter_motifs else None,
    )

    bias_correction = "--bc" if enable_bias_correction else ""

    logger.debug("Starting HINT")
    subprocess_string = (
        f"rgt-hint differential --organism={organism} {bias_correction} "
        f"--nc {settings.BIO_THREADS} "
        f"--mpbs-files={mpbs_str} --reads-files={read_str} "
        f"--conditions={conditions_str} "
        f"--output-location={str(output_location)}"
    )

    if not lineplots:
        subprocess_string += " --no-lineplots "

    result = subprocess.run(
        subprocess_string,
        shell=True,
        stderr=subprocess.PIPE,
        stdout=subprocess.PIPE,
        universal_newlines=True,
        env=get_rgtdata_env(),
    )

    diffanalysis.diff_hint.add_messages(subprocess_string, result.stderr, result.stdout)
    result.check_returncode()

    logger.debug(
        f"Hint subprocess finished for pipelines ({conditions_str}), storing data."
    )

    statistics_text_path = output_location / "differential_statistics.txt"

    # Statistic files get stored on the first run,
    # where all conditions are run against each other.
    if initial:
        upload_to_storage(diffanalysis, path=statistics_text_path)
        differential_factor_path = output_location / "differential_factor.txt"
        upload_to_storage(diffanalysis, path=differential_factor_path)

    # We can skip a second iteration if only two pipelines are selected anyway.
    if not lineplots and len(pipelines) == 2:
        update_motifs(statistics_text_path, hint=diffanalysis.diff_hint)
        identifier = "_".join((str(pipeline.pk) for pipeline in pipelines))

        # convert plot to png
        plot_path_pdf = output_location / "differential_statistics.pdf"
        plot_path = output_location / f"differential_statistics_{identifier}.png"
        convert_to_png(plot_path_pdf, plot_path, instance)
        instance.scatter_plots.add(upload_to_storage(diffanalysis, plot_path, "PLOT"))
    elif lineplots:
        # store lineplots
        store_lineplots(diffanalysis, instance, filter_motifs, output_location)


def update_motifs(path, hint):
    """Updates the motifs on the hint model."""

    # Fetch motifs from the given statitics file
    found = {}
    with open(path, "r") as f:
        f.readline()
        found.update(
            {
                parts[0]: float(parts[8])
                for parts in (line.split("\t") for line in f.readlines())
            }
        )

    # Get existing from db and update accordingly
    existing = hint.motifs.select_for_update(of=(("self",))).filter(
        name__in=found.keys()
    )
    with transaction.atomic():
        for motif in existing:
            # Insert in sorted order
            insort(motif.p_values, found.pop(motif.name))
            motif.save()

        # Create new motifs, that didn't exist previously
        new = Motif.objects.bulk_create(
            (Motif(name=k, p_values=[v]) for k, v in found.items())
        )
        hint.motifs.add(*new)
