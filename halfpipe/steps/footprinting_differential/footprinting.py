from itertools import combinations

from halfpipe.models import DiffAnalysis
from halfpipe.steps.footprinting_differential.call_diff_hint import call_diff_hint


class DiffFootprinting:
    def __init__(self, previous, options, diff_id):
        self.data = previous
        self.options = options
        self.diff_id = diff_id

        self.diffanalysis = DiffAnalysis.objects.get(pk=diff_id)

    def process(self):
        p_value_cutoff = 0.05

        # First we run with all pipeline to generate statistic files
        call_diff_hint(
            self.diffanalysis,
            self.diffanalysis.pipelines.all(),
            initial=True,
            enable_bias_correction=self.options["enable_bias_correction"],
        )

        # Only needed if there are more than two pipelines
        if self.diffanalysis.pipelines.count() > 2:
            for pipelines in combinations(self.diffanalysis.pipelines.all(), 2):
                call_diff_hint(
                    self.diffanalysis,
                    pipelines,
                    enable_bias_correction=self.options["enable_bias_correction"],
                )

        # Get significant motifs
        filter_motifs = self.diffanalysis.diff_hint.motifs.filter(
            p_values__0__lte=p_value_cutoff
        ).filter(lineplot__isnull=True)

        # Generate plots for significant motifs
        if filter_motifs.exists():
            call_diff_hint(
                self.diffanalysis,
                self.diffanalysis.pipelines.all(),
                lineplots=True,
                p_value_cutoff=p_value_cutoff,
                filter_motifs=filter_motifs,
                enable_bias_correction=self.options["enable_bias_correction"],
            )


def run_hint(previous, options, diff_id):
    step = DiffFootprinting(previous, options, diff_id)
    step.process()

    return step.data
