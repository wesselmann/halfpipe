import subprocess

from django.conf import settings

from halfpipe.models import DiffAnalysis
from halfpipe.storage import download_from_storage, upload_to_storage


class DiffChipseeker:
    def __init__(self, previous, options, diff_id):
        self.data = previous
        self.options = options
        self.diff_id = diff_id

        self.diffanalysis = DiffAnalysis.objects.get(pk=diff_id)

    def get_file_string(self):
        # get pipelines
        queryset = self.diffanalysis.pipelines.all()
        # create strings for subprocess
        tagmatrices = []
        peakannos = []
        names = []
        for pipeline in queryset:
            # Sanity
            if not pipeline.annotate.tagmatrix:
                raise Exception("No tagmatrix was found, but there should be one.")

            # Fetch tagmatrix from storage
            tagfile = download_from_storage(
                pipeline.annotate.tagmatrix,
                self.diffanalysis.get_diff_path() / str(pipeline.pk),
            )
            tagmatrices.append(str(tagfile))

            if not pipeline.peak_calling.peaks:
                raise Exception("No peakfile found, but there should be one.")

            # Fetch peak annotation file
            peakfile = download_from_storage(
                pipeline.peak_calling.peaks,
                self.diffanalysis.get_diff_path() / str(pipeline.pk),
            )
            peakannos.append(str(peakfile))
            # append pipeline pk to names
            names.append("Pipeline_" + str(pipeline.pk))

        enriched = "TRUE" if any((p.annotate.enriched for p in queryset)) else "FALSE"
        tag_str = ",".join(tagmatrices)
        peak_str = ",".join(peakannos)
        name_str = ",".join(names)
        return tag_str, peak_str, name_str, enriched

    def call_chipseeker(self):
        # get script path
        script_path = str(
            settings.BASE_DIR
            / "halfpipe/steps/peak_annotate_differential/differential_annotation.R"
        )
        # Script expects following parameters:
        # - args[1]: tagmatrix files (comma separated)
        # - args[2]: peak annot files (comma separated)
        # - args[3]: cluster size for KEGG analysis (i.e. 15)
        # - args[4]: outputpath
        # - args[5]: txdb library
        # - args[6]: limit
        # - args[7]: pipeline names (comma-separated-list)
        # - args[8]: boolean value: TRUE if at least one pipeline is enriched,
        #   else FALSE
        # todo cluster size option, txdb_library option
        cluster_size = self.options["cluster"]
        txdblibrary = self.diffanalysis.get_assembly().r_package_name

        tag_str, peak_str, name_str, enriched = self.get_file_string()

        if window_size := self.options.get("window_size"):
            limits = window_size * 11
        else:
            limits = 3000

        command = [
            "Rscript",
            script_path,
            tag_str,
            peak_str,
            str(cluster_size),
            str(self.diffanalysis.get_diff_path()),
            txdblibrary,
            str(limits),
            name_str,
            enriched,
            self.diffanalysis.get_assembly().organism.short,
            str(settings.BIO_THREADS_R),
        ]
        self.diffanalysis.diff_chip.add_message(" ".join(command))

        result = subprocess.run(
            command,
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE,
            universal_newlines=True,
        )
        self.diffanalysis.diff_chip.add_messages(result.stdout, result.stderr)

        result.check_returncode()

        # create instance and store plots
        instance = self.diffanalysis.diff_chip

        for name in ["avgplot", "heatmap", "annobar", "dist_to_tss"]:
            filepath = self.diffanalysis.get_diff_path() / (name + ".png")
            file = upload_to_storage(self.diffanalysis, filepath, "PLOT")
            setattr(instance, name, file)

        # Kegg can fail -> look at plots separately
        for name in ["dotplot", "vennplot"]:
            filepath = self.diffanalysis.get_diff_path() / (name + ".png")
            if filepath.exists():
                file = upload_to_storage(self.diffanalysis, filepath, "PLOT")
                setattr(instance, name, file)

        # save instance
        instance.save()


def run_chip(previous, options, diff_id):
    step = DiffChipseeker(previous, options, diff_id)
    step.call_chipseeker()

    return step.data
