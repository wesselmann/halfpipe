import os
import subprocess

from django.conf import settings

from halfpipe.models import Message, Pipeline
from halfpipe.storage import upload_to_storage


class Annotate:
    def __init__(self, previous, options, pipeline_id):
        self.pipeline = Pipeline.objects.get(pk=pipeline_id)
        self.options = options
        self.previous = previous
        self.result = {}

    def run(self):
        script_path = str(
            settings.BASE_DIR / "halfpipe/steps/peak_annotate_single/generate_plots.R"
        )

        db = self.options["r_package_name"]
        if os.path.getsize(self.previous["summit_bed"]) == 0:
            self.pipeline.annotate.messages.add(
                Message.objects.create(text="No peaks found in the input!")
            )

            raise Exception("No peaks!")

        # exists For testing purposes
        if window_size := self.previous.get("window_size"):
            limits = window_size * 11
        else:
            limits = 3000

        command = [
            "Rscript",
            script_path,
            self.previous["peak_xls"],
            db,
            str(self.pipeline.get_pipeline_path()),
            str(limits),
            self.pipeline.assembly.organism.short,
            str(settings.BIO_THREADS_R),
        ]

        result = subprocess.run(
            command,
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE,
            universal_newlines=True,
        )

        self.pipeline.annotate.add_messages(
            " ".join(command), result.stdout, result.stderr
        )

        result.check_returncode()

        instance = self.pipeline.annotate
        for name in [
            "covplot",
            "heatmap",
            "avgplot",
            "upset",
        ]:
            f = upload_to_storage(
                self.pipeline,
                self.pipeline.get_pipeline_path() / (name + ".png"),
                file_type="PLOT",
            )

            setattr(instance, name, f)

        # store tagMatrix and peaks
        instance.tagmatrix = upload_to_storage(
            self.pipeline, self.pipeline.get_pipeline_path() / "tagmatrix.RData"
        )

        # KEGG can fail
        for name in [
            "over_representation",
            "gene_set_enrichment",
            "upset_enriched",
            "cnet",
        ]:
            if (
                filepath := self.pipeline.get_pipeline_path() / (name + ".png")
            ).exists():
                f = upload_to_storage(self.pipeline, filepath, file_type="PLOT")

                setattr(instance, name, f)
                instance.enriched = True

        instance.save()


def run_annotate(previous, options, pk):
    annotate = Annotate(previous, options, pk)
    annotate.run()

    return annotate.result
