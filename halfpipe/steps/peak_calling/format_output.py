def calculate_narrow_peak(file_path, file_path_bed, instance):
    narrow_peaks = open(file_path, "r")
    narrow_peaks_bed = open(file_path_bed, "r")
    genome_size = 0.0
    p_value_cutoff = 0.0
    q_value = -1
    fragment_length = 0
    fragments_total = 0
    window_d = 0

    # TOdo du muss noch zaehlen wie viele es gibt !!!
    peak_count = 0
    for line in narrow_peaks_bed:
        peak_count += 1

    for line in narrow_peaks.readlines():
        if line.startswith("#"):
            if line.startswith("# effective genome size"):
                genome_size = int(float(line.split("=")[1].strip("\n").strip(" ")))
            if line.startswith("# pvalue cutoff"):
                p_value_cutoff = float(line.split("=")[1].strip(" "))
            if line.startswith("# qvalue cutoff ="):
                q_value = float(line.split("=")[1].strip(" "))
            if line.startswith("# fragment size"):
                fragment_length = int(line.split(" ")[6].strip(" "))
            if line.startswith("# fragments after"):
                fragments_total = int(line.split(":")[1].strip(" "))
            if line.startswith("# d "):
                window_d = int(line.split("=")[1].strip(" "))
                break

    instance.genome_size = genome_size
    instance.p_value_cutoff = p_value_cutoff
    instance.q_value = q_value
    instance.fragment_length = fragment_length
    instance.fragments_total = fragments_total
    instance.window_d = window_d
    instance.peak_count = peak_count
