import subprocess

from django.conf import settings

from halfpipe.models import Message, Pipeline
from halfpipe.steps.peak_calling.format_output import calculate_narrow_peak
from halfpipe.storage import download_pipeline_form_data, upload_to_storage


class Preprocessor:
    def __init__(self, previous, options, pipeline_id):
        self.result = previous
        self.previous = previous
        self.options = options
        self.pipeline_id = pipeline_id

        self.pipeline = Pipeline.objects.get(pk=pipeline_id)

        self.upload_to_storage = lambda path: str(
            upload_to_storage(self.pipeline, path)
        )

    def build_arguments(self):

        arguments = []

        # peak calling default:

        arguments += ["callpeak"]

        arguments += ["-t"]
        arguments += [self.previous["postalqc_result_bam"]]

        if self.options["enable_paired_end"]:
            arguments += ["-f", "BAMPE"]
        else:
            arguments += ["-f", "AUTO"]

        # macs takes only one if both are specified
        if not self.options["use_fdr_cutoff"]:
            arguments += ["-p", str(self.options["p_value_cutoff"])]
        else:
            arguments += ["-q", str(self.options["minimum_fdr_cutoff"])]

        # control file
        if self.options["control_bam_file"]:
            self.get_control_file()
            arguments += ["-c", self.options["control_file_url"]]

        # genome size
        arguments += ["-g", (str(self.options["genome_size"]))]

        # output directory
        arguments += ["--outdir", str(self.pipeline.get_pipeline_path()) + "/"]

        # name files
        arguments += ["-n", "Macs3"]

        return arguments

    def process(self):
        arguments = [str(settings.BIO_MACS3_BINARY)] + self.build_arguments()
        instance = self.pipeline.peak_calling

        result = subprocess.run(
            arguments,
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE,
            universal_newlines=True,
        )

        instance.messages.add(Message.objects.create(text=" ".join(arguments)))

        if result.stderr:
            instance.messages.add(Message.objects.create(text=result.stderr))

        if result.stdout:
            instance.messages.add(Message.objects.create(text=result.stdout))

        # Deferred check
        result.check_returncode()

        calculate_narrow_peak(
            self.pipeline.get_pipeline_path() / "Macs3_peaks.xls",
            self.pipeline.get_pipeline_path() / "Macs3_summits.bed",
            instance=instance,
        )

    def get_control_file(self):
        self.options["control_file_url"] = download_pipeline_form_data(
            self.pipeline, self.options["control_file_url"], self.options
        )

    def finalize(self):
        instance = self.pipeline.peak_calling

        # Upload all created files to the storage backend
        narrow_peak_path = self.pipeline.get_pipeline_path() / "Macs3_peaks.narrowPeak"
        self.result["narrow_peak"] = str(narrow_peak_path)
        self.upload_to_storage(narrow_peak_path)

        peak_xls_path = self.pipeline.get_pipeline_path() / "Macs3_peaks.xls"
        self.result["peak_xls"] = str(peak_xls_path)
        instance.peaks = upload_to_storage(self.pipeline, peak_xls_path)
        instance.save()

        summit_path = self.pipeline.get_pipeline_path() / "Macs3_summits.bed"
        self.result["summit_bed"] = str(summit_path)
        self.upload_to_storage(summit_path)


def run(previous, options, pipeline_id):
    preprocessor = Preprocessor(previous, options, pipeline_id)

    preprocessor.process()
    preprocessor.finalize()

    return preprocessor.result
