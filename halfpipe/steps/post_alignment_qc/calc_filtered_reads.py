import json
import subprocess

from halfpipe.models import Message

from .utils import samtools


def parse_json(statsfile_path):
    with open(statsfile_path, "r") as f:
        r = json.load(fp=f)
        total = r["QC-passed reads"]["total"]
        non_prim = r["QC-passed reads"]["secondary"]
        supp = r["QC-passed reads"]["supplementary"]
        unmapp = total - r["QC-passed reads"]["mapped"]
        unpaired = total - r["QC-passed reads"]["paired in sequencing"]
        truly_unpaired = unpaired - non_prim
        unprop = (
            total - r["QC-passed reads"]["properly paired"]
        )  # NOTE(isi): include unpaired and one unmapped
        mate_unmapp_non_prim = r["QC-passed reads"]["singletons"]
        return (
            total,
            non_prim,
            supp,
            unmapp,
            unpaired,
            truly_unpaired,
            unprop,
            mate_unmapp_non_prim,
        )


def calc_sum_filtered(
    steps,
    bamfile_name,
    org_statsfile_path,
    statsfile_path,
    instance,
    quality,
    pipeline_path,
    diff_mito,
):
    # (1) Calculate number of reads before post-alignment processing.
    (
        total_org,
        non_prim_org,
        supp_org,
        unmapp_org,
        unpaired_org,
        truly_unpaired_org,
        unprop_org,
        mate_unmapp_non_prim_org,
    ) = parse_json(org_statsfile_path)

    # (2) Calculate number of reads at the very end of post-alignment processing.
    (
        total,
        non_prim,
        supp,
        unmapp,
        unpaired,
        truly_unpaired,
        unprop,
        mate_unmapp_non_prim,
    ) = parse_json(statsfile_path)

    # Calculate number of reads that would have not met the quality requirements,
    # not regarding whether they
    # got removed during the actual quality filtering step.
    bam_path = str(pipeline_path / bamfile_name)
    arguments = ["view", "-c", bam_path]
    result = subprocess.run(
        samtools(" ".join(arguments)),
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE,
        universal_newlines=True,
        shell=True,
    )
    pre_quality_check = result.stdout
    if result.stderr:
        # save error message to table
        instance.messages.add(Message.objects.create(text=result.stderr))
    result.check_returncode()
    if steps[2]:
        # We did filter bad quality reads.
        arguments = ["view", "-c", "-q", str(quality), bam_path]
        result = subprocess.run(
            samtools(" ".join(arguments)),
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE,
            universal_newlines=True,
            shell=True,
        )
        post_quality_check = result.stdout
        if result.stderr:
            # save error message to table
            instance.messages.add(Message.objects.create(text=result.stderr))
        result.check_returncode()
    else:
        # We did not filter bad quality reads at all.
        post_quality_check = pre_quality_check
    diff_quality = int(pre_quality_check) - int(post_quality_check)

    # Calculate number of reads really removed during duplicate filtering.
    if steps[4]:
        f = open((pipeline_path / "duplicates.txt"), "r")
        counter = 0
        diff_dup = 0
        for line in f.readlines():
            counter += 1
            if counter == 8:
                splits = line.split("\t")
                diff_dup = int(splits[5]) + int(splits[6]) * 2 + int(splits[7])
    else:
        diff_dup = 0

    # Calculate the number of reads that got removed that were secondary,
    # supplementary, unpaired, unproperly paired, unmapped or mate-unmapped,
    # not regarding whether they actually got removed during basic filtering.

    diff_non_prim = non_prim_org - non_prim
    diff_supp = supp_org - supp
    diff_unmapp = unmapp_org - unmapp
    diff_unpaired = unpaired_org - unpaired
    diff_unprop = unprop_org - unprop
    diff_mate_unmapp = mate_unmapp_non_prim_org - mate_unmapp_non_prim
    # diff_unprop_wo = diff_unprop - diff_unpaired
    diff_truly_unpaired = truly_unpaired_org - truly_unpaired

    # Get the number of reads really removed during mitochondrial filtering.
    diff_mito = diff_mito

    # Save calculated numbers to the post-ALQC table.
    instance.diff_nonprim = diff_non_prim
    instance.diff_supp = diff_supp
    instance.diff_unmapp = diff_unmapp
    instance.diff_unpaired = diff_unpaired
    instance.diff_unprop = diff_unprop
    instance.diff_mate_unmapp = diff_mate_unmapp
    instance.diff_quality = diff_quality
    instance.diff_dup = diff_dup
    instance.diff_mito = diff_mito
    instance.diff_truly_unpaired = diff_truly_unpaired
    instance.save()
