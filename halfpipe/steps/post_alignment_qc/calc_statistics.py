import subprocess

from halfpipe.models import Message
from halfpipe.steps.post_alignment_qc.utils import samtools


def samtools_statistics(bamfile_name, pipeline_path, in_pipeline_dir, instance):
    """Counts the number of alignments for each flag value using flagstat."""
    # f = open(out_file_name, "w")
    # subprocess.run([samtools_path, "flagstat", bamfile_name], stdout=f)
    if in_pipeline_dir:
        complete_path = str(pipeline_path / bamfile_name)
    else:
        complete_path = bamfile_name
    statsfile_name = "stats_" + str(bamfile_name).split(".")[0] + ".json"
    complete_stats_path_raw = pipeline_path / statsfile_name
    complete_stats_path = str(complete_stats_path_raw)
    result = subprocess.run(
        samtools(f"flagstat {complete_path}") + f" -O json > {complete_stats_path}",
        shell=True,
        stderr=subprocess.PIPE,
        universal_newlines=True,
    )

    if result.stderr:
        instance.messages.add(Message.objects.create(text=result.stderr))
    result.check_returncode()

    return statsfile_name
