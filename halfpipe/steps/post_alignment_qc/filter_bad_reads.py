import subprocess

from halfpipe.models import Message
from halfpipe.steps.post_alignment_qc.utils import samtools


def samtools_calc_filterflag(fnonprim, fsupp, funmap, fmateunmap):
    """Calculates the necessary flag value."""
    flagsum = 0
    if funmap:
        flagsum += int(0x4)
    if fmateunmap:
        flagsum += int(0x8)
    if fsupp:
        flagsum += int(0x800)
    if fnonprim:
        flagsum += int(0x100)
    return flagsum


def samtools_remove_bad_reads(
    bamfile_name, funpaired, funprop, flag, pipeline_path, instance
):
    """Removes user-specified bad reads from file."""
    # possible "bad" reads: unpaired, not primary, supplementary, unmapped,
    # mate unmapped, improperly paired
    current_bam = bamfile_name
    complete_path = str(pipeline_path / current_bam)

    if funpaired:
        # keep ONLY paired reads
        # COMMAND: samtools view -b -f 0x1 foo.bam -o paired_foo.bam
        dest_bam = "p_" + current_bam
        complete_dest_path_raw = pipeline_path / dest_bam
        complete_dest_path = str(complete_dest_path_raw)
        result = subprocess.run(
            samtools(f"view -h -b -f 0x1 {complete_path} -o {complete_dest_path}"),
            shell=True,
            stderr=subprocess.PIPE,
            universal_newlines=True,
        )
        if result.stderr:
            # save error message to table
            instance.messages.add(Message.objects.create(text=result.stderr))
        result.check_returncode()

        current_bam = dest_bam
        complete_path = complete_dest_path

        if funprop:
            # keep ONLY properly paired reads
            # COMMAND: samtools view -b -f 0x2 foo.bam -o pp_foo.bam
            dest_bam = "pp_" + current_bam
            complete_dest_path_raw = pipeline_path / dest_bam
            complete_dest_path = str(complete_dest_path_raw)
            result = subprocess.run(
                samtools(f"view -h -b -f 0x2 {complete_path} -o {complete_dest_path}"),
                shell=True,
                stderr=subprocess.PIPE,
                universal_newlines=True,
            )
            if result.stderr:
                # save error message to table
                instance.messages.add(Message.objects.create(text=result.stderr))
            result.check_returncode()

            current_bam = dest_bam
            complete_path = complete_dest_path

    if flag != 0:
        # possibly DO NOT KEEP non-primary, supplementary, improperly-paired,
        # unmapped or mate-unmapped reads
        # COMMAND: samtools view -F *flagscore* foo.bam -o f_foo.bam
        dest_bam = "f_" + current_bam
        complete_dest_path_raw = pipeline_path / dest_bam
        complete_dest_path = str(complete_dest_path_raw)
        result = subprocess.run(
            samtools(
                f"view -h -b -F {str(flag)} {complete_path} -o {complete_dest_path}"
            ),
            shell=True,
            stderr=subprocess.PIPE,
            universal_newlines=True,
        )
        if result.stderr:
            # save error message to table
            instance.messages.add(Message.objects.create(text=result.stderr))
        result.check_returncode()

        current_bam = dest_bam

    return current_bam
