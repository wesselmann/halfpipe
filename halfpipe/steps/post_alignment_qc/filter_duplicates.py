import subprocess

from halfpipe.models import Message
from halfpipe.steps.post_alignment_qc.utils import picard


def picard_duplicates(bamfile_name, pipeline_path, instance):
    """Removes PCR-duplicates."""
    complete_path = str(pipeline_path / bamfile_name)
    dest_bam = "dup_" + bamfile_name
    complete_dest_path_raw = pipeline_path / dest_bam
    complete_dest_path = str(complete_dest_path_raw)
    dup_path_raw = pipeline_path / "duplicates.txt"
    dup_path = str(dup_path_raw)

    result = subprocess.run(
        [
            "java",
            "-jar",
            picard(),
            "MarkDuplicates",
            "-I" + complete_path,
            "-O" + complete_dest_path,
            "-REMOVE_DUPLICATES",
            "true",
            "-M",
            dup_path,
        ],
        stderr=subprocess.PIPE,
        universal_newlines=True,
    )
    if result.stderr:
        # save error message to table
        instance.messages.add(Message.objects.create(text=result.stderr))
    result.check_returncode()

    return dest_bam
