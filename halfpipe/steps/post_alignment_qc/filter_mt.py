import subprocess

from halfpipe.models import Message
from halfpipe.steps.post_alignment_qc.utils import removeChrom, samtools


def samtools_mt_genome(bamfile_name, mito, pipeline_path, instance):
    """Removes reads from a given chromosome."""

    complete_path = str(pipeline_path / bamfile_name)

    # count number of unmapped reads pre quality filtering
    result = subprocess.run(
        samtools(f"view -c -f {str(4)} {complete_path}"),
        shell=True,
        stdout=subprocess.PIPE,
        universal_newlines=True,
    )
    if result.stderr:
        # save error message to table
        instance.messages.add(Message.objects.create(text=result.stderr))
    result.check_returncode()
    count_pre = int(result.stdout)

    # perform quality filtering
    dest_bam = "mt_" + bamfile_name
    complete_dest_path_raw = pipeline_path / dest_bam
    complete_dest_path = str(complete_dest_path_raw)
    result = subprocess.run(
        samtools(f"view -h {complete_path}") + " | "
        f"{removeChrom()} - - {mito} | "
        + samtools(f"view -b - -o {complete_dest_path}"),
        shell=True,
        stderr=subprocess.PIPE,
        universal_newlines=True,
    )
    if result.stderr:
        # save error message to table
        instance.messages.add(Message.objects.create(text=result.stderr))
    result.check_returncode()

    # count number of unmapped reads again after quality filtering
    result = subprocess.run(
        samtools(f"view -c -f {str(4)} {complete_dest_path}"),
        shell=True,
        stdout=subprocess.PIPE,
        universal_newlines=True,
    )
    if result.stderr:
        # save error message to table
        instance.messages.add(Message.objects.create(text=result.stderr))
    result.check_returncode()
    count_post = int(result.stdout)
    diff_mito = count_post - count_pre

    # remove newly unmapped reads
    result = subprocess.run(
        samtools(f"view -h -b -F {str(4)} {complete_path} -o {complete_dest_path}"),
        shell=True,
    )
    if result.stderr:
        # save error message to table
        instance.messages.add(Message.objects.create(text=result.stderr))
    result.check_returncode()

    return dest_bam, diff_mito
