import subprocess

from halfpipe.models import Message
from halfpipe.steps.post_alignment_qc.utils import samtools


def samtools_quality_check(bamfile_name, quality, pipeline_path, instance):
    """Filters reads according to given MAPQ score threshold.
    Skips reads with MAPQ smaller than quality.
    """
    if quality != -1:
        # depends on used alignment software. MAPQ score range and default values may
        # vary.
        # bowtie1: only 0/255 (recommended default: 255)
        # -> use other params (-m, -strata)
        # bowtie2: 0-42 (recommended default: 10)
        # bwa: 0-37 (recommended default: 20)

        # COMMAND: samtools view -b -q *quality* foo.bam > q_foo.bam
        complete_path = str(pipeline_path / bamfile_name)
        dest_bam = "q_" + bamfile_name
        complete_dest_path_raw = pipeline_path / dest_bam
        complete_dest_path = str(complete_dest_path_raw)
        result = subprocess.run(
            samtools(
                f"view -h -b -q {str(quality)} {complete_path} -o {complete_dest_path}"
            ),
            shell=True,
            stderr=subprocess.PIPE,
            universal_newlines=True,
        )
        if result.stderr:
            # save error message to table
            instance.messages.add(Message.objects.create(text=result.stderr))
        result.check_returncode()

        # NOTE(isi): What about other quality filtering?
        # for example: -m 1 (bowtie) / -F "[XS] == null" (bowtie2) / grep 'XT:A:U'...
        # (bwa)? unsafe? unneccessary?
        return dest_bam

    return bamfile_name
