import subprocess

from halfpipe.steps.post_alignment_qc.utils import picard
from halfpipe.steps.utils import convert_to_png
from halfpipe.storage import upload_to_storage


def picard_multiple_metrics(bamfile_name, pipeline_path, instance, options):
    """Plots multiple metrics collected by Picard."""
    complete_path = pipeline_path / bamfile_name
    dest_dir_path = pipeline_path / "metrics"

    result = subprocess.run(
        [
            "java",
            "-jar",
            picard(),
            "CollectMultipleMetrics",
            "-I",
            complete_path,
            "-O",
            dest_dir_path,
        ],
        stderr=subprocess.PIPE,
        stdout=subprocess.PIPE,
        universal_newlines=True,
    )

    instance.add_messages(result.stderr, result.stdout)

    result.check_returncode()

    # handle files
    file_path = pipeline_path / "metrics.base_distribution_by_cycle.pdf"
    png_path = pipeline_path / "metrics.base_distribution_by_cycle.png"
    handle_plot(file_path, png_path, instance)

    file_path = pipeline_path / "metrics.insert_size_histogram.pdf"
    png_path = pipeline_path / "metrics.insert_size_histogram.png"
    handle_plot(file_path, png_path, instance)

    file_path = pipeline_path / "metrics.quality_by_cycle.pdf"
    png_path = pipeline_path / "metrics.quality_by_cycle.png"
    handle_plot(file_path, png_path, instance)

    file_path = pipeline_path / "metrics.quality_distribution.pdf"
    png_path = pipeline_path / "metrics.quality_distribution.png"
    handle_plot(file_path, png_path, instance)

    file_path = pipeline_path / "metrics.read_length_histogram.pdf"
    png_path = pipeline_path / "metrics.read_length_histogram.png"
    handle_plot(file_path, png_path, instance)


def handle_plot(file_path, png_path, instance):
    """Converts file to png, uploads and adds to table."""
    convert_to_png(file_path, png_path, instance)
    plot_object = upload_to_storage(instance.pipeline.get(), png_path, "PLOT")
    instance.plots.add(plot_object)
