import logging
import os

from halfpipe.models import Pipeline
from halfpipe.steps.post_alignment_qc import (
    calc_filtered_reads,
    calc_statistics,
    filter_bad_reads,
    filter_duplicates,
    filter_mt,
    filter_quality,
    plotting,
    shift_reads,
    sort_bam,
)
from halfpipe.storage import upload_to_storage

logger = logging.getLogger(__name__)


def complete_filtering(
    steps,
    bamfile_name,
    pipeline_path,
    instance,
    options,
    funpaired,
    funprop,
    fnonprim,
    fsupp,
    funmap,
    fmateunmap,
    quality,
    mito,
):
    """Calls all requested filtering according to steps.

    For each step (specified below), a bit is set in the steps list. The steps are
    executed in separate python scripts in a set order.
    """
    current_bam = bamfile_name
    diff_mito = 0

    # (1) SORTING BY NAME
    if steps[0]:
        logger.debug("Sorting...")
        current_bam = sort_bam.samtools_sorting(
            bamfile_name, "n", pipeline_path, instance
        )

    # (2) REMOVAL OF BAD READS
    if steps[1]:
        logger.debug("Filtering bad reads...")
        flag = filter_bad_reads.samtools_calc_filterflag(
            fnonprim, fsupp, funmap, fmateunmap
        )
        if flag != 0:
            current_bam = filter_bad_reads.samtools_remove_bad_reads(
                current_bam, funpaired, funprop, flag, pipeline_path, instance
            )

    # (3) QUALITY CHECK
    if steps[2]:
        logger.debug("Filtering quality...")
        current_bam = filter_quality.samtools_quality_check(
            current_bam, quality, pipeline_path, instance
        )

    # (4) REMOVAL OF MITOCHONDRIAL READS
    if steps[3]:
        logger.debug("Filtering mitochondrial reads...")
        current_bam, diff_mito = filter_mt.samtools_mt_genome(
            current_bam, mito, pipeline_path, instance
        )

    # (5) REMOVAL OF PCR-DUPLICATES
    if steps[4]:
        logger.debug("Filtering duplicated reads...")
        # ensure coordinates-sorted for filtering duplicates
        # COMMAND: samtools sort foo.bam -o cs_foo.bam
        current_bam = sort_bam.samtools_sorting(
            current_bam, "p", pipeline_path, instance
        )
        current_bam = filter_duplicates.picard_duplicates(
            current_bam, pipeline_path, instance
        )

    result_bam = current_bam
    # Generate statistics of result bam for verifying.
    statsfile_name = calc_statistics.samtools_statistics(
        bamfile_name=result_bam,
        pipeline_path=pipeline_path,
        in_pipeline_dir=True,
        instance=instance,
    )
    return result_bam, statsfile_name, diff_mito


class Preprocessor:
    def __init__(self, previous, options, pipeline_id):
        self.result = previous
        self.options = options
        self.pipeline_id = pipeline_id
        self.previous = previous

        self.pipeline = Pipeline.objects.get(pk=pipeline_id)

    def process(self):
        # rdb.set_trace()   # debugging
        bam = self.previous["bamfile"]
        pipeline_path = self.pipeline.get_pipeline_path()
        instance = self.pipeline.post_alignment_qc

        # Get parameters for filtering from options.
        funpaired = self.options["enable_unpaired_filter"]
        funprop = self.options["enable_unproperly_paired_filter"]
        funmap = self.options["enable_unmapped_filter"]
        fmateunmap = self.options["enable_mate_unmapped_filter"]
        fnonprim = self.options["enable_nonprimary_filter"]
        fsupp = self.options["enable_supplementary_filter"]
        sort_and_filter = any((funpaired, funprop, funmap, fmateunmap, fnonprim, fsupp))
        quality = self.options.get("quality_threshold")
        mito = (
            self.pipeline.assembly.mitochondrial_tag
        )  # mito = self.options["mitochondrial_tag"]

        do_filter_quality = self.options["enable_quality_filter"] and (quality > 0)
        do_filter_mito = self.options["enable_mitochondrial_filter"] and (mito != "")

        # Determine needed steps.
        todos = (
            sort_and_filter,
            sort_and_filter,
            do_filter_quality,
            do_filter_mito,
            self.options["enable_duplicated_filter"],
        )

        # Perform filtering.
        filtered_bam, statsfile_name, diff_mito = complete_filtering(
            steps=todos,
            bamfile_name=bam,
            pipeline_path=pipeline_path,
            instance=instance,
            options=self.options,
            funpaired=funpaired,
            funprop=funprop,
            fnonprim=fnonprim,
            fsupp=fsupp,
            funmap=funmap,
            fmateunmap=fmateunmap,
            quality=quality,
            mito=mito,
        )
        result_bam = filtered_bam

        # Generate the insert size distribution plote.
        # This needs to be done **before** shifting,
        # since that removes the quality scores
        if self.options["enable_paired_end"]:
            plotting.picard_multiple_metrics(
                result_bam, pipeline_path, instance, self.options
            )

        if self.options.get("enable_shift_filter"):
            result_bam = shift_reads.shift_reads(
                str(self.pipeline.get_pipeline_path() / filtered_bam), self.pipeline
            )

        # If there is no filtering, we don't have a output file
        if result_bam != "alignment.bam":
            # Rename alignment to unified name
            result_path_raw = pipeline_path / "filtered_alignment.bam"
            os.rename(pipeline_path / result_bam, result_path_raw)

            # Upload filtered alignment
            instance.final_alignment = upload_to_storage(
                self.pipeline,
                path=result_path_raw,
            )
        else:
            # If there is no filtering we use the file created during alignment
            result_path_raw = pipeline_path / result_bam
            instance.final_alignment = self.pipeline.files.get(name=result_bam)

        result_path = str(result_path_raw)

        statsfile_path = str(pipeline_path / statsfile_name)

        self.result["postalqc_result_bam"] = result_path
        self.result["stats"] = statsfile_path

        # Generate statistics of original pre-filtering bam.
        org_statsfile = calc_statistics.samtools_statistics(
            bam, pipeline_path, True, instance
        )
        org_statsfile_path = str(pipeline_path / org_statsfile)

        # Calculate number of filtered reads.
        calc_filtered_reads.calc_sum_filtered(
            steps=todos,
            bamfile_name=bam,
            org_statsfile_path=org_statsfile_path,
            statsfile_path=statsfile_path,
            instance=instance,
            quality=quality,
            pipeline_path=pipeline_path,
            diff_mito=diff_mito,
        )


def run(previous, options, pipeline_id):
    # rdb.set_trace()   # debugging
    preprocessor = Preprocessor(previous, options, pipeline_id)
    preprocessor.process()

    return preprocessor.result
