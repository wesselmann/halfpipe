import subprocess

from halfpipe.models import Message
from halfpipe.steps.post_alignment_qc.utils import samtools


def shift_reads(bamfile, pipeline):
    """Shift the reads in a bamfile by a given amount."""

    AWK_COMMAND = (
        'awk \'BEGIN {FS = "\t"; OFS="\t";} ; {if (and($2, 16) == 16)'
        " { $8 = $8 + 4; $9 = $9 + 9} else {$4 = $4 + 4; $9 = $9 - 9;};"
        ' print $1,$2,$3,$4,$5,"22M",$7,$8,$9,"*","*";}\''
    )

    convert = f"view - -b -o {str(pipeline.get_pipeline_path() / 'shifted.bam')}"
    arguments = (
        "cat "
        f"<({ samtools('view -H ' + bamfile) }) "
        f"<({ samtools('view ' + bamfile) } | "
        f"{ AWK_COMMAND }) | "
        f"{ samtools(convert) }"
    )

    result = subprocess.run(
        arguments,
        shell=True,
        executable="/bin/bash",
        stderr=subprocess.PIPE,
    )

    if result.stderr:
        pipeline.post_alignment_qc.messages.add(
            Message.objects.create(text=result.stderr)
        )

    result.check_returncode()

    return "shifted.bam"
