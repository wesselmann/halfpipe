import subprocess

from halfpipe.models import Message
from halfpipe.steps.post_alignment_qc.utils import samtools


def samtools_sorting(bamfile_name, sort_by, pipeline_path, instance):
    """Sorts a given bam file."""
    complete_path = str(
        pipeline_path / bamfile_name
    )  # TODO: show line when from alignment
    if sort_by != "n" and sort_by != "p":
        # unknown sorting command, will not sort
        return bamfile_name

    if sort_by == "n":
        # will sort by NAME
        sorted_bam_name = "ns_" + bamfile_name
        complete_dest_path_raw = pipeline_path / sorted_bam_name
        complete_dest_path = str(complete_dest_path_raw)

        # COMMAND: samtools sort -n foo.bam -o sorted_foo.bam
        result = subprocess.run(
            samtools(f"sort -n {complete_path} -o {complete_dest_path}"),
            shell=True,
            stderr=subprocess.PIPE,
            universal_newlines=True,
        )
        if result.stderr:
            instance.messages.add(Message.objects.create(text=result.stderr))
        result.check_returncode()

    elif sort_by == "p":
        # will sort by POSITION/coordinates
        sorted_bam_name = "cs_" + bamfile_name
        complete_dest_path_raw = pipeline_path / sorted_bam_name
        complete_dest_path = str(complete_dest_path_raw)

        # COMMAND: samtools sort foo.bam -o sorted_foo.bam
        result = subprocess.run(
            samtools(f"sort {complete_path} -o {complete_dest_path}"),
            shell=True,
            stderr=subprocess.PIPE,
            universal_newlines=True,
        )
        if result.stderr:
            # save error message to table
            instance.messages.add(Message.objects.create(text=result.stderr))
        result.check_returncode()

    return sorted_bam_name
