from django.conf import settings


def samtools(arg_string):
    # sam_tools_path = "/home/isabell/software/samtools-1.11-installation/bin/samtools"
    sam_tools_path = str(settings.BIO_SAMTOOLS_BINARY)
    command = f"{sam_tools_path} {arg_string} -@ {settings.BIO_THREADS}"
    return command


def picard():
    return str(settings.BIO_PICARD_JAR)


def removeChrom():
    return str(settings.BIO_REMOVECHROM_PY)


def bedtools():
    return settings.BIO_BEDTOOLS_BINARY
