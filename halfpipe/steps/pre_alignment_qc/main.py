import subprocess
from pathlib import Path

from django.conf import settings
from rest_framework.parsers import JSONParser

from halfpipe.models import Message, Pipeline
from halfpipe.storage import upload_to_storage

from .serializers import FastpReportSerializer


class PreAlignmentQCStep:
    def __init__(self, previous, options, pipeline_id):
        self.data = previous
        self.options = options
        self.pipeline = Pipeline.objects.get(pk=pipeline_id)

        self.upload_to_storage = lambda path: upload_to_storage(
            self.pipeline, Path(path)
        )

    def get_prefixed_file(self, prefix, path):
        """Prefix the given filename with the given value.
        The rest of the path stays unchanged.
        """
        return str(
            self.pipeline.get_pipeline_path()
            / (prefix + "_readfile" + "".join(Path(path).suffixes))
        )

    def build_arguments(self):
        arguments = []

        if self.options["enable_paired_end"]:
            arguments += ["--in1", self.data["forward_readfile"]]
            arguments += ["--in2", self.data["reverse_readfile"]]

            self.data["processed_forward_readfile"] = self.get_prefixed_file(
                "processed_forward", self.data["forward_readfile"]
            )

            self.data["processed_reverse_readfile"] = self.get_prefixed_file(
                "processed_reverse", self.data["reverse_readfile"]
            )

            arguments += ["--out1", self.data["processed_forward_readfile"]]
            arguments += ["--out2", self.data["processed_reverse_readfile"]]
        else:
            arguments += ["--in1", self.data["readfile"]]
            self.data["processed_readfile"] = self.get_prefixed_file(
                "processed", self.data["readfile"]
            )
            arguments += ["--out1", self.data["processed_readfile"]]

        if self.options["enable_adapter_trimming"]:
            # Adapter trimming for se is enabled by default in fastp,
            # but must be enabled for pe
            if self.options["detect_adapters"] and self.options["enable_paired_end"]:
                arguments += ["--detect_adapter_for_pe"]
            # Specify adapter for single end
            elif (
                not self.options["detect_adapters"]
                and not self.options["enable_paired_end"]
            ):
                arguments += ["--adapter_sequence", self.options["adapter_sequence"]]
            # Specify adapters for paired end
            elif self.options["enable_paired_end"]:
                arguments += [
                    "--adapter_sequence",
                    self.options["forward_adapter_sequence"],
                ]
                arguments += [
                    "--adapter_sequence_r2",
                    self.options["reverse_adapter_sequence"],
                ]
        else:
            arguments += ["--disable_adapter_trimming"]

        # Store the fastp report as json in the pipeline folder
        json_report = str(self.pipeline.get_pipeline_path() / "fastp_report_file.json")
        self.data["fastp_report_file"] = json_report
        arguments += ["--json", json_report]

        # Discard the the html report
        arguments += ["--html", "/dev/null"]

        # Set threads
        arguments += ["--thread", str(settings.BIO_THREADS)]

        return arguments

    def process(self):
        arguments = [str(settings.BIO_FASTP_BINARY)] + self.build_arguments()

        # We defer the check for success until stderr and stdout are saved to the db
        result = subprocess.run(
            arguments,
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE,
            universal_newlines=True,
        )

        # Capture call to fastp and it's output if available as messages
        instance = self.pipeline.pre_alignment_qc
        instance.messages.add(Message.objects.create(text=" ".join(arguments)))

        if result.stderr:
            instance.messages.add(Message.objects.create(text=result.stderr))

        if result.stdout:
            instance.messages.add(Message.objects.create(text=result.stdout))

        # Deferred check
        result.check_returncode()

        # Write report file contents to db
        with open(self.data["fastp_report_file"], "rb") as f:
            content = FastpReportSerializer(JSONParser().parse(f)).data

            instance.fastp_report = content
            instance.save()

    def finalize(self):
        # Upload all created files to the storage backend
        self.upload_to_storage(self.data["fastp_report_file"])

        if self.options["enable_paired_end"]:
            self.upload_to_storage(self.data["processed_forward_readfile"])
            self.upload_to_storage(self.data["processed_reverse_readfile"])
        else:
            self.upload_to_storage(self.data["processed_readfile"])


def run_pre_alignment_qc(previous, options, pipeline_id):
    step = PreAlignmentQCStep(previous, options, pipeline_id)

    step.process()
    step.finalize()

    return step.data
