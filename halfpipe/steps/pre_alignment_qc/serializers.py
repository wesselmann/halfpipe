from rest_framework import serializers


class SummaryEntrySerializer(serializers.Serializer):
    total_reads = serializers.IntegerField()
    total_bases = serializers.IntegerField()
    q20_bases = serializers.IntegerField()
    q30_bases = serializers.IntegerField()
    q20_rate = serializers.FloatField()
    q30_rate = serializers.FloatField()
    read1_mean_length = serializers.IntegerField()
    read2_mean_length = serializers.IntegerField(required=False)
    gc_content = serializers.FloatField()


class SummarySerializer(serializers.Serializer):
    before_filtering = SummaryEntrySerializer()
    after_filtering = SummaryEntrySerializer()


class FilteringResultSerializer(serializers.Serializer):
    low_quality_reads = serializers.IntegerField()
    too_many_N_reads = serializers.IntegerField()
    too_short_reads = serializers.IntegerField()
    too_long_reads = serializers.IntegerField()


class AdapterCuttingSerializer(serializers.Serializer):
    adapter_trimmed_reads = serializers.IntegerField()
    adapter_trimmed_bases = serializers.IntegerField()
    read1_adapter_sequence = serializers.CharField(required=False)
    read2_adapter_sequence = serializers.CharField(required=False)


class DuplicationResultSerializer(serializers.Serializer):
    rate = serializers.FloatField()
    histogram = serializers.ListField(child=serializers.FloatField())
    mean_gc = serializers.ListField(child=serializers.FloatField())


class InsertSizeSerializer(serializers.Serializer):
    peak = serializers.IntegerField()
    unknown = serializers.IntegerField()
    histogram = serializers.ListField(child=serializers.IntegerField())


class BaseCurveSerializer(serializers.Serializer):
    A = serializers.ListField(child=serializers.FloatField())
    T = serializers.ListField(child=serializers.FloatField())
    C = serializers.ListField(child=serializers.FloatField())
    G = serializers.ListField(child=serializers.FloatField())


class QualityCurveSerializer(BaseCurveSerializer):
    mean = serializers.ListField(child=serializers.FloatField())


class ContentCurveSerializer(BaseCurveSerializer):
    N = serializers.ListField(child=serializers.FloatField())
    GC = serializers.ListField(child=serializers.FloatField())


class DetailedReadReportSerializer(serializers.Serializer):
    quality_curves = QualityCurveSerializer()
    content_curves = ContentCurveSerializer()


class FastpReportSerializer(serializers.Serializer):
    summary = SummarySerializer()
    filtering_result = FilteringResultSerializer()
    duplication = DuplicationResultSerializer()
    insert_size = InsertSizeSerializer(required=False)

    adapter_cutting = AdapterCuttingSerializer(required=False)

    # Details
    read1_before_filtering = DetailedReadReportSerializer()
    read1_after_filtering = DetailedReadReportSerializer()

    # FIXME (till): only when using paired end data
    read2_before_filtering = DetailedReadReportSerializer(required=False)
    read2_after_filtering = DetailedReadReportSerializer(required=False)
