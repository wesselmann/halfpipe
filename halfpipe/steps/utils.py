import os
import subprocess
from pathlib import Path

from django.conf import settings


def get_rgtdata_env():
    """Returns an environment with all variables set to make rgt happy."""
    env = os.environ.copy()
    env["RGTDATA"] = settings.BIO_RGTDATA_FOLDER
    return env


def convert_to_png(input_file_path, output_file_path, instance):
    """Converts a given path or str of a pdf to an image."""

    # Take str and Paths
    input_file_path = str(input_file_path)
    output_file_path = str(output_file_path)

    # Discard potential file ending
    output_file_path = output_file_path.rsplit(".", maxsplit=1)[0]
    result = subprocess.run(
        f"pdftoppm {input_file_path} {output_file_path} -png -singlefile",
        shell=True,
        stderr=subprocess.PIPE,
        universal_newlines=True,
    )

    instance.add_message(result.stderr)
    result.check_returncode()


def filter_mpbs_file(path: Path, destination: Path, motifs):
    """Filter mpbs file by a motif list.

    Takes all occurences of the given motif names and writes the
    respective lines in a new mpbs file.
    """
    with open(destination, "w") as out:
        with open(path, "r") as f:
            for line in f.readlines():
                parts = line.split("\t")
                if parts[3] in motifs:
                    out.write(line)
