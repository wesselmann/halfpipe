import json
import os
from pathlib import Path
from urllib.parse import urlparse

from django.conf import settings
from minio import Minio

from halfpipe.models import File


def get_policy(bucket):
    """Returns the policy for public read buckets."""

    return json.dumps(
        {
            "Version": "2012-10-17",
            "Statement": [
                {
                    "Effect": "Allow",
                    "Principal": {"AWS": "*"},
                    "Action": ["s3:GetBucketLocation", "s3:ListBucket"],
                    "Resource": f"arn:aws:s3:::{bucket}",
                },
                {
                    "Effect": "Allow",
                    "Principal": {"AWS": "*"},
                    "Action": "s3:GetObject",
                    "Resource": f"arn:aws:s3:::{bucket}/*",
                },
            ],
        }
    )


def url_to_parts(url):
    """Splits a S3 api link into bucket and filename."""
    o = urlparse(url)
    return o.path.lstrip("/").split("/", 1)


def get_client():
    """Creates a new minio client from the django settings."""
    return Minio(
        settings.MINIO_SERVER,
        settings.MINIO_ACCESS_KEY,
        settings.MINIO_SECRET_KEY,
        secure=settings.MINIO_SSL,
    )


def ensure_bucket(bucket):
    """Ensures that a given bucket exists."""
    client = get_client()
    if not client.bucket_exists(bucket):
        client.make_bucket(bucket)
        client.set_bucket_policy(bucket, get_policy(bucket))


def cleanup(path: Path):
    """Removes the file if retain folders is off."""
    if not settings.RETAIN_FOLDERS:
        os.remove(path)


def download_from_storage(instance, path, folder=True):
    """Takes a file instance and downloads to given path or folder."""
    client = get_client()

    if folder:
        path /= instance.name
    client.fget_object(*url_to_parts(instance.url), str(path))

    return path


def upload_to_storage(instance, path, file_type="DEFAULT"):
    """Uploads a file to a pipeline specific bucket."""

    bucket = instance.get_bucket_name()
    ensure_bucket(bucket)

    client = get_client()
    client.fput_object(bucket, path.name, path)
    f = File.objects.create(
        url=f"{settings.MINIO_PROTOCOL}{settings.MINIO_SERVER}/{bucket}/{path.name}",
        name=path.name,
        file_type=file_type,
    )

    instance.files.add(f)

    return f


def upload_for_test(instance, path, filename, file_type="DEFAULT"):
    bucket = instance.get_bucket_name()
    ensure_bucket(bucket)

    client = get_client()
    client.fput_object(bucket, filename, path)
    f = File.objects.create(
        url=f"{settings.MINIO_PROTOCOL}{settings.MINIO_SERVER}/{bucket}/{filename}",
        name=filename,
        file_type=file_type,
    )

    instance.files.add(f)

    return f


def download_pipeline_form_data(pipeline, fileinfo, options: dict):
    client = get_client()

    path = Path(settings.MEDIA_ROOT) / f"pipeline_{pipeline.pk}" / fileinfo["name"]
    client.fget_object(*url_to_parts(fileinfo["url"]), str(path))
    if options["retain_user_uploaded"]:
        upload_to_storage(pipeline, path, "USER")

    # Remove from user upload bucket
    client.remove_object(*url_to_parts(fileinfo["url"]))

    # Needs to be json serializable
    return str(path)
