import logging
import shutil

from django.conf import settings
from django.utils import timezone

from halfpipe import celery_app as app

logger = logging.getLogger(__name__)


def cleanup(instance, status):
    """Runs all required cleanup after a run ended."""

    # add the end time of a run
    instance.end_date = timezone.now()
    instance.status = status

    instance.save()

    # Remove folder from worker
    if not settings.RETAIN_FOLDERS:
        shutil.rmtree(instance.get_path(), ignore_errors=True)

    # Notify user abount process end.
    instance.send_email()


def create_watchdog(model_class, update_channel, related_name):
    """Creates a watchdog decorator for simple status updating."""

    def watchdog(attribute_name):
        def update_step(instance, status):
            """Updates one step and messages the detail group."""
            instance.status = status
            instance.save()

            # Update the detail channel
            base = getattr(instance, related_name).get()
            base.refresh_from_db()
            update_channel(base)

        def outer(func):
            def inner(self, previous, options, pk):
                logger.warning(f"Starting step: {attribute_name}")
                base = model_class.objects.get(pk=pk)
                instance = getattr(base, attribute_name)
                update_step(instance, "RUNNING")

                result = func(self, previous, options, pk)

                # The instance could be stale due to a step running
                # inbetween with another db cursor.
                instance.refresh_from_db()
                update_step(instance, "SUCCESS")
                logger.warning(f"Finished step: {attribute_name}")
                return result

            return inner

        return outer

    return watchdog


def create_handle_group_error(model_class, detail_channel, name):
    @app.task(bind=True, name=name + ".handle_group_error")
    def handle_failed_in_group(self, *args, **kwargs):
        """Handles a failure in a task group."""
        pk = kwargs.pop("pk")
        field = kwargs.pop("field")

        instance = model_class.objects.get(pk=pk)

        step = getattr(instance, field)
        step.status = "FAILURE"
        step.save()

        # Update the detail channel
        instance.refresh_from_db()
        detail_channel(instance)

    return handle_failed_in_group


def create_handle_error(model_class, name):
    @app.task(bind=True, name=name + ".handle_error")
    def handle_error(self, *args, **kwargs):
        """Error handler gets run on a link error."""
        pk = kwargs.pop("pk")

        cleanup(model_class.objects.get(pk=pk), "FAILURE")

    return handle_error
