from django.contrib import admin
from django.contrib.auth.views import LoginView, LogoutView
from django.urls import include, path

from halfpipe.views import IndexView, UploadURlView

urlpatterns = [
    path("admin/", admin.site.urls),
    path("", IndexView.as_view(), name="home"),
    path("api/get_presigned_url", UploadURlView.as_view(), name="get_presigned_url"),
    path("api/", include("halfpipe.api.urls")),
    path("pipeline/", include("halfpipe.pipeline.urls")),
    path("diff_analysis/", include("halfpipe.differential.urls")),
    path("login", LoginView.as_view(), name="login"),
    path(
        "logout",
        LogoutView.as_view(),
        name="logout",
    ),
]
