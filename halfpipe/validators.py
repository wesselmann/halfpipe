import re

from django.core.exceptions import ValidationError
from django.utils.translation import gettext as _


def validate_even(value):
    """Validates if a number is even."""
    if value % 2 != 0:
        raise ValidationError(
            _("%(value)s is not an even number"),
            params={"value": value},
        )


def validate_dna_sequence(value):
    search = re.compile(r"[^atcg]+", re.IGNORECASE).search

    if bool(search(value)):
        raise ValidationError(_("Sequence must only contain A,T,C,G."))
