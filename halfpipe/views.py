import logging
import uuid

from django.conf import settings
from django.contrib.auth.mixins import LoginRequiredMixin
from django.http import HttpResponse, HttpResponseRedirect
from django.urls import reverse
from django.views import View
from django.views.generic import TemplateView
from django.views.generic.edit import DeleteView
from urllib3.exceptions import MaxRetryError

from halfpipe.storage import get_client

logger = logging.getLogger(__name__)


class IndexView(TemplateView):
    template_name = "index.html"


class UploadURlView(LoginRequiredMixin, View):
    """Provides and endpoint to sign urls for file uploads using S3."""

    def get(self, request):
        client = get_client()
        try:
            if not client.bucket_exists(settings.MINIO_UPLOAD_BUCKET):
                client.make_bucket(settings.MINIO_UPLOAD_BUCKET)

            return HttpResponse(
                client.get_presigned_url(
                    "PUT", settings.MINIO_UPLOAD_BUCKET, str(uuid.uuid4())
                )
            )
        except MaxRetryError:
            logger.error(
                f"The storage backend at { settings.MINIO_SERVER }could not be reached!"
            )
            return HttpResponse(status=503)


class ProcessDeleteView(LoginRequiredMixin, DeleteView):
    def delete(self, request, *args, **kwargs):
        self.object = self.get_object()

        if request.user.is_superuser or request.user == self.object.user:
            self.object.delete()
            return HttpResponseRedirect(reverse(self.success_url))
        else:
            return HttpResponse(
                "Insufficient permission.", content_type="text/plain", status=403
            )
