import json

from django.forms.widgets import Widget


class ContextMixin:
    def get_context(self, name, value, attrs):
        context = super().get_context(name, value, attrs)

        if value:
            context["widget"]["data"] = json.loads(value)

        return context


class FileUploadWidget(ContextMixin, Widget):
    """Widget for direct upload to an S3 compatible API."""

    multiple = False
    template_name = "widgets/file_upload.html"


class PipelineMultiSelectWidget(ContextMixin, Widget):
    """Widget for selecting multiple pipelines with the same assembly."""

    multiple = False
    template_name = "widgets/pipeline_select.html"
