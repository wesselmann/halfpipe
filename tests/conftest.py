import factory
import pytest
import pytest_factoryboy
from django.conf import settings


@pytest_factoryboy.register
class UserFactory(factory.django.DjangoModelFactory):
    username = factory.Faker("name")
    email = factory.Faker("email")

    class Meta:
        model = settings.AUTH_USER_MODEL
        django_get_or_create = ("username",)


@pytest_factoryboy.register
class OrganismFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = "halfpipe.Organism"
        django_get_or_create = ("name",)

    name = "Saccharomyces cerevisiae"
    short = "sce"
    genome_size = 10000000


@pytest_factoryboy.register
class AssemblyFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = "halfpipe.Assembly"

    information = "sacCer3"
    name = "sacCer3"
    r_package_name = "TxDb.Scerevisiae.UCSC.sacCer3.sgdGene"
    organism = factory.SubFactory(OrganismFactory)
    mitochondrial_tag = "chrM"


@pytest.fixture
def authenticated_client(client, user):
    client.force_login(user)
    return client


@pytest_factoryboy.register
class PreAlignmentQCFactory(factory.django.DjangoModelFactory):
    status = "PENDING"

    class Meta:
        model = "halfpipe.PreAlignmentQC"


@pytest_factoryboy.register
class AlignmentFactory(factory.django.DjangoModelFactory):
    status = "PENDING"

    class Meta:
        model = "halfpipe.Alignment"


@pytest_factoryboy.register
class PostAlignmentQCFactory(factory.django.DjangoModelFactory):
    status = "PENDING"

    class Meta:
        model = "halfpipe.PostAlignmentQC"


@pytest_factoryboy.register
class PeakCallingFactory(factory.django.DjangoModelFactory):
    status = "PENDING"

    class Meta:
        model = "halfpipe.PeakCalling"


@pytest_factoryboy.register
class AnnotateFactory(factory.django.DjangoModelFactory):
    status = "PENDING"

    class Meta:
        model = "halfpipe.Annotate"


@pytest_factoryboy.register
class FootprintingFactory(factory.django.DjangoModelFactory):
    status = "PENDING"

    class Meta:
        model = "halfpipe.Footprinting"


@pytest_factoryboy.register
class PipelineFactory(factory.django.DjangoModelFactory):
    options = {}

    pre_alignment_qc = factory.SubFactory(PreAlignmentQCFactory)
    alignment = factory.SubFactory(AlignmentFactory)
    post_alignment_qc = factory.SubFactory(PostAlignmentQCFactory)
    peak_calling = factory.SubFactory(PeakCallingFactory)
    annotate = factory.SubFactory(AnnotateFactory)
    footprinting = factory.SubFactory(FootprintingFactory)

    assembly = factory.SubFactory(AssemblyFactory)
    user = factory.SubFactory(UserFactory)

    class Meta:
        model = "halfpipe.Pipeline"


@pytest_factoryboy.register
class FileFactory(factory.django.DjangoModelFactory):
    url = "/empty"
    name = "filename"

    class Meta:
        model = "halfpipe.File"


@pytest_factoryboy.register
class MotifFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = "halfpipe.Motif"


@pytest_factoryboy.register
class DiffHintFactory(factory.django.DjangoModelFactory):
    status = "PENDING"

    class Meta:
        model = "halfpipe.DiffHint"


@pytest_factoryboy.register
class DiffChipFactory(factory.django.DjangoModelFactory):
    status = "PENDING"

    class Meta:
        model = "halfpipe.DiffChip"


@pytest_factoryboy.register
class DiffAnalysisFactory(factory.django.DjangoModelFactory):
    options = {}

    diff_hint = factory.SubFactory(DiffHintFactory)
    diff_chip = factory.SubFactory(DiffChipFactory)

    user = factory.SubFactory(UserFactory)

    class Meta:
        model = "halfpipe.DiffAnalysis"
