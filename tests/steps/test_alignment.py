from subprocess import CalledProcessError

import pytest
from django.test import override_settings

from halfpipe.steps.alignment.build_index_files import run_bowtie

pairness_data = [
    (
        True,
        {
            "processed_forward_readfile": (
                "tests/data/TESTX_H7YRLADXX_S1_L001_R1_001.fastq.gz"
            ),
            "processed_reverse_readfile": (
                "tests/data/TESTX_H7YRLADXX_S1_L001_R2_001.fastq.gz"
            ),
        },
    ),
    (
        False,
        {
            "processed_readfile": "tests/data/TESTX_H7YRLADXX_S1_L001_R1_001.fastq.gz",
        },
    ),
]


@pytest.mark.django_db
@override_settings(RETAIN_FOLDERS=True)
@pytest.mark.parametrize("pairness,reads", pairness_data)
def test_alignment(pipeline_factory, assembly, pairness, reads):
    pipeline = pipeline_factory()

    # Create folder for pipeline that normally gets created in
    # a previous pipeline step.
    pipeline.get_pipeline_path().mkdir(exist_ok=True)

    options = {
        "index_folder": str(assembly.get_index_path()),
        "retain_intermediary_files": False,
        "enable_paired_end": pairness,
    }

    run_bowtie(reads, options, pipeline.pk)

    assert (pipeline.get_pipeline_path() / "mapping_statistics.png").exists()


@pytest.mark.django_db
@override_settings(RETAIN_FOLDERS=True)
def test_invalid_readst(pipeline, assembly):
    pipeline.get_pipeline_path().mkdir(exist_ok=True)

    options = {
        "index_folder": str(assembly.get_index_path()),
        "retain_intermediary_files": False,
        "enable_paired_end": True,
    }

    # "invalid" readfiles
    last = {
        "processed_forward_readfile": (
            "tests/data/TESTX_H7YRLADXX_S1_L001_R1_001.fastq.gz"
        ),
        "processed_reverse_readfile": "tests/data/NA_peaks.xls",
    }

    with pytest.raises(CalledProcessError):
        run_bowtie(last, options, pipeline.pk)

    assert pipeline.alignment.messages.count() > 0
