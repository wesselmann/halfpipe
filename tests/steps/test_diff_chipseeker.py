import pytest

from halfpipe.steps.peak_annotate_differential.annotate import run_chip
from halfpipe.steps.peak_annotate_single.annotate import run_annotate
from halfpipe.storage import upload_for_test
from tests.conftest import PipelineFactory


def store_peaks(pipeline, peakfile):
    peak_instance = pipeline.peak_calling
    peak_instance.peaks = upload_for_test(
        pipeline, peakfile, filename="Macs3_peaks.xls"
    )
    peak_instance.save()


def store_tagmatrix(pipeline, tagmatrix, enriched):
    anno_instance = pipeline.annotate
    anno_instance.tagmatrix = upload_for_test(
        pipeline, tagmatrix, filename="tagmatrix.RData"
    )
    anno_instance.enriched = enriched
    anno_instance.save()


def simulate_annotation(pipeline, r_package, summit_bed, peak_xls, window_size):

    options = {"r_package_name": r_package}

    previous = {
        "summit_bed": summit_bed,
        "peak_xls": peak_xls,
        "window_size": window_size,
    }

    run_annotate(previous, options, pipeline.pk)


@pytest.mark.django_db
def test_chip(diff_analysis_factory):
    diffanalysis = diff_analysis_factory()
    diffanalysis.get_diff_path().mkdir(parents=True, exist_ok=True)

    # simulate pipelines
    pipeline1 = PipelineFactory.create()
    pipeline1.get_pipeline_path().mkdir(parents=True, exist_ok=True)
    pipeline2 = PipelineFactory.create()
    pipeline2.get_pipeline_path().mkdir(parents=True, exist_ok=True)

    peakfile1 = "tests/data/NA_peaks_short.xls"
    peakfile2 = "tests/data/NA_peaks_short2.xls"

    # # run annotate to get certain files
    simulate_annotation(
        pipeline=pipeline1,
        r_package="TxDb.Scerevisiae.UCSC.sacCer3.sgdGene",
        summit_bed="tests/data/NA_summits.bed",
        peak_xls=peakfile1,
        window_size=50,
    )
    simulate_annotation(
        pipeline=pipeline2,
        r_package="TxDb.Scerevisiae.UCSC.sacCer3.sgdGene",
        summit_bed="tests/data/NA_summits.bed",
        peak_xls=peakfile2,
        window_size=50,
    )

    # store peakfiles
    store_peaks(pipeline1, peakfile1)
    store_peaks(pipeline2, peakfile2)

    # store pipelines in diffanalysis
    diffanalysis.pipelines.add(pipeline1.pk)
    diffanalysis.pipelines.add(pipeline2.pk)

    options = {
        "cluster": 15,
        "window_size": 50,
    }

    previous = {}

    run_chip(previous, options, diffanalysis.pk)

    assert (diffanalysis.get_diff_path() / "heatmap.png").exists()
    assert (diffanalysis.get_diff_path() / "avgplot.png").exists()
    assert (diffanalysis.get_diff_path() / "annobar.png").exists()
    assert (diffanalysis.get_diff_path() / "dist_to_tss.png").exists()
