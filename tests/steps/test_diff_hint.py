import shutil
from pathlib import Path

import pytest

from halfpipe.steps.footprinting_differential.footprinting import run_hint
from halfpipe.storage import upload_to_storage
from tests.conftest import PipelineFactory


@pytest.mark.django_db
@pytest.mark.parametrize("bc", [True, False])
def test_hint(diff_analysis_factory, bc):
    pipeline_a = PipelineFactory.create()
    alignment_file = upload_to_storage(
        pipeline_a, Path("tests/data/pairs/filtered_a.bam")
    )
    pipeline_a.post_alignment_qc.final_alignment = alignment_file
    pipeline_a.footprinting.footprint_file = upload_to_storage(
        pipeline_a, Path("tests/data/pairs/peaks_a.xls")
    )
    pipeline_a.post_alignment_qc.save()
    pipeline_a.footprinting.save()

    pipeline_b = PipelineFactory.create()
    alignment_file = upload_to_storage(
        pipeline_a, Path("tests/data/pairs/filtered_b.bam")
    )
    pipeline_b.post_alignment_qc.final_alignment = alignment_file
    pipeline_b.post_alignment_qc.save()
    pipeline_b.footprinting.footprint_file = upload_to_storage(
        pipeline_b, Path("tests/data/pairs/peaks_b.xls")
    )
    pipeline_b.footprinting.save()

    differential = diff_analysis_factory()
    differential.pipelines.add(pipeline_a, pipeline_b)

    # Purge directory
    shutil.rmtree(differential.get_path(), ignore_errors=True)

    options = {"enable_bias_correction": bc}
    run_hint({}, options, differential.pk)

    path = (
        differential.get_path()
        / f"differential_statistics_{pipeline_a.pk}_{pipeline_b.pk}.png"
    )

    assert path.exists()
