import shutil

import pytest

from halfpipe.steps.footprint_analysis.footprinting import run

data = ["num", "bit"]


@pytest.mark.django_db
@pytest.mark.parametrize("ranking", data)
def test_db_values(pipeline_factory, ranking):
    """Tests if the correct values are written in the DB after a
    default test run of Post-ALQC.
    """
    pipeline = pipeline_factory()

    # Create folder for pipeline that normally gets created in a previous pipeline step.
    pipeline.get_pipeline_path().mkdir(exist_ok=True)
    instance = pipeline.footprinting

    # Copy test files and set them as last.

    path = pipeline.get_pipeline_path() / "alignment.bam"
    shutil.copyfile("tests/data/test_align.bam", path)
    path = pipeline.get_pipeline_path() / "peaks.narrowPeak"
    shutil.copyfile("tests/data/test.narrowPeak", path)
    last = {
        "postalqc_result_bam": str(pipeline.get_pipeline_path() / "alignment.bam"),
        "narrow_peak": str(pipeline.get_pipeline_path() / "peaks.narrowPeak"),
    }

    # Generate options/parameters.
    options = {
        "enable_paired_end": True,
        "calculate_footprint_scores": True,
        "build_genome_tracks": False,
        "find_matching_motifs": True,
        "build_seq_profiles": True,
        "max_number_of_plots": 2,
        "motif_ranking": ranking,
    }

    assembly = pipeline.assembly
    assembly.name = "mm10"
    assembly.save()

    run(last, options, pipeline.pk)

    instance.refresh_from_db()
    assert 444646 == instance.num_reads
    assert 1188 == instance.num_peaks
    assert 28913.0 == instance.num_tc_within_peaks
    assert 5421 == instance.num_footprints
