import pytest

from halfpipe.steps.peak_annotate_single.annotate import run_annotate


@pytest.mark.django_db
def test_annotation(pipeline_factory):
    pipeline = pipeline_factory()
    pipeline.get_pipeline_path().mkdir(parents=True, exist_ok=True)
    options = {
        "enable_paired_end": True,
        "r_package_name": "TxDb.Scerevisiae.UCSC.sacCer3.sgdGene",
    }

    previous = {
        "summit_bed": "tests/data/NA_summits.bed",
        "peak_xls": "tests/data/NA_peaks_short.xls",
        "window_size": 50,
    }

    run_annotate(previous, options, pipeline.pk)

    assert (pipeline.get_pipeline_path() / "tagmatrix.RData").exists()
    assert (pipeline.get_pipeline_path() / "covplot.png").exists()
    assert (pipeline.get_pipeline_path() / "heatmap.png").exists()
    assert (pipeline.get_pipeline_path() / "avgplot.png").exists()
    assert (pipeline.get_pipeline_path() / "upset.png").exists()
