import pytest

from halfpipe.steps.peak_calling.macs3 import run


@pytest.mark.django_db
def test_run(pipeline_factory):
    pipeline = pipeline_factory()
    options = {
        "enable_paired_end": True,
        "readfile": None,
        "enable_adapter_trimming": True,
        "detect_adapters": True,
        "forward_adapter_sequence": "",
        "reverse_adapter_sequence": "",
        "adapter_sequence": "",
        # peak calling
        "use_fdr_cutoff": True,
        "minimum_fdr_cutoff": 0.5,
        "control_bam_file": False,
        "genome_size": "10000000",
    }

    last = {"postalqc_result_bam": "tests/data/test_yeastalign.bam"}

    run(last, options, pipeline.pk)

    assert (
        (pipeline.get_pipeline_path() / "Macs3_peaks.xls").exists()
        and (pipeline.get_pipeline_path() / "Macs3_summits.bed").exists()
        and (pipeline.get_pipeline_path() / "Macs3_peaks.narrowPeak").exists()
    )


@pytest.mark.django_db
def test_run2(pipeline_factory):
    pipeline = pipeline_factory()
    options = {
        "enable_paired_end": True,
        "readfile": None,
        "enable_adapter_trimming": True,
        "detect_adapters": True,
        "forward_adapter_sequence": "",
        "reverse_adapter_sequence": "",
        "adapter_sequence": "",
        # peak calling
        "use_fdr_cutoff": True,
        "minimum_fdr_cutoff": 0.5,
        "control_bam_file": False,
        "genome_size": "10000000",
    }

    last = {"postalqc_result_bam": "tests/data/test_yeastalign.bam"}

    run(last, options, pipeline.pk)

    assert (
        (pipeline.get_pipeline_path() / "Macs3_peaks.xls").exists()
        and (pipeline.get_pipeline_path() / "Macs3_summits.bed").exists()
        and (pipeline.get_pipeline_path() / "Macs3_peaks.narrowPeak").exists()
    )
