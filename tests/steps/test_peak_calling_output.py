import pytest

from halfpipe.steps.peak_calling.format_output import calculate_narrow_peak


@pytest.mark.django_db
def test_run(pipeline_factory):
    pipeline = pipeline_factory()
    instance = pipeline.peak_calling

    calculate_narrow_peak(
        "tests/data/NA_peaks.xls", "tests/data/NA_summits.bed", instance
    )

    assert instance.genome_size == 2700000000
    assert instance.p_value_cutoff == 0.05
    assert instance.q_value == -1
    assert instance.fragment_length == 145
    assert instance.fragments_total == 61821
    assert instance.window_d == 145
    assert instance.peak_count == 45
