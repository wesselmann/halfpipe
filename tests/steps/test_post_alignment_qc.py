import os
import shutil

import pytest
from django.test import override_settings

from halfpipe.steps.post_alignment_qc.post_align_qc import run


def run_default_testcase(options, pipeline):
    """Incorporates test bam file as last/previous."""
    path = pipeline.get_pipeline_path() / "alignment.bam"
    shutil.copyfile("tests/data/test_yeastalign.bam", path)
    last = {
        "bamfile": "alignment.bam",
    }
    run(last, options, pipeline.pk)


def default_run_options(retain):
    """Sets the option for a default, valid pipeline run."""
    options = {
        "retain_intermediary_files": retain,  # (!)
        "enable_unpaired_filter": True,
        "enable_unproperly_paired_filter": True,
        "enable_mate_unmapped_filter": True,
        "enable_unmapped_filter": True,
        "enable_nonprimary_filter": True,
        "enable_supplementary_filter": True,
        "enable_quality_filter": True,
        "quality_threshold": 20,
        "enable_mitochondrial_filter": True,
        "mitochondrial_tag": "chrM",
        "enable_duplicated_filter": True,
        "enable_shift_filter": True,
        "enable_paired_end": True,
    }
    return options


PLOTS = [
    "metrics.insert_size_histogram.png",
    "metrics.base_distribution_by_cycle.png",
    "metrics.quality_by_cycle.png",
    "metrics.quality_distribution.png",
    "metrics.read_length_histogram.png",
]


def check_plots_exist(pipeline_path):
    for plot in PLOTS:
        assert (pipeline_path / plot).exists()


@pytest.mark.django_db
@override_settings(RETAIN_FOLDERS=False)
def test_run(pipeline_factory):
    """Tests whether all intermediate files got DELETED after a whole Post-ALQC run."""
    pipeline = pipeline_factory()

    # Create folder for pipeline that normally gets created in a previous pipeline step.
    pipeline.get_pipeline_path().mkdir(exist_ok=True)
    pipeline_path = pipeline.get_pipeline_path()

    # Generate options/parameters.
    bam = "alignment"
    options = default_run_options(retain=False)
    run_default_testcase(options, pipeline)

    # check if final files got written
    assert os.path.exists(pipeline_path / "filtered_alignment.bam")  # final file
    assert os.path.exists(pipeline_path / "duplicates.txt")  # duplicate info
    assert os.path.exists(pipeline_path / f"stats_dup_cs_mt_q_f_pp_p_ns_{bam}.json")
    # stats file post filtering
    check_plots_exist(pipeline_path)

    assert os.path.exists(
        pipeline_path / f"stats_{bam}.json"
    )  # stats file pre filtering


@pytest.mark.django_db
@override_settings(RETAIN_FOLDERS=True)
def test_run_retain(pipeline_factory):
    """Tests whether all intermediate files got RETAINED after a whole Post-ALQC run."""
    pipeline = pipeline_factory()

    # Create folder for pipeline that normally gets created in a previous pipeline step.
    pipeline.get_pipeline_path().mkdir(exist_ok=True)
    pipeline_path = pipeline.get_pipeline_path()

    # Generate options/parameters.
    bam = "alignment"
    options = default_run_options(retain=True)
    run_default_testcase(options, pipeline)

    # check if final files got written
    assert os.path.exists(pipeline_path / "filtered_alignment.bam")  # final file
    assert os.path.exists(pipeline_path / "duplicates.txt")  # duplicate info
    assert os.path.exists(pipeline_path / f"stats_dup_cs_mt_q_f_pp_p_ns_{bam}.json")

    # stats file post filtering
    check_plots_exist(pipeline_path)

    assert os.path.exists(
        pipeline_path / f"stats_{bam}.json"
    )  # stats file pre filtering

    # check if intermediate files got retained
    assert os.path.exists(
        pipeline_path / f"cs_mt_q_f_pp_p_ns_{bam}.bam"
    )  # intermediate bam
    assert os.path.exists(
        pipeline_path / f"mt_q_f_pp_p_ns_{bam}.bam"
    )  # intermediate bam
    assert os.path.exists(pipeline_path / f"q_f_pp_p_ns_{bam}.bam")  # intermediate bam
    assert os.path.exists(pipeline_path / f"f_pp_p_ns_{bam}.bam")  # intermediate bam
    assert os.path.exists(pipeline_path / f"pp_p_ns_{bam}.bam")  # intermediate bam
    assert os.path.exists(pipeline_path / f"p_ns_{bam}.bam")  # intermediate bam
    assert os.path.exists(pipeline_path / f"ns_{bam}.bam")  # intermediate bam


@pytest.mark.django_db
def test_db_values(pipeline_factory):
    """Tests if the correct values are written in the DB
    after a default test run of Post-ALQC.
    """

    pipeline = pipeline_factory()

    # Create folder for pipeline that normally gets created in a previous pipeline step.
    pipeline.get_pipeline_path().mkdir(exist_ok=True)
    instance = pipeline.post_alignment_qc

    # Generate options/parameters.
    options = default_run_options(retain=False)
    run_default_testcase(options, pipeline)

    instance.refresh_from_db()
    assert 0 == instance.diff_nonprim
    assert 0 == instance.diff_supp
    assert 16631 == instance.diff_unmapp
    assert 0 == instance.diff_unpaired
    assert 0 == instance.diff_truly_unpaired
    assert 17560 == instance.diff_unprop
    assert 671 == instance.diff_mate_unmapp
    assert 34463 == instance.diff_quality
    assert 465 == instance.diff_dup
    assert 0 == instance.diff_mito

    assert instance.plots.count() == 5
