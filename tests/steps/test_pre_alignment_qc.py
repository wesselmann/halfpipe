import pytest

from halfpipe.steps.pre_alignment_qc.main import run_pre_alignment_qc

pairness_data = [
    (
        True,
        {
            "forward_readfile": "tests/data/TESTX_H7YRLADXX_S1_L001_R1_001.fastq.gz",
            "reverse_readfile": "tests/data/TESTX_H7YRLADXX_S1_L001_R2_001.fastq.gz",
        },
        "processed_forward_readfile.fastq.gz",
    ),
    (
        False,
        {
            "readfile": "tests/data/TESTX_H7YRLADXX_S1_L001_R1_001.fastq.gz",
        },
        "processed_readfile.fastq.gz",
    ),
]

adapters = [(True, True), (True, False), (False, False)]


@pytest.mark.django_db
@pytest.mark.parametrize("trim,detect", adapters)
@pytest.mark.parametrize("pairness,reads,outname", pairness_data)
def test_pre_alignment_qc(pipeline_factory, pairness, reads, outname, trim, detect):
    pipeline = pipeline_factory()

    # Create folder for pipeline that normally gets created in
    # a previous pipeline step.
    pipeline.get_pipeline_path().mkdir(exist_ok=True)

    options = {
        "retain_intermediary_files": False,
        "enable_paired_end": pairness,
        "enable_adapter_trimming": trim,
        "detect_adapters": detect,
        "forward_adapter_sequence": "ATCGATATT",
        "reverse_adapter_sequence": "ATCGATATT",
        "adapter_sequence": "ATCGATATT",
    }

    run_pre_alignment_qc(reads, options, pipeline.pk)

    assert (pipeline.get_pipeline_path() / outname).exists()
