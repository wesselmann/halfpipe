from http import HTTPStatus

import pytest
from django.urls import reverse

from halfpipe.differential.process import run_differential_analysis
from halfpipe.models import DiffAnalysis
from tests.conftest import MotifFactory, PipelineFactory


@pytest.mark.django_db()
def test_create(monkeypatch, authenticated_client):
    result = {}

    orig = DiffAnalysis.save

    def assemblymockreturn(analysis, **kwargs):
        orig(analysis)
        result["differential"] = analysis

    def celerymockreturn(*args, **kwargs):
        pass

    monkeypatch.setattr(run_differential_analysis, "apply_async", celerymockreturn)
    monkeypatch.setattr(DiffAnalysis, "save", assemblymockreturn)

    pipeline_a = PipelineFactory.create()
    pipeline_b = PipelineFactory.create()

    response = authenticated_client.post(
        reverse("differential:create"),
        data={
            "pipelines": '{"pipelines":' + f"[{pipeline_a.pk}, {pipeline_b.pk}]" + " }",
            "cluster": 15,
        },
    )

    assert response.status_code == HTTPStatus.FOUND

    differential = result["differential"]
    assert differential.diff_hint
    assert differential.diff_chip
    assert differential.status == "PENDING"


@pytest.mark.django_db()
def test_query_motifs(
    authenticated_client, diff_analysis_factory, file_factory, monkeypatch
):
    differential = diff_analysis_factory()

    motif_a = MotifFactory.create(name="A", p_values=[0.5])
    motif_b = MotifFactory.create(name="B", p_values=[0.05], lineplot=file_factory())

    differential.diff_hint.motifs.add(motif_a, motif_b)

    response = authenticated_client.get(
        reverse("differential:footprinting", kwargs={"pk": differential.pk})
    )

    assert response.status_code == HTTPStatus.OK
