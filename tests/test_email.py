import pytest
from django.core import mail


@pytest.mark.django_db()
@pytest.mark.parametrize("enabled", [True, False])
def test_send_mail(pipeline_factory, settings, enabled):
    settings.EMAIL_BACKEND = "django.core.mail.backends.locmem.EmailBackend"

    pipeline = pipeline_factory()
    pipeline.status = "SUCCESS"
    pipeline.options["send_email"] = enabled
    pipeline.save()

    pipeline.send_email()

    if enabled:
        assert len(mail.outbox) == 1
        assert mail.outbox[0].subject == pipeline.email_subject
    else:
        assert len(mail.outbox) == 0
