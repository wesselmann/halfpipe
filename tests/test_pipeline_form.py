from http import HTTPStatus

import pytest
from django.urls import reverse


@pytest.mark.django_db()
def test_upload_field_missing_key(authenticated_client, assembly):
    response = authenticated_client.post(
        reverse("pipeline:create"),
        data={"readfile_url": '{"name": "r.fastq"}'},
    )

    assert response.status_code == HTTPStatus.OK
    assert "Missing tags." in response.content.decode()


@pytest.mark.django_db()
def test_pairness_missing_readfile(authenticated_client, assembly):
    response = authenticated_client.post(
        reverse("pipeline:create"),
        data={
            "enable_paired_end": "on",
            "forward_readfile_url": '{"url": "the://url", "name": "r.fastq"}',
        },
    )

    assert response.status_code == HTTPStatus.OK
    assert (
        "The reverse read file needs to be specified on paired end experiments."
        in response.content.decode()
    )


@pytest.mark.django_db()
def test_pairness_single_readfile(authenticated_client, assembly):
    response = authenticated_client.post(
        reverse("pipeline:create"), data={"enable_paired_end": ""}
    )

    assert response.status_code == HTTPStatus.OK
    assert "The readfile is required." in response.content.decode()


@pytest.mark.django_db()
def test_even_n(authenticated_client, assembly):
    response = authenticated_client.post(
        reverse("pipeline:create"), data={"max_number_of_plots": 3}
    )

    assert response.status_code == HTTPStatus.OK
    assert "3 is not an even number" in response.content.decode()
