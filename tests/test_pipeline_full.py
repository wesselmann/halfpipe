import json
from http import HTTPStatus
from pathlib import Path

import pytest
import requests
from django.urls import reverse

from halfpipe.models import Pipeline
from halfpipe.pipeline.process import run_pipeline
from halfpipe.pipeline.tasks import (
    align,
    footprint_analysis,
    peak_annotate,
    peak_calling,
    post_alignment_qc,
    pre_alignment_qc,
    prepare,
)


def upload_read_file(client, filepath):
    url = client.get(
        reverse("get_presigned_url"),
    ).content.decode()

    with open(filepath, "rb") as f:
        result = requests.put(
            url, data=f.read(), headers={"Content-Type": "application/octet-stream"}
        )

    assert result.status_code == HTTPStatus.OK

    return json.dumps({"url": url.split("?", 1)[0], "name": filepath.name})


@pytest.mark.django_db(transaction=True)
def test_full_paired(authenticated_client, assembly, monkeypatch):

    # Don't start the pipeline via celery
    def mockreturn(*args, **kwargs):
        pass

    monkeypatch.setattr(run_pipeline, "apply_async", mockreturn)

    fw = upload_read_file(authenticated_client, Path("tests/data/yeast_R1.fastq.gz"))
    rw = upload_read_file(authenticated_client, Path("tests/data/yeast_R2.fastq.gz"))

    response = authenticated_client.post(
        reverse("pipeline:create"),
        data={
            "enable_paired_end": "on",
            "forward_readfile_url": fw,
            "reverse_readfile_url": rw,
            "assembly": str(assembly.pk),
            "p_value_cutoff": 0.05,
        },
    )

    assert response.status_code == HTTPStatus.FOUND

    instance = Pipeline.objects.last()
    assert instance.options["enable_paired_end"]
    assert instance.options["forward_readfile_url"]["name"] == "yeast_R1.fastq.gz"

    # Test pipline steps
    last = prepare(instance.options, instance.pk)
    steps = [pre_alignment_qc, align, post_alignment_qc, peak_calling]
    for step in steps:
        last = step(last, instance.options, instance.pk)

    # Peak annotate will not return enriched pathways,
    # since there are none in the test data
    peak_annotate(last, instance.options, instance.pk)
    footprint_analysis(last, instance.options, instance.pk)

    # Those plots should still be there
    assert (instance.get_pipeline_path() / "covplot.png").exists()
    assert (instance.get_pipeline_path() / "heatmap.png").exists()
    assert (instance.get_pipeline_path() / "avgplot.png").exists()
    assert (instance.get_pipeline_path() / "upset.png").exists()
