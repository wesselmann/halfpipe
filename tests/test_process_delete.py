from http import HTTPStatus

import pytest
from django.urls import reverse

from halfpipe.models import Pipeline


@pytest.mark.django_db()
def test_delete_pipeline(pipeline_factory, client, user_factory):
    pipeline = pipeline_factory()
    pk = pipeline.pk

    # Unauthenticated, redirected to login
    response = client.post(reverse("pipeline:delete", args=[pk]))
    assert response.status_code == HTTPStatus.FOUND
    assert Pipeline.objects.filter(pk=pk).exists()

    # Authenticated other
    client.force_login(user_factory())
    client.delete(reverse("pipeline:delete", args=[pk]))
    assert response.status_code == HTTPStatus.FOUND
    assert Pipeline.objects.filter(pk=pk).exists()

    # Authenticated
    client.force_login(pipeline.user)
    client.post(reverse("pipeline:delete", args=[pk]))

    assert not Pipeline.objects.filter(pk=pk).exists()
