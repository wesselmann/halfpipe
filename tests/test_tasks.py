import pytest

from halfpipe.models import Pipeline
from halfpipe.pipeline.signals import update_detail_channel
from halfpipe.tasks import create_handle_error, create_handle_group_error


@pytest.mark.django_db()
def test_error_handling_group(pipeline):
    group_handler = create_handle_group_error(
        Pipeline, update_detail_channel, "halfpipe.pipeline"
    )

    group_handler(pk=pipeline.pk, field="footprinting")

    pipeline.refresh_from_db()
    assert pipeline.footprinting.status == "FAILURE"


@pytest.mark.django_db()
def test_error_handling_chain(pipeline):
    error_handler = create_handle_error(Pipeline, "halfpipe.pipeline")

    pipeline.get_path().mkdir(exist_ok=True)
    error_handler(pk=pipeline.pk)

    pipeline.refresh_from_db()

    assert not pipeline.get_path().exists()
    assert pipeline.status == "FAILURE"

    # End date should be set now
    assert pipeline.end_date
